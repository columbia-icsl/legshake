package columbia.seas.icsl.legshake.util;

import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import columbia.seas.icsl.legshake.constants.AppConstants;
import columbia.seas.icsl.legshake.interfaces.AsyncResponse;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Queue;
import java.util.TreeMap;


/**
 * Created by stephen on 4/11/2017.
 */

public class httpModule {

    /**
     * public function to call to send to server
     */
    public static void run_http_post_to_server(String host_url, JSONObject params) {
        // Start sending in a new thread
        final JSONObject final_params = params;
        final String final_url = host_url;

        Runnable runnable = new Runnable() {

            @Override
            public void run() {
                String data_display_msg = "";
                String json_response = send_json_http_post(final_url, final_params);
                if(json_response != null) {
                    Log.i(AppConstants.HTTP_TAG.toString(), "Tx Success");
                } else {
                    Log.i(AppConstants.HTTP_TAG.toString(), "Tx unsuccessful");
                }
            }
        };
        new Thread(runnable).start();
    }

    /**
     * Queries server to log user feedback
     */
    public static void run_http_post_feedback(String host_url, String email, String password, String comment, AsyncResponse instance) {
        JSONObject params = new JSONObject();

        try {
            params.put(AppConstants.LOGFEEDBACK_TAG.toString(), 1);
            params.put(AppConstants.EMAIL_JSON_TAG.toString(), email);
            params.put(AppConstants.PASSWORD_JSON_TAG.toString(), password);
            params.put(AppConstants.FEEDBACK_TAG.toString(), comment);
            params.put(AppConstants.TIME_JSON_TAG.toString(), System.currentTimeMillis() / 1000);
            params.put(AppConstants.VERSION_TAG.toString(), AppConstants.VERSION_VAL.toDouble());
            HTTPPostRegisterAsyncTask registerTask = new HTTPPostRegisterAsyncTask(host_url, params, instance);
            registerTask.execute();
        } catch(Exception e) {
            Log.i(AppConstants.HTTP_TAG.toString(), e.getMessage());
        }

    }

    /**
     * Queries the server to add user into database
     */
    public static void run_http_post_register(String host_url, String email, String password, String name, String handedness, int age, int weight, int height_in, String ethnicity, String gender, AsyncResponse instance) {
        JSONObject params = new JSONObject();

        try {
            params.put(AppConstants.REGISTER_JSON_TAG.toString(), 1);
            params.put(AppConstants.EMAIL_JSON_TAG.toString(), email);
            params.put(AppConstants.PASSWORD_JSON_TAG.toString(), password);
            params.put(AppConstants.NAME_JSON_TAG.toString(), name);
            params.put(AppConstants.AGE_JSON_TAG.toString(), age);
            params.put(AppConstants.HANDEDNESS_JSON_TAG.toString(), handedness);
            params.put(AppConstants.WEIGHT_LBS_JSON_TAG.toString(), weight);
            params.put(AppConstants.HEIGHT_IN_JSON_TAG.toString(), height_in);  // Store height in inches
            params.put(AppConstants.ETHNICITY_JSON_TAG.toString(), ethnicity);
            params.put(AppConstants.GENDER_JSON_TAG.toString(), gender);
            params.put(AppConstants.TIME_JSON_TAG.toString(), System.currentTimeMillis() / 1000);
            params.put(AppConstants.VERSION_TAG.toString(), AppConstants.VERSION_VAL.toDouble());
            params.put(AppConstants.DEVICE_API_BUILD_JSON_TAG.toString(), Build.VERSION.RELEASE); // Log the android API level
            params.put(AppConstants.DEVICE_MODEL_JSON_TAG.toString(), Build.MODEL); // Log the phone model
            params.put(AppConstants.DEVICE_MANUFACTURER_JSON_TAG.toString(), Build.MANUFACTURER); // Log the phone manufacturer
            HTTPPostRegisterAsyncTask registerTask = new HTTPPostRegisterAsyncTask(host_url, params, instance);
            registerTask.execute();
        } catch(Exception e) {
            Log.i(AppConstants.HTTP_TAG.toString(), e.getMessage());
        }
    }

    /**
     * Queries the server with login information
     * @param host_url
     * @param email
     * @param password
     */
    public static void run_http_post_login(String host_url, String email, String password, AsyncResponse instance) {

        JSONObject params = new JSONObject();

        try {
            params.put(AppConstants.LOGIN_JSON_TAG.toString(), 1);
            params.put(AppConstants.EMAIL_JSON_TAG.toString(), email);
            params.put(AppConstants.PASSWORD_JSON_TAG.toString(), password);
            params.put(AppConstants.TIME_JSON_TAG.toString(), System.currentTimeMillis() / 1000);
            HTTPPostRegisterAsyncTask registerTask = new HTTPPostRegisterAsyncTask(host_url, params, instance);
            registerTask.execute();
        } catch(Exception e) {
            Log.i(AppConstants.HTTP_TAG.toString(), e.getMessage());
        }
    }

    /**
     * Logging false negative
     * @param host_url
     * @param dataStorageQueue
     * @param comment
     * @param instance
     */
    public static void run_http_post_log_falseNegative(String host_url, String email, String password, Queue<RawDataStorage> dataStorageQueue, String comment, AsyncResponse instance) {
        JSONObject jsonParameters = packageParametersFalseNegativeEvent(email, password, System.currentTimeMillis() / 1000, dataStorageQueue, comment);
        HTTPPostRegisterAsyncTask registerTask = new HTTPPostRegisterAsyncTask(host_url, jsonParameters, instance);
        registerTask.execute();
    }

    /**
     * Sends a HTTP Post request to the specified host with the input json params as content; returns the body of the output
     * or null if there was an error.
     */
    private static String send_json_http_post(String host_url, JSONObject params) {
        URL url_object;
        HttpURLConnection con;
        StringBuilder sb;
        BufferedReader br;
        int HttpResult;
        String json_response = null;

        try {
            Log.d(AppConstants.HTTP_TAG.toString(), "Setting up http connection and writing params");

            // Setup http connection parameters and write content
            url_object = new URL(host_url);
            con = (HttpURLConnection) url_object.openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Accept", "application/json");
            con.setRequestMethod("POST");
            OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
            if(params == null) {
                wr.write(new JSONObject().toString());
            } else {
                wr.write(params.toString());
            }
            wr.flush();
            wr.close();
            //con.disconnect();

            // Handle what is returned from POST request
            Log.d(AppConstants.HTTP_TAG.toString(), "Receiving response");

            sb = new StringBuilder();
            HttpResult = con.getResponseCode();
            //con.disconnect();
            Log.d(AppConstants.HTTP_TAG.toString(), String.valueOf(HttpResult));
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                Log.d(AppConstants.HTTP_TAG.toString(), AppConstants.OK_TAG.toString() + "Reading response");

                br = new BufferedReader(
                        new InputStreamReader(con.getInputStream(), "utf-8"));
                Log.d(AppConstants.HTTP_TAG.toString(), "Opened buffered reader");
                String line = br.readLine();
                while (line != null) {
                    Log.d(AppConstants.HTTP_TAG.toString(), "Read line");
                    sb.append(line + "\n");
                    line = br.readLine();
                    //text_display.setText(sb.toString())
                }
                Log.d(AppConstants.HTTP_TAG.toString(), "Closing Buffered Reader");
                br.close();
                Log.d(AppConstants.HTTP_TAG.toString(), "Response read");
                Log.d(AppConstants.HTTP_TAG.toString(), sb.toString());
                Log.d(AppConstants.HTTP_TAG.toString(), "Response displayed");

                //text_display.setText(message);
                //wr.write("\n");
                con.disconnect();

                // Success
                json_response = sb.toString();
            } else {
                Log.d(AppConstants.HTTP_TAG.toString(), AppConstants.WARNING_TAG.toString() + con.getResponseMessage());
                //Toast.makeText(Home.this, AppConstants.WARNING_TAG.toString() + con.getResponseMessage(), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Log.d(AppConstants.HTTP_TAG.toString(), AppConstants.ERROR_TAG.toString() + e.getMessage());
            //Toast.makeText(Home.this, AppConstants.ERROR_TAG.toString() + e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        return json_response;
    }

    /**
     * package input parameters for detection event into a json object to send to server
     * @return jsonParameters
     */
    public static JSONObject packageParametersDetectionEvent(String email,
                                                             String password,
                                                             long time,
                                                             double[] x_axis,
                                                             double[] y_axis,
                                                             double[] z_axis,
                                                             double[] eigVals,
                                                             double[] eigVec1,
                                                             double[] eigVec2,
                                                             double[] eigVec3,
                                                             double[] gforce_mag_spectrum,
                                                             double[] one_dim_mag_spectrum,
                                                             String longitude,
                                                             String latitude,
                                                             TreeMap<String, ArrayList<float[]>> otherSensorData,
                                                             int falsePositive) {
        JSONObject jsonParameters = new JSONObject();

        try {
            jsonParameters.put(AppConstants.EMAIL_JSON_TAG.toString(), email);
            jsonParameters.put(AppConstants.PASSWORD_JSON_TAG.toString(), password);
            jsonParameters.put(AppConstants.TIME_JSON_TAG.toString(), time);
            jsonParameters.put(AppConstants.XAXIS_JSON_TAG.toString(), Arrays.toString(x_axis));
            jsonParameters.put(AppConstants.YAXIS_JSON_TAG.toString(), Arrays.toString(y_axis));
            jsonParameters.put(AppConstants.ZAXIS_JSON_TAG.toString(), Arrays.toString(z_axis));
            jsonParameters.put(AppConstants.EIGVALS_JSON_TAG.toString(), Arrays.toString(eigVals));
            jsonParameters.put(AppConstants.EIGVEC1_JSON_TAG.toString(), Arrays.toString(eigVec1));
            jsonParameters.put(AppConstants.EIGVEC2_JSON_TAG.toString(), Arrays.toString(eigVec2));
            jsonParameters.put(AppConstants.EIGVEC3_JSON_TAG.toString(), Arrays.toString(eigVec3));
            jsonParameters.put(AppConstants.GFORCEMAGSPECTRUM_JSON_TAG.toString(), Arrays.toString(gforce_mag_spectrum));
            jsonParameters.put(AppConstants.ONEDIMMAGSPECTRUM_JSON_TAG.toString(), Arrays.toString(one_dim_mag_spectrum));
            jsonParameters.put(AppConstants.FALSEPOSITIVE_JSON_TAG.toString(), falsePositive);
            jsonParameters.put(AppConstants.VERSION_TAG.toString(), AppConstants.VERSION_VAL.toDouble());
            jsonParameters.put(AppConstants.LONGITUDE_JSON_TAG.toString(), longitude);
            jsonParameters.put(AppConstants.LATITUDE_JSON_TAG.toString(), latitude);
            jsonParameters.put(AppConstants.COMMENT_JSON_TAG.toString(), "");

            // Unpack sensor data
            JSONObject otherSensorDataJSON = new JSONObject();
            for(TreeMap.Entry<String, ArrayList<float[]>> entry: otherSensorData.entrySet()) {
                String sensorName = entry.getKey();
                ArrayList<float[]> data_arraylist = entry.getValue();

                // Convert from object to primitive array
                float[][] data_list = new float[data_arraylist.size()][];
                for(int i = 0; i < data_arraylist.size(); i++) {
                    data_list[i] = data_arraylist.get(i);
                }

                otherSensorDataJSON.put(sensorName, Arrays.deepToString(data_list));
            }
            jsonParameters.put(AppConstants.OTHERSENSORDATA_JSON_TAG.toString(), otherSensorDataJSON);

        } catch(Exception e) {
            Log.e(AppConstants.HTTP_TAG.toString(), AppConstants.ERROR_TAG.toString() + e.getMessage());
        }
        return jsonParameters;
    }

    /**
     * package input parameters for false negative event detection to send to server
     * @return
     */
    public static JSONObject packageParametersFalseNegativeEvent(String email, String password, long timestamp, Queue<RawDataStorage> dataStorageQueue, String comment) {
        JSONObject jsonParameters = new JSONObject();
        int queueSize = dataStorageQueue.size();

        try{
            jsonParameters.put(AppConstants.EMAIL_JSON_TAG.toString(), email);
            jsonParameters.put(AppConstants.PASSWORD_JSON_TAG.toString(), password);
            jsonParameters.put(AppConstants.TIME_JSON_TAG.toString(), timestamp);
            jsonParameters.put(AppConstants.VERSION_TAG.toString(), AppConstants.VERSION_VAL.toDouble());

            // Put each entry in. Order such that the first entry is the most recent
            for(int i = 0; i < queueSize; i++) {
                RawDataStorage window = dataStorageQueue.poll();
                JSONObject jsonWindow = packageParametersDetectionEvent(window.email,
                                                                        window.password,
                                                                        window.timestamp,
                                                                        window.x_axis,
                                                                        window.y_axis,
                                                                        window.z_axis,
                                                                        window.eigVals,
                                                                        window.eigVec1,
                                                                        window.eigVec2,
                                                                        window.eigVec3,
                                                                        window.gforce_mag_spectrum,
                                                                        window.one_dim_mag_spectrum,
                                                                        window.longitude,
                                                                        window.latitude,
                                                                        window.otherSensorData,
                                                                        window.false_positive);

                jsonParameters.put(String.valueOf(AppConstants.NUMWINDOWSTOLOG_VAL.toInt() - i), jsonWindow.toString());
            }
            jsonParameters.put(AppConstants.COMMENT_JSON_TAG.toString(), "");
            jsonParameters.put(AppConstants.COMMENT_FN_JSON_TAG.toString(), comment);
            return jsonParameters;

        } catch(Exception e) {
            Log.e(AppConstants.HTTP_TAG.toString(), AppConstants.ERROR_TAG.toString() + e.getMessage());
        }
        return jsonParameters;
    }

    /**
     * package input parameters for false positive event into a json object to send to server
     * @return jsonParameters
     */
    public static JSONObject packageParametersFalsePositiveEvent(String email, String password, long time, String comment) {
        JSONObject jsonParameters = new JSONObject();

        try {
            jsonParameters.put(AppConstants.EMAIL_JSON_TAG.toString(), email);
            jsonParameters.put(AppConstants.PASSWORD_JSON_TAG.toString(), password);
            jsonParameters.put(AppConstants.TIME_JSON_TAG.toString(), time);
            jsonParameters.put(AppConstants.VERSION_TAG.toString(), AppConstants.VERSION_VAL.toDouble());
            jsonParameters.put(AppConstants.COMMENT_FP_JSON_TAG.toString(), comment);

            // False positives are logged as 1s in the server
            jsonParameters.put(AppConstants.FALSEPOSITIVE_JSON_TAG.toString(), 1);
        } catch(Exception e) {
            Log.e(AppConstants.HTTP_TAG.toString(), AppConstants.ERROR_TAG.toString() + e.getMessage());
        }
        return jsonParameters;
    }

    /**
     * AsyncTask class for http posting info to the server.
     */
    public static class HTTPPostRegisterAsyncTask extends AsyncTask<String, Void, String> {
        private String host_url;
        private JSONObject params;
        private AsyncResponse instance;
        public HTTPPostRegisterAsyncTask(String host_url, JSONObject params, AsyncResponse instance) {
            this.host_url = host_url;
            this.params = params;
            this.instance = instance;
        }

        protected String doInBackground(String... params) {
            String retVal = null;

            try {
                String json_response = send_json_http_post(host_url, this.params);
                return json_response;
            } catch(Exception e) {
                Log.d(AppConstants.HTTP_TAG.toString(), AppConstants.ERROR_TAG.toString() + e.getMessage());
            }

            return retVal;
        }

        // Afterwards, Return the body of the message.
        protected void onPostExecute(String result) {
            instance.processFinish(result);
        }

    }
}
