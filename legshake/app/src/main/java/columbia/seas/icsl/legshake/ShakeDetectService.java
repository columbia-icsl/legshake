package columbia.seas.icsl.legshake;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import columbia.seas.icsl.legshake.constants.AppConstants;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

/**
 * Created by yanlu on 11/2/16.
 * Keep running as back ground service, by implementing the batching, the service will be wake up every 6 second
 * When week up, it will collect acceleration data stored in batching, and send the data to ShakeAnalysis class for further processing
 */

public class ShakeDetectService extends Service implements SensorEventListener {
    private SensorManager mSensorManager;
    private Sensor mSensor;
    private PowerManager.WakeLock wakeLock = null;
    int ptr=0;
    //  Decide the size of to be analysis, should be a power of 2
    private static int N = 128;
    //  Decide the sleep time
    private static int T;
    private static String userId;
    private static String password;
    private long curr_time = 0;
    private long prev_time = 0;

    //private Location location = null;

    // gForce
    double[] val;

    // Raw 3 axis data
    double[][] raw_val;

    int frequency;

    // Structures for collecting all sensor data
    List<Sensor> allSensors = null;
    boolean currentAllSensorFlag = false; // True if we are currently collecting all sensor data, false otherwise.
    TreeMap<String, ArrayList<float[]>> allSensorDataContainer = new TreeMap<>();

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // Only use if HandleServices Not in use
        PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                "MyWakelockTag");
        if (wakeLock.isHeld() == false)
            wakeLock.acquire();

    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public void onDestroy() {
        super.onDestroy();

        // Only use if HandleServices Not in use
        if (wakeLock != null) {
            wakeLock.release();
            wakeLock = null;
        }
        if(mSensorManager != null)
            mSensorManager.unregisterListener(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(intent!=null) {
            Bundle b = intent.getExtras();
            if (b != null) {
                int val = b.getInt("f");
                if (val == 50)
                    frequency = 50;
                else
                    frequency = 50; // Just use 50 hz for now.
                    //frequency = 66666;

                // Setup location receiver
                /*BroadcastReceiver locationServiceReceiver = new BroadcastReceiver() {
                    public void onReceive(Context context, Intent intent) {
                        location = intent.getExtras().getParcelable(AppConstants.NEWLOCATIONUPDATE_INTENTFILTER_TAG.toString());
                    }
                };

                LocalBroadcastManager.getInstance(this.getApplicationContext()).registerReceiver(locationServiceReceiver, new IntentFilter(AppConstants.NEWLOCATIONUPDATE_INTENTFILTER_TAG.toString()));
                */
                // Start detection
                startDetection();
            }

        }
        return super.onStartCommand(intent, flags, startId);
    }

    private void startDetection() {
        Log.e("ShakeDetect", "Starting Detection...");
        // Set up accelerometer sensor data collection
        SharedPreferences sharedPref = this.getApplicationContext().getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        userId = sharedPref.getString(getString(R.string.preference_email),"");
        password = sharedPref.getString(getString(R.string.preference_password),"");

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        float power = mSensor.getPower();
        Log.d("power", power + " ");
        if (mSensor.getFifoMaxEventCount() > 512) {T = 6;}
        else {T = 3;}
        val = new double[N];
        raw_val = new double[N][3];

//      The next line is for batching, which can save battery. For batching, we can control the sleep time as shown in "T * 1000000"
        if(frequency == 50) {
            Log.e("ShakeDetect", "Starting lshake at 50hz");
            val = new double[N];
            raw_val = new double[N][3];
            mSensorManager.registerListener(this, mSensor, 20000, T * 1000000 /* T * 1 seconds */);
        }
        else {
            Log.e("ShakeDetect", "Starting lshake at 15hz");

            // Logging to denote beginning of sampling
            Log.d("Services", "Service Start");
            Log.d("Batching T", String.valueOf(T));
            Log.d("BatchingSize", mSensor.getMaxDelay() + ";" + mSensor.getFifoMaxEventCount() );

            Log.d("CENTRA_FUNDAMENTAL_FREQ", "" + AppConstants.CENTRAL_FUNDAMENTAL_FREQ_IDX.toInt());
            Log.d("CENTRA_FH_FREQ", "" + AppConstants.CENTRAL_FIRST_HARMONIC_FREQ_IDX.toInt());
            Log.d("FUND_FREQ_HALFBAND", "" + AppConstants.FUNDAMENTAL_FREQ_HALF_BAND_IDX.toInt());
            Log.d("FH_FREQ_HALFBAND", "" + AppConstants.FIRST_HARMONIC_FREQ_HALF_BAND_IDX.toInt());
            Log.d("HIGHEST_PEAK_BUFFER", "" + AppConstants.HIGHEST_PEAK_BUFFER_FREQ_IDX.toInt());
            Log.d("HALF BAND SEC PEAKS", "" + AppConstants.HALF_BAND_SECONDARY_PEAK_IDX.toInt());
            Log.d("Sampling freq", "" + AppConstants.ACCEL_SAMPLING_FREQ.toDouble());
            Log.d("N_DATA", "" + AppConstants.N_DATA.toInt());
            Log.d("N_FFT", "" + AppConstants.N_FFT.toInt());
            Log.d("Sampling Period_US", "" + AppConstants.ACCEL_SAMPLING_PERIOD_US.toInt());

            val = new double[AppConstants.N_DATA.toInt()];
            raw_val = new double[AppConstants.N_DATA.toInt()][3];
            mSensorManager.registerListener(this, mSensor, frequency, 1 * 1000000 /* T * 1 seconds */);
            //mSensorManager.registerListener(this, mSensor, frequency);
        }

        //      Log.d("Services", "Service Start");
        // Log.d("Batching T", String.valueOf(T));
        // Log.d("BatchingSize", mSensor.getMaxDelay() + ";" + mSensor.getFifoMaxEventCount() );
    }

    public void onSensorChanged(SensorEvent event) {
        //Right in here is where you put code to read the current sensor values and
        //update any views you might have that are displaying the sensor information
        //You'd get accelerometer values like this:
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {

            raw_val[ptr][0] = event.values[0] / 9.81;
            raw_val[ptr][1] = event.values[1] / 9.81;
            raw_val[ptr][2] = event.values[2] / 9.81;
            val[ptr] = Math.sqrt(raw_val[ptr][0] * raw_val[ptr][0] + raw_val[ptr][1] * raw_val[ptr][1]
                    + raw_val[ptr][2] * raw_val[ptr][2]);
            //Log.i(AppConstants.SHAKEDETECTSERVICE_TAG.toString(), "x: " + String.valueOf(raw_val[ptr][0]));
            //Log.i(AppConstants.SHAKEDETECTSERVICE_TAG.toString(), "y: " + String.valueOf(raw_val[ptr][1]));
            //Log.i(AppConstants.SHAKEDETECTSERVICE_TAG.toString(), "z: " + String.valueOf(raw_val[ptr][2]));
            //Log.i(AppConstants.SHAKEDETECTSERVICE_TAG.toString(), "g: " + String.valueOf(val[ptr]));

            // Running 50 Hz
            if (frequency == 50) {
                ptr = (ptr + 1) % N;
                if (ptr == 0) {

                    // Check to see if location is valid, not too old, and if the user is ok with sending location data.
                    String longitude;
                    String latitude;
                    long timeObtained = MainActivity.locationTimeObtained;
                    if(timeObtained >= System.currentTimeMillis() - AppConstants.LOCATIONSERVICE_TIMERANGE.toInt() && MainActivity.allDataCollectionFlag) {
                        longitude = MainActivity.longitude;
                        latitude = MainActivity.latitude;
                    } else {
                        longitude = "";
                        latitude = "";
                    }

                    prev_time = curr_time;
                    curr_time = System.currentTimeMillis();
                    Log.d("True Sampling Rate", "" + 1.0 * N / (curr_time - prev_time));

                    Log.d("ShakeDect", "starting analysis");
                    // Execute legshaking detection.
                    ShakeAnalysis task = new ShakeAnalysis(getApplicationContext(), userId, password, longitude, latitude);
                    task.execute(val, raw_val, frequency, allSensorDataContainer);

                    val = new double[N];
                    raw_val = new double[N][3];
                    ptr = 0;

                    // Check if the user changed the state of collecting all data or not
                    checkCollectAllSensorData();
                }
            }

            // Running 15 Hz
            else {
                ptr = (ptr + 1) % AppConstants.N_DATA.toInt();
                if (ptr == 0) {

                    // Check to see if location is valid, not too old, and if the user is ok with sending location data.
                    String longitude;
                    String latitude;
                    long timeObtained = MainActivity.locationTimeObtained;
                    if(timeObtained >= System.currentTimeMillis() - AppConstants.LOCATIONSERVICE_TIMERANGE.toInt() && MainActivity.allDataCollectionFlag) {
                        longitude = MainActivity.longitude;
                        latitude = MainActivity.latitude;
                    } else {
                        longitude = "";
                        latitude = "";
                    }

                    prev_time = curr_time;
                    curr_time = System.currentTimeMillis();
                    Log.d("True Sampling Rate", "" + 1.0 * AppConstants.N_DATA.toInt() / (curr_time - prev_time));

                    Log.d("ShakeDect", "starting analysis");
                    // Execute `    legshaking detection.
                    ShakeAnalysis task = new ShakeAnalysis(getApplicationContext(), userId, password, longitude, latitude);
                    task.execute(val, raw_val, frequency, allSensorDataContainer);

                    val = new double[AppConstants.N_DATA.toInt()];
                    raw_val = new double[AppConstants.N_DATA.toInt()][3];
                    ptr = 0;

                    // Check if the user changed the state of collecting all data or not
                    checkCollectAllSensorData();
                }
            }
        }

        // All other sensors
        else {
            allSensorDataContainer.get(event.sensor.getName()).add(event.values);
        }
    }

    /**
     * Checks if we should collect all sensor data or not. Turns on or off collection accordingly.
     * @return
     */
    private void checkCollectAllSensorData() {

        // User just allowed all sensor data to be collected, but we have not yet started to do so
        // In this case, start collection from all sensors
        if(MainActivity.allDataCollectionFlag && !currentAllSensorFlag) {

            // Initialize container
            allSensorDataContainer = new TreeMap<>();

            // Get all sensors, and register listeners for them unless it's the accelerometer
            allSensors = mSensorManager.getSensorList(Sensor.TYPE_ALL);
            for(Sensor sensor: allSensors) {
                if(sensor.getType() != Sensor.TYPE_ACCELEROMETER) {
                    mSensorManager.registerListener(this, sensor, sensor.getMaxDelay(), T * 1000000 /* T * 1 seconds */);
                    allSensorDataContainer.put(sensor.getName(), new ArrayList<float[]>());
                }
            }

            Log.d(AppConstants.SHAKEDETECTSERVICE_TAG.toString(), "Total Sensors: " + allSensors.size());
            Log.d(AppConstants.SHAKEDETECTSERVICE_TAG.toString(), "Sensors Logging: " + allSensorDataContainer.size());
            currentAllSensorFlag = true;
        }

        // User just turned off all data sensor to be collected, but we are still collecting stuff,
        // so we need to turn it off
        else if(!MainActivity.allDataCollectionFlag && currentAllSensorFlag) {

            for(Sensor sensor: allSensors) {
                if(sensor.getType() != Sensor.TYPE_ACCELEROMETER) {
                    mSensorManager.unregisterListener(this, sensor);
                }
            }
            allSensors = null;
            allSensorDataContainer = new TreeMap<>();
            currentAllSensorFlag = false;
        }
    }

    /**
     * Sends a new window of accelerometer data back to the main activity; used for logging false negatives
     */
    /*private void sendWindowToMain(String username, long timestamp, double[] x_axis, double[] y_axis, double[] z_axis, int falsePositive) {

        //Log.i(AppConstants.SHAKEDETECTSERVICE_TAG.toString(), "Sending new Window back to Main");
        Intent intent = new Intent(AppConstants.NEWACCELDATEUPDATE_TAG.toString());
        intent.putExtra(AppConstants.USERNAME_JSON_TAG.toString(), username);
        intent.putExtra(AppConstants.TIME_JSON_TAG.toString(), timestamp);
        intent.putExtra(AppConstants.XAXIS_JSON_TAG.toString(), x_axis);
        intent.putExtra(AppConstants.YAXIS_JSON_TAG.toString(), y_axis);
        intent.putExtra(AppConstants.ZAXIS_JSON_TAG.toString(), z_axis);
        intent.putExtra(AppConstants.FALSEPOSITIVE_JSON_TAG.toString(), falsePositive);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }*/
}
