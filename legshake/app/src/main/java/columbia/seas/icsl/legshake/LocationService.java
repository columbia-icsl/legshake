package columbia.seas.icsl.legshake;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import columbia.seas.icsl.legshake.constants.AppConstants;

/**
 * Created by stephen on 2/4/2018.
 */

public class LocationService extends Service implements LocationListener {

    private LocationManager locationManager;

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void onCreate() {
        super.onCreate();

        Log.i(AppConstants.LOCATIONSERVICE_TAG.toString(), "Initializing LocationService....");
        locationManager = null;
        if(this.getApplicationContext().checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == PackageManager.PERMISSION_GRANTED) {

            // Create and register location service
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, AppConstants.LOCATIONSERVICE_MINTIME.toInt(), AppConstants.LOCATIONSERVICE_MINDISTANCE.toInt(), this);
        }
        else {
            Log.e(AppConstants.LOCATIONSERVICE_TAG.toString(), "Fine grain location permission not granted.");
        }
        Log.i(AppConstants.LOCATIONSERVICE_TAG.toString(), "LocationService finished initializing....");
    }

    public void onLocationChanged(Location loc) {

        // Send location data to shake detect service
        Log.d(AppConstants.LOCATIONSERVICE_TAG.toString(), "Location Changed");
        Intent intent = new Intent(AppConstants.NEWLOCATIONUPDATE_INTENTFILTER_TAG.toString());
        intent.putExtra(AppConstants.NEWLOCATIONUPDATE_INTENTFILTER_TAG.toString(), loc);
        LocalBroadcastManager.getInstance(this.getApplicationContext()).sendBroadcast(intent);
    }

    public void onProviderDisabled(String provider) {
    }

    public void onProviderEnabled(String provider) {
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    public void onDestroy() {

        Log.i(AppConstants.LOCATIONSERVICE_TAG.toString(), "Stopping LocationService....");
        if(this.getApplicationContext().checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == PackageManager.PERMISSION_GRANTED && locationManager != null) {
            locationManager.removeUpdates(this);
            locationManager = null;
        }
    }
}
