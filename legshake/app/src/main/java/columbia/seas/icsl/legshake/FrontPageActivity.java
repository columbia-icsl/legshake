package columbia.seas.icsl.legshake;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import columbia.seas.icsl.legshake.constants.AppConstants;

/**
 * Created by yanlu on 11/21/16.
 */

public class FrontPageActivity extends AppCompatActivity{
    private Context context;
    Activity _this = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this.getApplicationContext();

        // Check if there was another running activity, if so resume it
        /*if (!isTaskRoot()
                && getIntent().hasCategory(Intent.CATEGORY_LAUNCHER)
                && getIntent().getAction() != null
                && getIntent().getAction().equals(Intent.ACTION_MAIN)) {

            finish();
            return;
        }*/

        // Check if user logged in previously into this phone.
        loginCheck();

        // Initialize page
        setContentView(R.layout.activity_frontpage);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Button registerButton = (Button) findViewById(R.id.RegisterButton);
        Button loginButton = (Button) findViewById(R.id.LoginButton);
        TextView versionField = (TextView) findViewById(R.id.frontPageVersion);

        // Brings to registration page
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FrontPageActivity.this, RegisterActivity.class);
                FrontPageActivity.this.startActivity(intent);
                FrontPageActivity.this.finish();
            }
        });

        // Brings to login screen
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FrontPageActivity.this, LoginActivity.class);
                FrontPageActivity.this.startActivity(intent);
                FrontPageActivity.this.finish();
            }
        });

        // Versioning
        versionField = (TextView) findViewById(R.id.frontPageVersion);
        versionField.setText("Ver " + String.valueOf(AppConstants.VERSION_VAL.toDouble()) + " alpha");
    }

    protected void loginCheck() {
        Log.i("login", "working");

        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        String email = sharedPref.getString(getString(R.string.preference_email),"");
        String password = sharedPref.getString(getString(R.string.preference_password),"");
        if (!email.equals("") && !password.equals("")) {
            //startService(new Intent(this, ShakeDetectService.class));
            this.startActivity(new Intent(this, MainActivity.class));
            this.finish();
        }
    }
}
