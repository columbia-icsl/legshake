package columbia.seas.icsl.legshake;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import columbia.seas.icsl.legshake.constants.AppConstants;
import columbia.seas.icsl.legshake.constants.NotificationConstants;
import columbia.seas.icsl.legshake.util.RawDataStorage;

import java.util.Queue;

/**
 * Created by jordanvega on 12/14/17.
 */

public class SendCommentDialog extends Activity {
    EditText editTextComment;
    static boolean active = false;
    private final String LOG_TAG = "SendCommentDialog";

    String username;
    Queue<RawDataStorage> dataStorageQueue;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        active = true;
        setContentView(R.layout.dialog);
        editTextComment = (EditText) findViewById(R.id.comment);
    }

    @Override
    protected void onStop() {
        super.onStop();
        active = false;
    }

    //triggered when button submit
    public void submit(View v){
        String comment = editTextComment.getText().toString();
        //submit with comment
        Log.d("Comment", comment);

        // Send comment back to the service
        Intent intent = new Intent(getApplicationContext(), NotificationService.class);
        intent.setAction(NotificationConstants.ACTION.RETURN_COMMENT_ACTION);
        intent.putExtra(AppConstants.COMMENT_JSON_TAG.toString(), comment);
        startService(intent);

        closeDialog();
    }

    //triggered when button cancel
    public void cancel(View v){
        closeDialog();
    }

    private void closeDialog() {
        this.finish();
    }
}
