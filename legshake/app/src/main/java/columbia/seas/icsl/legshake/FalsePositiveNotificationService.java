package columbia.seas.icsl.legshake;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

import columbia.seas.icsl.legshake.constants.AppConstants;
import columbia.seas.icsl.legshake.constants.NotificationConstants;
import columbia.seas.icsl.legshake.interfaces.AsyncResponse;
import columbia.seas.icsl.legshake.util.httpModule;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by stephen on 1/9/2018.
 *
 * For handling reporting false positives
 */

public class FalsePositiveNotificationService extends Service implements AsyncResponse {

    private static NotificationManager notificationManager;
    public static final int NOTIFICATION_ID = 1;

    private final String LOG_TAG = "FP_NotificationService";

    // Fields required for logging false positive to server
    private static String username = "";
    private static String password = "";
    private static long latest_timestamp = 0;

    public FalsePositiveNotificationService() {
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        // Initial notification
        if (intent.getAction().equals(NotificationConstants.ACTION.STARTFOREGROUND_ACTION)) {
            username = intent.getStringExtra(AppConstants.EMAIL_JSON_TAG.toString());
            password = intent.getStringExtra(AppConstants.PASSWORD_JSON_TAG.toString());
            latest_timestamp = intent.getLongExtra(AppConstants.TIME_JSON_TAG.toString(), 0);
            Log.i(LOG_TAG, "Leg Shake Detection Logging");
            Log.i(LOG_TAG, "Username: " + username);
            Log.i(LOG_TAG, "Timestamp: " + String.valueOf(latest_timestamp));

            showNotification();
        }

        // User decided to log a false positive
        else if(intent.getAction().equals(NotificationConstants.ACTION.FALSE_POS_WITH_COMMENT)) {

            Intent it = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
            getApplicationContext().sendBroadcast(it);

            if(!FalsePositiveSendCommentDialog.active) {
                Intent commentDia = new Intent(this, FalsePositiveSendCommentDialog.class);
                commentDia.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                commentDia.setAction(Intent.ACTION_MAIN);
                startActivity(commentDia);
            }
        }

        // Receiving comment from dialog
        else if(intent.getAction().equals(NotificationConstants.ACTION.RETURN_COMMENT_ACTION)) {
            Log.i(LOG_TAG, "Logging False Neg with Comment");
            String comment = intent.getStringExtra(AppConstants.COMMENT_JSON_TAG.toString());
            broadcastFalsePositive(comment);
        }

        //  Pushed icon, go to main activity
        else if(intent.getAction().equals(NotificationConstants.ACTION.NOTIFICATION_ICON)) {
            Intent homeIntent = new Intent(this, MainActivity.class);
            homeIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);
            startActivity(homeIntent);
        }
        return START_STICKY;
    }

    private void broadcastFalsePositive(String comment) {
        if(AppConstants.SERVER_FLAG.toInt() > 0) {
            JSONObject json_parameters = httpModule.packageParametersFalsePositiveEvent(username, password, latest_timestamp, comment);
            httpModule.run_http_post_to_server(AppConstants.LOGFALSEPOSITIVE_URL.toString(), json_parameters);
            notificationManager.cancel(NOTIFICATION_ID);
        }
    }

    /**
     * Displays false positive notification
     */
    private void showNotification() {
        RemoteViews views = new RemoteViews(getPackageName(),
                R.layout.false_positive_status_bar);


        views.setViewVisibility(R.id.fp_status_bar_icon, View.VISIBLE);

        Intent notificationIntent = new Intent(this, MainActivity.class);
        notificationIntent.setAction(NotificationConstants.ACTION.MAIN_ACTION);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

        // Set time of notification
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");

        String time = simpleDateFormat.format(calendar.getTime());
        views.setTextViewText(R.id.fp_textView8, "Leg shaking detected at " + time + "!");

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);

        Intent fpIntent = new Intent(this, FalsePositiveNotificationService.class);
        fpIntent.setAction(NotificationConstants.ACTION.FALSE_POS_WITH_COMMENT);
        PendingIntent falsePosIntent = PendingIntent.getService(this, 0,
                fpIntent, 0);

        Intent iconIntent = new Intent(this, FalsePositiveNotificationService.class);
        iconIntent.setAction(NotificationConstants.ACTION.NOTIFICATION_ICON);
        PendingIntent iconPendingIntent = PendingIntent.getService(this, 0, iconIntent, 0);

        views.setOnClickPendingIntent(R.id.fp_alert, falsePosIntent);

        views.setOnClickPendingIntent(R.id.fp_status_bar_icon, iconPendingIntent);
        views.setOnClickPendingIntent(R.id.fp_message_box, iconPendingIntent);
        views.setOnClickPendingIntent(R.id.fp_status_bar_xml, iconPendingIntent);

        Notification.Builder n = new Notification.Builder(this);

        // Set sound and vibration settings
        if(MainActivity.soundSwitchFlag && MainActivity.vibrateSwitchFlag) {
            //n.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
            n.setDefaults(Notification.DEFAULT_SOUND);
            n.setVibrate(new long[]{100, 1000});
        }
        else if(MainActivity.soundSwitchFlag) {
            n.setDefaults(Notification.DEFAULT_SOUND);
            n.setVibrate(new long[]{0});
        }
        else if (MainActivity.vibrateSwitchFlag) {
            //n.setDefaults(Notification.DEFAULT_VIBRATE);
            n.setVibrate(new long[]{100, 1000});
            n.setSound(null);
        }
        else {
            n.setVibrate(new long[]{0});
            n.setSound(null);
        }
        n.setSmallIcon(R.drawable.waiting_room);
        n.setColor(getResources().getColor(R.color.iconFillColor));
        n.setOnlyAlertOnce(true);
        Notification status = n.build();
        status.contentView = views;
        status.contentIntent = pendingIntent;

        notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(NOTIFICATION_ID);
        notificationManager.notify(NOTIFICATION_ID, status);

        // Stop the service
        Intent intent = new Intent(this, FalsePositiveNotificationService.class);
        this.stopService(intent);
    }

    /**
     * For receiving server response after logging false positive
     * @param retVal
     */
    public void processFinish(String retVal) {

    }
}
