package columbia.seas.icsl.legshake.constants;

import android.os.Environment;

import columbia.seas.icsl.legshake.util.Maths;

import java.io.File;

/**
 * Created by stephen on 4/11/2017.
 */

public enum AppConstants {
    APP_NAME("LegShake"),
    HTTP_TAG("HTTP: "),
    MAINACTIVITY_TAG("MainActivity"),
    REGISTERACTIVITY_TAG("RegisterActivity"),
    LOGINACTIVITY_TAG("LoginActivity"),
    SHAKEDETECTSERVICE_TAG("ShakeDetectService"),
    SHAKEANALYSISSERVICE_TAG("ShakeAnalysis"),
    OK_TAG("OK: "),
    ERROR_TAG("Error: "),
    WARNING_TAG("Warning: "),
    LOG_DIR(Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + APP_NAME),
    LOCATIONSERVICE_TAG("LocationService"),
    HANDLESERVICES_TAG("HandleServices"),
    ACTIVITYRECOGNIZEDSERVICE_TAG("ActivityRecognizedService"),
    PROXIMITYSENSORSERVICE_TAG("ProximitySensorService"),
    ROTATIONSENSORSERVICE_TAG("RotationSensorService"),

    // SERVER URLS
    LOGFEEDBACK_URL("http://icsl.ee.columbia.edu:8001/legShake/logFeedback"),
    REGISTRATION_URL("http://icsl.ee.columbia.edu:8001/legShake/register"),
    LOGIN_URL("http://icsl.ee.columbia.edu:8001/legShake/login"),
    LOGDETECTION_URL("http://icsl.ee.columbia.edu:8001/legShake/logDetection"),
    LOGFALSEPOSITIVE_URL("http://icsl.ee.columbia.edu:8001/legShake/logFalsePositive"),
    LOGFALSENEGATIVE_URL("http://icsl.ee.columbia.edu:8001/legShake/logFalseNegative"),

    // SERVER TAGS
    LOGFEEDBACK_TAG("logFeedback"),
    LOGDETECTIONTAG("logDetection"),
    LOGFALSEPOSITIVE_TAG("logFalsePositive"),
    LOGFALSENEGATIVE_TAG("logFalseNegative"),

    // Shared Preferences file tags
    SHARED_PREF_TAG("icsl"),
    SHARED_PREF_ID_TAG("ID"),

    // JSON key values for parameters sent to server
    //USERNAME_JSON_TAG("username"),
    TIME_JSON_TAG("timestamp"),
    XAXIS_JSON_TAG("x"),
    YAXIS_JSON_TAG("y"),
    ZAXIS_JSON_TAG("z"),
    EIGVALS_JSON_TAG("eigVals"),
    EIGVEC1_JSON_TAG("eigVec1"),
    EIGVEC2_JSON_TAG("eigVec2"),
    EIGVEC3_JSON_TAG("eigVec3"),
    GFORCEMAGSPECTRUM_JSON_TAG("gforce_mag_spectrum"),
    FALSEPOSITIVE_JSON_TAG("false_positive"),
    REGISTER_JSON_TAG("register"),
    LOGIN_JSON_TAG("login"),
    ONEDIMMAGSPECTRUM_JSON_TAG("one_dim_mag_spectrum"),
    OTHERSENSORDATA_JSON_TAG("other_sensor_data"),
    COMMENT_JSON_TAG("comments"),
    COMMENT_FP_JSON_TAG("comments_fp"),
    COMMENT_FN_JSON_TAG("comments_fn"),
    DATASTORAGEQUEUE_JSON_TAG("datastoragequeue"),
    FEEDBACK_TAG("feedback"),
    DEVICE_API_BUILD_JSON_TAG("device_build"),
    DEVICE_MODEL_JSON_TAG("device_model"),
    DEVICE_MANUFACTURER_JSON_TAG("device_manufacturer"),

    // Service/Activity updates
    NEWACCELDATEUPDATE_INTENTFILTER_TAG("new_accel_data_update"),
    NEWLOCATIONUPDATE_INTENTFILTER_TAG("new_location_data_update"),  // For receiving new location data

    // constants for alerting user of leg shaking
    LOG_FP_ALERT_LEGSHAKING_TAG("log_fp_alert_legshaking"),                    // user logs false positive with no comment
    LOG_FP_ALERT_LEGSHAKING_COMMENT_TAG("log_fp_alert_legshaking_comment"),    // user logs false positive with comment

    // Number of windows to send if user says there is a false negative.
    NUMWINDOWSTOLOG_VAL(4),

    // Version
    VERSION_TAG("version"),
    VERSION_VAL(2.9),

    // Flag for enabling data to be sent to server or not
    SERVER_FLAG(1),

    /** Begin 15 Hz detection parameters **/
    // Sampling
    ACCEL_SAMPLING_PERIOD_US(66666), // Sampling period in microseconds
    ACCEL_SAMPLING_FREQ((1000000.0 / ACCEL_SAMPLING_PERIOD_US.toInt())),           // Sampling frequency in hz
    ACCEL_MAX_SAMPLING_FREQ(50.0),                                             // Maximum Sampling Frequency

    // Detection parameters
    WIN_LEN(2.0),           // Window Sample length in seconds
    N_DATA((int) (WIN_LEN.toDouble() / (ACCEL_SAMPLING_PERIOD_US.toInt() / 1000000.0))),   // Window Length in samples of sampled data
    N_FFT(Maths.nextpow2(N_DATA.toInt())),                                  // Double-sided FFT length in samples = next power of 2 of N_DATA

    // Detection parameters: Looking for highest peak
    CENTRAL_FUNDAMENTAL_FREQ(5.0),                     // In Hz; fundamental frequency center point
    CENTRAL_FIRST_HARMONIC_FREQ(10.5),               // In Hz; first harmonic frequency center point
    FUNDAMENTAL_FREQ_HALF_BAND(1.2),                 // Look for the fundamental frequency in range below and above the central specified frequency
    FIRST_HARMONIC_FREQ_HALF_BAND(2.7),               // Look for the first harmonic in range below and above the central specified frequency
    HIGHEST_PEAK_BUFFER_FREQ(2.5),                   // Give a buffer of +- this value around the highest peak when searching for the second highest peak; in other words, don't check values within += this range centered around highest peak when searching for secondary peaks.

    // Detection parameters: Looking for highest peak converted to samples
    CENTRAL_FUNDAMENTAL_FREQ_IDX((int)Math.min(Math.max(Math.round(CENTRAL_FUNDAMENTAL_FREQ.toDouble() * N_FFT.toInt() * ACCEL_SAMPLING_PERIOD_US.toInt() / 1000000.0), 0), N_FFT.toInt() - 1)),
    CENTRAL_FIRST_HARMONIC_FREQ_IDX((int)Math.min(Math.max(Math.round(CENTRAL_FIRST_HARMONIC_FREQ.toDouble() * N_FFT.toInt() * ACCEL_SAMPLING_PERIOD_US.toInt() / 1000000.0), 0), N_FFT.toInt() - 1)),
    FUNDAMENTAL_FREQ_HALF_BAND_IDX((int)Math.min(Math.max(Math.round(FUNDAMENTAL_FREQ_HALF_BAND.toDouble() * N_FFT.toInt() * ACCEL_SAMPLING_PERIOD_US.toInt() / 1000000.0), 0), N_FFT.toInt() - 1)),
    FIRST_HARMONIC_FREQ_HALF_BAND_IDX((int)Math.min(Math.max(Math.round(FIRST_HARMONIC_FREQ_HALF_BAND.toDouble() * N_FFT.toInt() * ACCEL_SAMPLING_PERIOD_US.toInt() / 1000000.0), 0), N_FFT.toInt() - 1)),
    HIGHEST_PEAK_BUFFER_FREQ_IDX((int)Math.min(Math.max(Math.round(HIGHEST_PEAK_BUFFER_FREQ.toDouble() * N_FFT.toInt() * ACCEL_SAMPLING_PERIOD_US.toInt() / 1000000.0), 0), N_FFT.toInt() - 1)),

    // Detection parameters: looking for secondary peaks
    HALF_BAND_SECONDARY_PEAK(1.5),                    // When searching for secondary peaks, they should be in this +- this range at an integer multiple of the first peak found

    // Detection parameters: looking for secondary peaks converted to samples
    HALF_BAND_SECONDARY_PEAK_IDX((int)Math.min(Math.max(Math.round(HALF_BAND_SECONDARY_PEAK.toDouble() * N_FFT.toInt() * ACCEL_SAMPLING_PERIOD_US.toInt() / 1000000.0), 0), N_FFT.toInt() - 1)),

    // Buffers around the peaks that should not have high amplitude
    LOWER_BUFFER_ZONE_FREQ(1.0),
    UPPER_BUFFER_ZONE_FREQ(1.5),
    LOWER_BUFFER_ZONE_FREQ_IDX((int)Math.min(Math.max(Math.round(LOWER_BUFFER_ZONE_FREQ.toDouble() * N_FFT.toInt() * ACCEL_SAMPLING_PERIOD_US.toInt() / 1000000.0), 0), N_FFT.toInt() - 1)),
    UPPER_BUFFER_ZONE_FREQ_IDX((int)Math.min(Math.max(Math.round(UPPER_BUFFER_ZONE_FREQ.toDouble() * N_FFT.toInt() * ACCEL_SAMPLING_PERIOD_US.toInt() / 1000000.0), 0), N_FFT.toInt() - 1)),

    // Detection parameters: undersampling
    ALIAS_FIRST_HARMONIC_FLAG(N_FFT.toInt() / 2.0 < CENTRAL_FIRST_HARMONIC_FREQ_IDX.toInt()),     // Determined by sampling freq; true if first harmonic center is aliased, false otherwise; should never undersample to the point where the fundamental aliases
    ALIAS_SECOND_HARMONIC_FLAG(N_FFT.toInt() / 2.0 < 3 * CENTRAL_FUNDAMENTAL_FREQ_IDX.toInt()),     // Determined by sampling freq; true if second harmonic center is aliased, false otherwise; should never undersample to the point where the fundamental aliases
    /** End 15 Hz detection parameters **/

    // Alert history parameters
    ALERT_HISTORY_TITLE("Leg Shaking Alert History"),
    ALERT_HISTORY_XLABEL(""),
    ALERT_HISTORY_YLABEL("Number of Leg Shaking Alerts"),
    ALERT_HISTORY_BIN(2),           // Number of minutes per bin
    ALERT_HISTORY_TIME_RANGE(60),    // Number of minutes in past to show.
    ALERT_HISTORY_LEGSHAKING_TAG("alert_history_legshaking"),

    // constants for login and registering
    EMAIL_JSON_TAG("email"),
    PASSWORD_JSON_TAG("password"),
    NAME_JSON_TAG("name"),
    AGE_JSON_TAG("age"),
    WEIGHT_LBS_JSON_TAG("weight_lbs"),
    HANDEDNESS_JSON_TAG("handedness"),
    GENDER_JSON_TAG("gender"),
    HEIGHT_FT_JSON_TAG("height_ft"),
    HEIGHT_IN_JSON_TAG("height_in"),
    ETHNICITY_JSON_TAG("ethnicity"),
    LONGITUDE_JSON_TAG("longitude"),
    LATITUDE_JSON_TAG("latitude"),
    GEOLOCATION_JSON_TAG("geolocation"),

    // Location service constants
    LOCATION_PERMISSION_ID(99),
    LOCATIONSERVICE_MINTIME(60 * 60 * 1000), // ms
    LOCATIONSERVICE_MINDISTANCE(0),
    LOCATIONSERVICE_TIMERANGE(120 * 60 * 1000),    // Max time (ms) between location ping and current time to be considered a valid current position for the user.

    // Checking if the detectservice is floating
    DETECTION_INACTIVITY_TIME(8 * 1000),    // Maximum time (ms) since last detection call where it is deemed that the detection is service is actually not running.

    // Google geolocation variables
    GEOLOCATIONSERVICE_TAG("GeolocationService"),
    GEOLOCATION_INTENTFILTER_TAG("geolocation"),
    GEOLOCATION_URL("https://www.googleapis.com/geolocation/v1/geolocate?key="),
    GEOLOCATION_INTERVAL(20 * 1000),  // Update location once every hour

    // Proximity Sensor Values
    PROXIMITYSENSOR_FLAG(1),
    PROXIMITYSENSOR_THRESHOLD(0.9), // Above this value, and the phone is not in the pocket.

    ROTATIONVECTOR_PERIOD(3000000),   // microseconds
    ROTATIONVECTOR_FLAG(1),
    ROTATIONVECTOR_THRESHOLD(0.035),

    // Camera sensor Values
    CAMERASENSOR_THRESHOLD(10),  // Above this value, and the phone is not in the pocket.

    // Detected Activity variables
    GOOGLEDETECTEDACTIVITY_STILLNESS_TAG("stillness"),
    GOOGLEDETECTEDACTIVITY_STILLNESS_INTENTFILTER("stillness_factor");   // 1 meaning still 100%, 0 other wise.

    private String strValue;
    private int intValue;
    private double doubleValue;
    private boolean boolValue;

    private AppConstants(String value) {
        this.strValue = value;
    }
    private AppConstants(int value) {
        this.intValue = value;
    }
    private AppConstants(double value) { this.doubleValue = value; }
    private AppConstants(boolean value) { this.boolValue = value; }

    @Override
    public String toString() {
        return this.strValue;
    }

    public int toInt() { return this.intValue; }

    public double toDouble() { return this.doubleValue; }

    public boolean toBoolean() { return this.boolValue; }
}
