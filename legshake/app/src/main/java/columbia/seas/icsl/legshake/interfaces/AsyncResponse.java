package columbia.seas.icsl.legshake.interfaces;

/**
 * Created by stephen on 4/26/2017.
 * For passing back responses from http tasks to activities
 */

public interface AsyncResponse {
    void processFinish(String retVal);
}
