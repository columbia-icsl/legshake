package columbia.seas.icsl.legshake;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import columbia.seas.icsl.legshake.constants.AppConstants;
import columbia.seas.icsl.legshake.interfaces.AsyncResponse;
import columbia.seas.icsl.legshake.util.httpModule;

import org.json.JSONObject;

/**
 * Created by stephen on 2/2/2018.
 */

public class AboutUsFeedbackDialog extends Activity implements AsyncResponse {

    Activity _this = this;

    EditText editTextComment;
    static boolean active = false;
    private final String LOG_TAG = "AboutUsFeedbackDialog";

    String username;
    String password;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        active = true;
        setContentView(R.layout.dialog);
        editTextComment = (EditText) findViewById(R.id.comment);

        // get username
        SharedPreferences sharedPref = this.getApplicationContext().getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        username = sharedPref.getString(getString(R.string.preference_email),"");
        password = sharedPref.getString(getString(R.string.preference_password),"");
    }

    @Override
    protected void onStop() {
        super.onStop();
        active = false;
    }

    //triggered when button submit
    public void submit(View v){
        String comment = editTextComment.getText().toString();
        Log.d(LOG_TAG, comment);

        // Send comment to server
        httpModule.run_http_post_feedback(AppConstants.LOGFEEDBACK_URL.toString(), username, password, comment, this);
    }

    //triggered when button cancel
    public void cancel(View v){
        closeDialog();
    }

    private void closeDialog() {
        this.finish();
    }

    /**
     * Receive results from logging comment. True means that the username was successfully added to database.
     * @param retVal
     */
    public void processFinish(String retVal) {

        try {
            JSONObject json_result = new JSONObject(retVal);
            int result = json_result.getInt(AppConstants.LOGFEEDBACK_TAG.toString());
            Log.d(LOG_TAG, "Feedback success: " + String.valueOf(retVal));

            if(result > 0) {
                Log.i(LOG_TAG, "Feedback successful");
                Toast toast = Toast.makeText(_this, "Feedback received. Thank you!", Toast.LENGTH_SHORT);
                toast.show();
                closeDialog();
            } else {
                Log.i(LOG_TAG, "Feedback unsuccessful, server error");
                Toast toast = Toast.makeText(_this, "Feedback unsuccessfully sent, server error", Toast.LENGTH_SHORT);
                toast.show();
            }
        } catch (Exception e) {
            Log.d(AppConstants.LOGINACTIVITY_TAG.toString(), AppConstants.ERROR_TAG.toString() + e.getMessage());
            Toast toast = Toast.makeText(_this, "Feedback unsuccessfully sent, internal error", Toast.LENGTH_SHORT);
            toast.show();
        }

    }
}
