package columbia.seas.icsl.legshake;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by jordanvega on 11/28/17.
 */

// use this as an inner class like here or as a top-level class
public class HandleServicesReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

    }
    // constructor
    public HandleServicesReceiver(){

    }
}
