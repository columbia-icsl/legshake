package columbia.seas.icsl.legshake;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.util.Log;

import columbia.seas.icsl.legshake.constants.AppConstants;

/**
 * Created by stephen on 2/10/2018.
 */

public class RotationSensorService extends Service implements SensorEventListener {

    private SensorManager mSensorManager;

    private Sensor gameRotation;

    public static float[] rotation = new float[3];

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public void onDestroy() {
        super.onDestroy();

        Log.d(AppConstants.ROTATIONSENSORSERVICE_TAG.toString(), "Shutting off rotation sensor");
        if(mSensorManager != null)
            mSensorManager.unregisterListener(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.d(AppConstants.ROTATIONSENSORSERVICE_TAG.toString(), "Turning on rotation sensor");

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        gameRotation = mSensorManager.getDefaultSensor(Sensor.TYPE_GAME_ROTATION_VECTOR);
        mSensorManager.registerListener(this, gameRotation, AppConstants.ROTATIONVECTOR_PERIOD.toInt());
        Log.d(AppConstants.ROTATIONSENSORSERVICE_TAG.toString(), "Game Max Delay: " + gameRotation.getMaxDelay());
        Log.d(AppConstants.ROTATIONSENSORSERVICE_TAG.toString(), "Game Min Delay: " + gameRotation.getMinDelay());
        Log.d(AppConstants.ROTATIONSENSORSERVICE_TAG.toString(), "Game Max Range: " + gameRotation.getMaximumRange());
        Log.d(AppConstants.ROTATIONSENSORSERVICE_TAG.toString(), "Game Power: " + gameRotation.getPower());

        return super.onStartCommand(intent, flags, startId);
    }

    public void onSensorChanged(SensorEvent event) {

        if(event.sensor.getType() == Sensor.TYPE_GAME_ROTATION_VECTOR) {

            // Save rotation values.
            rotation = event.values;
        }
    }
}
