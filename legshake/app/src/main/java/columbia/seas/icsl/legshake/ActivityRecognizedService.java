package columbia.seas.icsl.legshake;

import android.app.ActivityManager;
import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import columbia.seas.icsl.legshake.constants.AppConstants;
import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;

import java.util.List;

/**
 * Created by jordanvega on 11/14/17.
 */

public class ActivityRecognizedService extends IntentService {

    public ActivityRecognizedService() {
        super("ActivityRecognizedService");
    }

    public ActivityRecognizedService(String name) {
        super(name);
    }
    int frequency;

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        if(intent != null) {
            Bundle b = intent.getExtras();
            if(b != null) {
                boolean is50 = b.getBoolean("f");
                if (is50)
                    frequency = 15;
                else
                    frequency = 50;

                Log.d(AppConstants.ACTIVITYRECOGNIZEDSERVICE_TAG.toString(), "Frequency = " + String.valueOf(frequency));
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if(ActivityRecognitionResult.hasResult(intent)) {
            ActivityRecognitionResult result = ActivityRecognitionResult.extractResult(intent);
            handleDetectedActivities( result.getProbableActivities() );
        }
    }

    private void handleDetectedActivities(List<DetectedActivity> probableActivities) {
        Log.e( "ActivityRecogition", "In");
        Intent intent = new Intent(HandleServices.GOOGLE);
        sendBroadcast(intent);
        boolean isServiceRunning = checkIfDetectServiceIsRunning();
        for( DetectedActivity activity : probableActivities ) {
            int activityType = activity.getType();
            if (activityType == DetectedActivity.STILL ||
                    activityType == DetectedActivity.IN_VEHICLE ||
                    activityType == DetectedActivity.RUNNING ||
                    activityType == DetectedActivity.WALKING ||
                    activityType == DetectedActivity.ON_BICYCLE) {

                String act = "Still ";
                if(activityType == DetectedActivity.IN_VEHICLE)
                    act = "Veh ";
                else if (activityType == DetectedActivity.ON_BICYCLE)
                    act = "Bike ";
                else if (activityType == DetectedActivity.WALKING)
                    act = "Walking ";

                Log.e( "ActivityRecogition", act+ activity.getConfidence() );
                if( activityType == DetectedActivity.STILL && activity.getConfidence() == 100 ) {

                    // Send broadcast to the Handle services method to turn on proximity and rotation if used.
                    Intent stillnessIntent = new Intent(AppConstants.GOOGLEDETECTEDACTIVITY_STILLNESS_INTENTFILTER.toString());
                    stillnessIntent.putExtra(AppConstants.GOOGLEDETECTEDACTIVITY_STILLNESS_TAG.toString(), 1);
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(stillnessIntent);

                    // If detected to be in pocket, keep the legshake detection running
                    if(inPocket()) {
                        Log.d("Services", "In Pocket: Keeping detected activity running even though still");
                        if(!isServiceRunning) {
                            Intent detectLegShake = new Intent(getBaseContext(), ShakeDetectService.class);
                            detectLegShake.putExtra("f", frequency);
                            startService(detectLegShake);
                        }
                    }

                    // otherwise the phone may be sitting on a table, so turn off the detection
                    else {
                        if (isServiceRunning) {
                            Log.d("Services", "Stopping Service From Activity Dectection API");
                            stopService(new Intent(this, ShakeDetectService.class));
                            stopService(new Intent(this, LocationService.class));
                        }
                    }
                }

                // If there is significant movement, Google API will wake up and we can disable our in pocket monitoring
                else if((activityType == DetectedActivity.RUNNING || activityType == DetectedActivity.WALKING ||
                        activityType == DetectedActivity.ON_BICYCLE) && activity.getConfidence() > 50) {

                    // Send broadcast to the Handle services method to turn off proximity and rotation if used.
                    Intent stillnessIntent = new Intent(AppConstants.GOOGLEDETECTEDACTIVITY_STILLNESS_INTENTFILTER.toString());
                    stillnessIntent.putExtra(AppConstants.GOOGLEDETECTEDACTIVITY_STILLNESS_TAG.toString(), 0);
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(stillnessIntent);

                    if(isServiceRunning) {
                        Log.d("Services", "Stopping Service From Activity Dectection API");
                        stopService(new Intent(this, ShakeDetectService.class));
                        stopService(new Intent(this, LocationService.class));
                    }
                }

                // Here, Google API is still running, so we keep our own monitoring off
                else {

                    // Send broadcast to the Handle services method to turn off proximity and rotation if used.
                    Intent stillnessIntent = new Intent(AppConstants.GOOGLEDETECTEDACTIVITY_STILLNESS_INTENTFILTER.toString());
                    stillnessIntent.putExtra(AppConstants.GOOGLEDETECTEDACTIVITY_STILLNESS_TAG.toString(), 0);
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(stillnessIntent);

                    if(!isServiceRunning) {
                        Log.d("Services", "Starting Service Still");
                        Intent detectLegShake = new Intent(getBaseContext(), ShakeDetectService.class);
                        detectLegShake.putExtra("f", frequency);
                        startService(detectLegShake);
                        //startService(new Intent(this, LocationService.class));
                        break;
                    }
                }
            }

            // Some other state, keep our monitoring off
            else{

                // Send broadcast to the Handle services method to turn off proximity and rotation if used.
                Intent stillnessIntent = new Intent(AppConstants.GOOGLEDETECTEDACTIVITY_STILLNESS_INTENTFILTER.toString());
                stillnessIntent.putExtra(AppConstants.GOOGLEDETECTEDACTIVITY_STILLNESS_TAG.toString(), 0);
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(stillnessIntent);

                if(!isServiceRunning) {
                    Log.d("Services", "Starting Service Other");
                    Intent detectLegShake = new Intent(getBaseContext(), ShakeDetectService.class);
                    detectLegShake.putExtra("f", frequency);
                    startService(detectLegShake);
                    //startService(new Intent(this, LocationService.class));
                    break;
                }
            }
        }
    }

    private boolean checkIfDetectServiceIsRunning(){
        Log.d(AppConstants.ACTIVITYRECOGNIZEDSERVICE_TAG.toString(), "Checking if Detection is running");
        boolean runningFlag = false;
        final ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        final List<ActivityManager.RunningServiceInfo> services = activityManager.getRunningServices(Integer.MAX_VALUE);
        for (int i = 0; i < services.size(); i++) {
            if ("columbia.seas.icsl.legshake.ShakeDetectService".equals(services.get(i).service.getClassName())) {
                runningFlag = true;
                break;
            }
        }

        if(runningFlag) {
            Log.d(AppConstants.ACTIVITYRECOGNIZEDSERVICE_TAG.toString(), "Leg Shake Service running");
        } else {
            Log.d(AppConstants.ACTIVITYRECOGNIZEDSERVICE_TAG.toString(), "Leg Shake service is not running");
        }

        // If we found a Shakedetectservice running, then check when was the last time ShakeAnalysis was called.
        // If it's been too long, or has never been called, stop the shakedetectservice and return false as if it is off.
        /*long currentTime = System.currentTimeMillis();
        if(runningFlag && currentTime - ShakeAnalysis.lastCalled > AppConstants.DETECTION_INACTIVITY_TIME.toInt()) {

            Log.d("ActivityRecognized", "Detect Service not responsive");
            Log.d("ActivityRecognized", "Last Called: " + String.valueOf(ShakeAnalysis.lastCalled));
            Log.d("ActivityRecognized", "Current time: " + String.valueOf(currentTime));

            runningFlag = false;
            stopService(new Intent(getBaseContext(), ShakeDetectService.class));
            stopService(new Intent(getBaseContext(), LocationService.class));
        }*/
        return runningFlag;
    }

    // returns true if the rotationsensor service is running
    public boolean checkIfRotationSensorServiceRunning() {

        final ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        final List<ActivityManager.RunningServiceInfo> services = activityManager.getRunningServices(Integer.MAX_VALUE);
        for (int i = 0; i < services.size(); i++) {
            if ("columbia.seas.icsl.legshake.RotationSensorService".equals(services.get(i).service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    // returns true if phone is inferred to be in pocket, by default return true
    public boolean inPocket() {
        boolean returnValue = true;

        if(AppConstants.PROXIMITYSENSOR_FLAG.toInt() > 0) {

            // If the proximity sensor value is less than threshold, then phone may be in pocket
            if(ProximitySensorService.proximity < AppConstants.PROXIMITYSENSOR_THRESHOLD.toDouble()) {

                returnValue = true;

                // Use the rotation vector for refinement
                if(AppConstants.ROTATIONVECTOR_FLAG.toInt() > 0 && checkIfRotationSensorServiceRunning()) {

                    // If the absolute value of the z-axis is greater than threshold, phone is probably in pocket
                    if(Math.abs(RotationSensorService.rotation[2]) < AppConstants.ROTATIONVECTOR_THRESHOLD.toDouble()) {
                        returnValue = false;
                    }
                }
            }

            // If proximity sensor is greater than threshold, then phone is not in pocket
            else {
                returnValue = false;
            }
        }

        Log.d(AppConstants.ACTIVITYRECOGNIZEDSERVICE_TAG.toString(), "In Pocket: " + String.valueOf(returnValue));
        return returnValue;
    }
}
