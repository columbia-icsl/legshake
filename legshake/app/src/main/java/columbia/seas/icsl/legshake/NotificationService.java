package columbia.seas.icsl.legshake;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.Toast;

import columbia.seas.icsl.legshake.constants.AppConstants;
import columbia.seas.icsl.legshake.constants.NotificationConstants;
import columbia.seas.icsl.legshake.interfaces.AsyncResponse;
import columbia.seas.icsl.legshake.util.RawDataStorage;
import columbia.seas.icsl.legshake.util.httpModule;

import org.json.JSONObject;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

public class NotificationService extends Service implements AsyncResponse {
    Notification status;
    private final String LOG_TAG = "NotificationService";
    String username;
    String password;
    private static Queue<RawDataStorage> dataStorageQueue;

    public NotificationService() {
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent.getAction().equals(NotificationConstants.ACTION.STARTFOREGROUND_ACTION)) {
            SharedPreferences sharedPref = this.getApplicationContext().getSharedPreferences(
                    getString(R.string.preference_file_key), Context.MODE_PRIVATE);
            username = sharedPref.getString(getString(R.string.preference_email),"");
            password = sharedPref.getString(getString(R.string.preference_password),"");
            showNotification();
        }
        else if (intent.getAction().equals(NotificationConstants.ACTION.FALSE_NEG)) {
            Log.i(LOG_TAG, "False Neg");
            broadcastFalseNegative("");
            //Send False Negative without comment
        }
        else if (intent.getAction().equals(NotificationConstants.ACTION.FALSE_NEG_WITH_COMMENT)) {
            //Send False Negative with comment
            Intent it = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
            getApplicationContext().sendBroadcast(it);
            if(!SendCommentDialog.active) {
                Intent commentDia = new Intent(this, SendCommentDialog.class);
                commentDia.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                commentDia.setAction(Intent.ACTION_MAIN);
                startActivity(commentDia);
            }
        }

        // Receiving comment from dialog
        else if(intent.getAction().equals(NotificationConstants.ACTION.RETURN_COMMENT_ACTION)) {
            Log.i(LOG_TAG, "Logging False Neg with Comment");
            String comment = intent.getStringExtra(AppConstants.COMMENT_JSON_TAG.toString());
            broadcastFalseNegative(comment);
        }

        //  Pushed icon, go to main activity
        else if(intent.getAction().equals(NotificationConstants.ACTION.NOTIFICATION_ICON)) {
            Intent homeIntent = new Intent(this, MainActivity.class);
            homeIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);
            startActivity(homeIntent);
        }

        return START_STICKY;
    }

    private void showNotification() {
        RemoteViews views = new RemoteViews(getPackageName(),
                R.layout.status_bar);


        views.setViewVisibility(R.id.status_bar_icon, View.VISIBLE);

        Intent notificationIntent = new Intent(this, MainActivity.class);
        notificationIntent.setAction(NotificationConstants.ACTION.MAIN_ACTION);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);


        Intent fnIntent = new Intent(this, NotificationService.class);
        fnIntent.setAction(NotificationConstants.ACTION.FALSE_NEG);
        PendingIntent falseNegPendIntent = PendingIntent.getService(this, 0,
                fnIntent, 0);

        Intent fpIntent = new Intent(this, NotificationService.class);
        fpIntent.setAction(NotificationConstants.ACTION.FALSE_NEG_WITH_COMMENT);
        PendingIntent falsePosIntent = PendingIntent.getService(this, 0,
                fpIntent, 0);

        Intent iconIntent = new Intent(this, NotificationService.class);
        iconIntent.setAction(NotificationConstants.ACTION.NOTIFICATION_ICON);
        PendingIntent iconPendingIntent = PendingIntent.getService(this, 0, iconIntent, 0);

        views.setOnClickPendingIntent(R.id.error, falseNegPendIntent);

        views.setOnClickPendingIntent(R.id.alert, falsePosIntent);

        views.setOnClickPendingIntent(R.id.status_bar_icon, iconPendingIntent);

        views.setOnClickPendingIntent(R.id.status_bar_xml, iconPendingIntent);

        Notification.Builder n = new Notification.Builder(this);
        n.setSmallIcon(R.drawable.waiting_room);
        n.setColor(getResources().getColor(R.color.iconFillColor));
        n.setOnlyAlertOnce(true).setOngoing(true);
        status = n.build();
        status.contentView = views;
        status.flags = Notification.FLAG_ONGOING_EVENT;
        status.contentIntent = pendingIntent;
        startForeground(NotificationConstants.NOTIFICATION_ID.FOREGROUND_SERVICE, status);
    }

    private void broadcastFalseNegative(String comment) {
        if(AppConstants.SERVER_FLAG.toInt() > 0) {
            Log.i(LOG_TAG, "Broadcasting false negative to server");
            Log.i(LOG_TAG, "HI");
            Log.i(LOG_TAG, username);
            Log.i(LOG_TAG, comment);
            Log.i(LOG_TAG, MainActivity.dataStorageQueue.toString());
            Toast.makeText(this, "Logging False Negative to Server....", Toast.LENGTH_SHORT).show();
            httpModule.run_http_post_log_falseNegative(AppConstants.LOGFALSENEGATIVE_URL.toString(), username, password, MainActivity.dataStorageQueue, comment, this);
        }
    }

    private void setUpHTTP() {
        // Structure to store windows for logging false negatives
        dataStorageQueue = new PriorityQueue<>(3, new Comparator<RawDataStorage>() {
            public int compare(RawDataStorage a, RawDataStorage b) {

                // Returns the 'lowest' values first; in this case, we want the smaller timestamp to be returned first
                // because we want past windows to get dumped first
                return a.timestamp - b.timestamp > 0 ? 1: -1;
            }
        });

        SharedPreferences sharedPref = this.getApplicationContext().getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        username = sharedPref.getString(getString(R.string.preference_email),"");
        password = sharedPref.getString(getString(R.string.preference_password),"");
    }

    /**
     * Receive results from falseNegative broadcast.
     * @param retVal
     */
    public void processFinish(String retVal) {
        try{
            JSONObject json_result = new JSONObject(retVal);
            int result = json_result.getInt(AppConstants.LOGFALSENEGATIVE_TAG.toString());

            if(result > 0) {
                Toast.makeText(this, "False Negative Logged to server. Thank you for your feedback.", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "False Negative failed. Server response received.", Toast.LENGTH_SHORT).show();
            }
        } catch(Exception e) {
            Log.i(AppConstants.MAINACTIVITY_TAG.toString(), e.getLocalizedMessage());
            Toast.makeText(this, "Logging failed, internal error", Toast.LENGTH_SHORT).show();
        }
    }

}
