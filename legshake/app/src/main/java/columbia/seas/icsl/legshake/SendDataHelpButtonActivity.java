package columbia.seas.icsl.legshake;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;

/**
 * Created by stephen on 2/4/2018.
 */

public class SendDataHelpButtonActivity extends Activity{

    Activity _this = this;

    EditText editTextComment;
    static boolean active = false;
    private final String LOG_TAG = "AboutUsFeedbackDialog";

    String username;
    String password;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        active = true;
        setContentView(R.layout.activity_send_data_help);

        // get username
        SharedPreferences sharedPref = this.getApplicationContext().getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        username = sharedPref.getString(getString(R.string.preference_email),"");
        password = sharedPref.getString(getString(R.string.preference_password),"");
    }

    @Override
    protected void onStop() {
        super.onStop();
        active = false;
    }


    //triggered when button cancel
    public void activity_send_data_help_close(View v){
        closeDialog();
    }

    private void closeDialog() {
        this.finish();
    }

}
