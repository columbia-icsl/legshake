package columbia.seas.icsl.legshake.util;

/**
 * Created by stephen on 4/22/2017.
 * Class for utility functions; mostly some signal processing shtuffs.
 */

public class Utils {
    /**
     * Given an input time-domain signal, subtract off mean
     * @param data: input data
     * @return whitened_data: Data with mean subtracted off
     */
    public static double[] whiten_data(double[] data) {
        double[] whitened_data = new double[data.length];
        double mean = 0;

        // Compute mean
        for(int i = 0; i < data.length; i++) {
            mean += data[i];
        }
        mean /= data.length;

        // Subtract off mean
        for(int i = 0; i < data.length; i++) {
            whitened_data[i] = data[i] - mean;
        }
        return whitened_data;
    }

    /**
     * Computes autocorrelation function
     * @return
     */
    public static double[] autoCorrelation(Matrix data, double normalizationTerm) {
        double[] output = new double[data.getRowDimension()];
        for(int i = 0; i < data.getRowDimension(); i++) {
            double value = 0;
            for(int k = 0; k < data.getRowDimension() - i; k++) {
                value += data.getMatrix(k, k, 0, 2).times(data.getMatrix(k + i, k + i, 0, 2).transpose()).get(0, 0);
            }
            output[i] = value / normalizationTerm;
        }
        return output;
    }

    /**
     * Returns the input array with the specified number of zeroes padded onto the right.
     * @param inArray - Array to pad
     * @param numPad - Number of zeros to pad onto the right of array
     * @return
     */
    public static double[] zeroPad(double[] inArray, int numPad) {

        // Only pad if a valid number of zeroes to pad is specified
        if(numPad > 0) {
            double[] outArray = new double[inArray.length + numPad];
            System.arraycopy(inArray, 0, outArray, 0, inArray.length);
            return outArray;
        }
        return inArray;
    }

    /**
     * Given an index and the fft size, determine where the sample would correspond to if aliasing occurred
     * @param inIdx - index
     * @param n_fft - fft size
     * @return
     */
    public static int resolveAliasing(int inIdx, int n_fft) {
        int outIdx = inIdx;

        if(inIdx > n_fft / 2);
        return outIdx;
    }
}
