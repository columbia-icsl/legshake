package columbia.seas.icsl.legshake;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.util.Log;

import columbia.seas.icsl.legshake.constants.AppConstants;

/**
 * Created by stephen on 2/9/2018.
 */

public class ProximitySensorService extends Service implements SensorEventListener {

    private SensorManager mSensorManager;
    private Sensor mProximity;

    public static float proximity = 0;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public void onDestroy() {
        super.onDestroy();

        if(mSensorManager != null)
            mSensorManager.unregisterListener(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mProximity = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        mSensorManager.registerListener(this, mProximity, mProximity.getMaxDelay());

        Log.d(AppConstants.PROXIMITYSENSORSERVICE_TAG.toString(), "Proximity Max Delay: " + mProximity.getMaxDelay());
        Log.d(AppConstants.PROXIMITYSENSORSERVICE_TAG.toString(), "Proximity Min Delay: " + mProximity.getMinDelay());
        Log.d(AppConstants.PROXIMITYSENSORSERVICE_TAG.toString(), "Proximity Max Range: " + mProximity.getMaximumRange());
        Log.d(AppConstants.PROXIMITYSENSORSERVICE_TAG.toString(), "Proximity Power: " + mProximity.getPower());

        return super.onStartCommand(intent, flags, startId);
    }

    public void onSensorChanged(SensorEvent event) {

        if(event.sensor.getType() == Sensor.TYPE_PROXIMITY) {
            Log.d(AppConstants.PROXIMITYSENSORSERVICE_TAG.toString(), "Proximity: " + event.values[0]);

            // Save the proximity value
            proximity = event.values[0];
        }

    }
}
