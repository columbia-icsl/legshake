package columbia.seas.icsl.legshake;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.hardware.TriggerEvent;
import android.hardware.TriggerEventListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import columbia.seas.icsl.legshake.constants.AppConstants;
import columbia.seas.icsl.legshake.constants.NotificationConstants;
import columbia.seas.icsl.legshake.interfaces.AsyncResponse;
import columbia.seas.icsl.legshake.util.RawDataStorage;
import columbia.seas.icsl.legshake.util.httpModule;
import com.google.android.gms.location.ActivityRecognitionClient;
import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.BarGraphSeries;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.TreeMap;

public class MainActivity extends Activity implements AsyncResponse {
    Activity _this = this;
    private static String username;
    private static String password;
    boolean isServiceOn = false;
    public static final int NOTIFICATION_ID = 1;
    private static NotificationManager notificationManager;
    private BroadcastReceiver newDataReceiver;
    private BroadcastReceiver stillnessReceiver;
    public static Queue<RawDataStorage> dataStorageQueue;

    private static SensorManager mSensorManager;
    private static Sensor mSensor;

    private static TriggerEventListener mTriggerEventListener;
    private ActivityRecognitionClient mActivityRecognitionClient;
    private TextView status;
    private PendingIntent I;
    private Intent intent;
    Switch g;
    Switch f;

    // Flags for whether or not to collect other data for research purposes. True by default
    Switch allDataCollectionSwitch;
    public static boolean allDataCollectionFlag = true;

    // Sound and vibrate switches
    Switch soundSwitch;
    public static boolean soundSwitchFlag = true;

    Switch vibrateSwitch;
    public static boolean vibrateSwitchFlag = true;

    // Alert history graph variables
    GraphView graph;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");
    private DataPoint[] alertHistoryDataPoints;
    private Date[] alertHistoryXValues;
    private int[] alertHistoryYValues;  // Used for keeping track of number of leg shakes of all time frames
    private int alertHistoryMaxY;
    private Handler alertHistoryHandler;
    private Runnable alertHistoryRunnable;
    private Runnable alertHistoryUpdateLegShakeRunnable;   // Specifically for updating the graph as a leg shake event is detected
    private BarGraphSeries<DataPoint> alertHistorySeries;
    private int alertHistoryNumPoints;
    private BroadcastReceiver alertHistoryLegShakeReceiver;

    // Google geolocation variables
    private Runnable googleGeolocationRunnable;
    private Handler googleGeolocationHandler;
    public static String longitude = "";
    public static String latitude = "";
    public static long locationTimeObtained = 0;
    private BroadcastReceiver geolocationReceiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d("Model", Build.MODEL);
        Log.d("Model", Build.MANUFACTURER);
        // get username
        SharedPreferences sharedPref = this.getApplicationContext().getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        username = sharedPref.getString(getString(R.string.preference_email),"");
        password = sharedPref.getString(getString(R.string.preference_password),"");

        // Structure to store windows for logging false negatives
        dataStorageQueue = new PriorityQueue<>(3, new Comparator<RawDataStorage>() {
            public int compare(RawDataStorage a, RawDataStorage b) {

                // Returns the 'lowest' values first; in this case, we want the smaller timestamp to be returned first
                // because we want past windows to get dumped first
                return a.timestamp - b.timestamp >0 ? 1: -1;
            }
        });

        // Setup broadcast messenger for getting accelerometer data windows back from detect activity for logging, and also for updating the alert history graph whenver
        // there is a leg shake
        newDataReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String username = intent.getStringExtra(AppConstants.EMAIL_JSON_TAG.toString());
                String password = intent.getStringExtra(AppConstants.PASSWORD_JSON_TAG.toString());
                long timestamp = intent.getLongExtra(AppConstants.TIME_JSON_TAG.toString(), 0);
                double[] x_axis = intent.getDoubleArrayExtra(AppConstants.XAXIS_JSON_TAG.toString());
                double[] y_axis = intent.getDoubleArrayExtra(AppConstants.YAXIS_JSON_TAG.toString());
                double[] z_axis = intent.getDoubleArrayExtra(AppConstants.ZAXIS_JSON_TAG.toString());
                double[] eigVals = intent.getDoubleArrayExtra(AppConstants.EIGVALS_JSON_TAG.toString());
                double[] eigVec1 = intent.getDoubleArrayExtra(AppConstants.EIGVEC1_JSON_TAG.toString());
                double[] eigVec2 = intent.getDoubleArrayExtra(AppConstants.EIGVEC2_JSON_TAG.toString());
                double[] eigVec3 = intent.getDoubleArrayExtra(AppConstants.EIGVEC3_JSON_TAG.toString());
                String longitude = intent.getStringExtra(AppConstants.LONGITUDE_JSON_TAG.toString());
                String latitude = intent.getStringExtra(AppConstants.LATITUDE_JSON_TAG.toString());
                double[] gforce_mag_spectrum = intent.getDoubleArrayExtra(AppConstants.GFORCEMAGSPECTRUM_JSON_TAG.toString());
                double[] one_dim_mag_spectrum = intent.getDoubleArrayExtra(AppConstants.ONEDIMMAGSPECTRUM_JSON_TAG.toString());
                TreeMap<String, ArrayList<float[]>> otherSensorData = (TreeMap<String, ArrayList<float[]>>) intent.getSerializableExtra(AppConstants.OTHERSENSORDATA_JSON_TAG.toString());

                RawDataStorage newWindow = new RawDataStorage(username, password, timestamp, x_axis, y_axis, z_axis, eigVals, eigVec1, eigVec2, eigVec3, gforce_mag_spectrum, one_dim_mag_spectrum, longitude, latitude, otherSensorData, 0);

                // See if the queue is full; if so, remove the oldest entry
                if (dataStorageQueue.size() == AppConstants.NUMWINDOWSTOLOG_VAL.toInt()) {
                    dataStorageQueue.poll();
                }
                dataStorageQueue.offer(newWindow);

            }
        };
        LocalBroadcastManager.getInstance(_this).registerReceiver(newDataReceiver, new IntentFilter(AppConstants.NEWACCELDATEUPDATE_INTENTFILTER_TAG.toString()));
        Log.i(AppConstants.MAINACTIVITY_TAG.toString(), "False Negative logging intialized");

        // Set up receiver for updating alert history from leg shake detections
        alertHistoryLegShakeReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                alertHistoryUpdateLegShakeRunnable.run();
            }
        };
        LocalBroadcastManager.getInstance(_this).registerReceiver(alertHistoryLegShakeReceiver, new IntentFilter(AppConstants.ALERT_HISTORY_LEGSHAKING_TAG.toString()));
        Log.i(AppConstants.MAINACTIVITY_TAG.toString(), "Alert history receiver initialized");

        // Setup receiving responses for geolocation
        geolocationReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                longitude = intent.getStringExtra(AppConstants.LONGITUDE_JSON_TAG.toString());
                latitude = intent.getStringExtra(AppConstants.LATITUDE_JSON_TAG.toString());
                locationTimeObtained = System.currentTimeMillis();

                Log.d(AppConstants.MAINACTIVITY_TAG.toString(), "New Longitude received: " + longitude);
                Log.d(AppConstants.MAINACTIVITY_TAG.toString(), "New Latitude received: " + latitude);
            }
        };
        LocalBroadcastManager.getInstance(_this).registerReceiver(geolocationReceiver, new IntentFilter(AppConstants.GEOLOCATION_INTENTFILTER_TAG.toString()));
        Log.i(AppConstants.MAINACTIVITY_TAG.toString(), "Geolocation receiver initialized");

        // General setup
        setContentView(R.layout.activity_main_material);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        final ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        final List<ActivityManager.RunningServiceInfo> services = activityManager.getRunningServices(Integer.MAX_VALUE);
        for (int i = 0; i < services.size(); i++) {

            if ("columbia.seas.icsl.legshake.HandleServices".equals(services.get(i).service.getPackageName())) {
                isServiceOn = true;
            }
        }
        status = (TextView) findViewById(R.id.status);
        g = (Switch) findViewById(R.id.google);
        f = (Switch) findViewById(R.id.frequency);

        if (isServiceOn == false) {
            status.setText("Status: Off");
        }
        else
            status.setText("Status: On");

        // Setup button for sending false negatives
        Button falseNegativeButton = (Button) findViewById(R.id.report_false_negative);
        falseNegativeButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.i(AppConstants.MAINACTIVITY_TAG.toString(), "Sending Data for False negative to server");
                broadcastFalseNegative();
            }
        });

        // Switch button for sending rest of sensor data.
        allDataCollectionSwitch = (Switch) findViewById(R.id.collect_all_data_button);
        allDataCollectionSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                allDataCollectionFlag = isChecked;
                Log.d(AppConstants.MAINACTIVITY_TAG.toString(), "Sending all data flag: " + Boolean.toString(isChecked));
            }
        });

        // Switch button for toggling sound
        soundSwitch = (Switch) findViewById(R.id.sound_button);
        soundSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                soundSwitchFlag = isChecked;
                Log.d(AppConstants.MAINACTIVITY_TAG.toString(), "Sound Switch flag: " + Boolean.toString(isChecked));
            }
        });

        // Switch button for toggling vibration
        vibrateSwitch = (Switch) findViewById(R.id.vibrate_button);
        vibrateSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                vibrateSwitchFlag = isChecked;
                Log.d(AppConstants.MAINACTIVITY_TAG.toString(), "Vibrate Switch flag: " + Boolean.toString(isChecked));
            }
        });

        // Button for the "About us" page
        Button aboutUsButton = (Button) findViewById(R.id.about_us_button);
        aboutUsButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AboutUsFeedbackActivity.class);
                startActivity(intent);
            }
        });

        // Send data help button
        Button send_data_help = (Button) findViewById(R.id.send_data_help_button);
        send_data_help.setBackgroundResource(R.drawable.question_mark);
        send_data_help.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SendDataHelpButtonActivity.class);
                startActivity(intent);
            }
        });


        // Display app version
        TextView version = (TextView) findViewById(R.id.version);
        version.setText("Ver " + AppConstants.VERSION_VAL.toDouble());

        // Setup alert history graph
        graph = (GraphView) findViewById(R.id.alert_history_graph);
        initAlertHistory();
        startAlertHistoryUpdateTimer();

        // Check if location permissions are granted
        //gpsPermissionCheck();

        // Stop the shake detect service
        stopService(new Intent(_this, ShakeDetectService.class));
        stopService(new Intent(_this, LocationService.class));

        // Setup up the power button for detection
        setUpFab();
    }

    @Override
    public void onBackPressed() {
        // Don't let user exit app with "back" button

        moveTaskToBack(true);
    }

    // Method for broadcasting false negatives
    private void broadcastFalseNegative() {
        if(AppConstants.SERVER_FLAG.toInt() > 0) {
            Log.i(AppConstants.MAINACTIVITY_TAG.toString(), "Broadcasting false negative to server");
            Toast.makeText(this, "Logging False Negative to Server....", Toast.LENGTH_SHORT).show();
            httpModule.run_http_post_log_falseNegative(AppConstants.LOGFALSENEGATIVE_URL.toString(), username, password, dataStorageQueue, "",  this);
        }
    }

    /**
     * Receive results from falseNegative broadcast.
     * @param retVal
     */
    public void processFinish(String retVal) {
        try{
            JSONObject json_result = new JSONObject(retVal);
            int result = json_result.getInt(AppConstants.LOGFALSENEGATIVE_TAG.toString());

            if(result > 0) {
                Toast.makeText(this, "False Negative Logged to server. Thank you for your feedback.", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "False Negative failed. Server response received.", Toast.LENGTH_SHORT).show();
            }
        } catch(Exception e) {
            Log.i(AppConstants.MAINACTIVITY_TAG.toString(), e.getLocalizedMessage());
            Toast.makeText(this, "Logging failed, internal error", Toast.LENGTH_SHORT).show();
        }
    }

    // For detectins ignificant motion
    class TriggerListener extends TriggerEventListener {
        public void onTrigger(TriggerEvent event) {
            Log.d("SIGNIFICANT EVENT", "SIGNIFICANT EVENT");
            mSensorManager.requestTriggerSensor(this, mSensor);
        }
    }

    // Method for starting the detection activity.
    private void powerOnSystem() {
        // Make sure that motion services are off.
        stopService(new Intent(getApplicationContext(), ActivityRecognizedService.class));
        stopService(new Intent(getApplicationContext(), MotionDetectService.class));
        stopService(new Intent(getApplicationContext(), ShakeDetectService.class));
        stopService(new Intent(getApplicationContext(), LocationService.class));
        stopService(new Intent(getApplicationContext(), ProximitySensorService.class));
        stopService(new Intent(getApplicationContext(), RotationSensorService.class));
        //stopService(new Intent(getApplicationContext(), GeolocationService.class));

        Log.d("Services", "Starting Handle Services");
        status.setText("Status: On");
        Intent start = new Intent(_this, HandleServices.class);
        start.putExtra("f", f.isChecked());
        if(f.isChecked())
            g.setChecked(false);
        start.putExtra("google",  g.isChecked());

        if(g.isChecked() && f.isChecked()){
            g.setChecked(false);
        }
        g.setClickable(false);
        f.setClickable(false);
        startService(start);

        Intent serviceIntent = new Intent(MainActivity.this, NotificationService.class);
        serviceIntent.setAction(NotificationConstants.ACTION.STARTFOREGROUND_ACTION);
        startService(serviceIntent);
    }

    private void setUpFab() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.floating_action_button);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isServiceOn == false) {

                    //powerOnSystem();
                    Intent detectLegShake = new Intent(getBaseContext(), ShakeDetectService.class);
                    detectLegShake.putExtra("f", 50);
                    startService(detectLegShake);

                    //enableGeolocationTimer();

                    // Initialize alert history graph
                    //initAlertHistory();
                    //startAlertHistoryUpdateTimer();
                }
                else {

                    // Deinitialize alert history graph
                    //deinitAlertHistory();
                    //deinitGeolocationTimer();

                    status.setText("Status: Off");
                    Intent intent = new Intent(HandleServices.OFF);
                    getApplication().sendBroadcast(intent);
                    stopService(new Intent(MainActivity.this, NotificationService.class));
                    stopService(new Intent(MainActivity.this, FalsePositiveNotificationService.class));
                    stopService(new Intent(getApplicationContext(), ActivityRecognizedService.class));
                    stopService(new Intent(getApplicationContext(), MotionDetectService.class));
                    stopService(new Intent(getApplicationContext(), ShakeDetectService.class));
                    stopService(new Intent(getApplicationContext(), LocationService.class));
                    stopService(new Intent(getApplicationContext(), ProximitySensorService.class));
                    stopService(new Intent(getApplicationContext(), RotationSensorService.class));
                    //stopService(new Intent(getApplication(), HandleServices.class));
                    g.setClickable(true);
                    f.setClickable(true);
                }
                isServiceOn = !isServiceOn;
            }
        });

        // By default keep system on
        //powerOnSystem();
        Intent detectLegShake = new Intent(getBaseContext(), ShakeDetectService.class);
        detectLegShake.putExtra("f", 50);
        startService(detectLegShake);
        //enableGeolocationTimer();
        isServiceOn = true;
    }

    @Override
    protected void onDestroy() {

        deinitAlertHistory();
        super.onDestroy();
    }

    /**
     * Initialize the alert_history graph
     */
    private void initAlertHistory() {

        graph.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {

            @Override
            public String formatLabel(double value, boolean isValueX) {
                if(isValueX)
                {
                    return simpleDateFormat.format(new Date((long) value));
                } else {
                    return super.formatLabel(value, isValueX);
                }
            }
        });
        graph.getGridLabelRenderer().setHorizontalLabelsAngle(45);
        graph.setTitle(AppConstants.ALERT_HISTORY_TITLE.toString());
        graph.getGridLabelRenderer().setVerticalAxisTitle(AppConstants.ALERT_HISTORY_YLABEL.toString());

        // Initialize graph to all zeroes upon startup
        alertHistoryNumPoints = AppConstants.ALERT_HISTORY_TIME_RANGE.toInt() / AppConstants.ALERT_HISTORY_BIN.toInt();
        alertHistoryXValues = new Date[alertHistoryNumPoints];
        alertHistoryYValues = new int[alertHistoryNumPoints];
        alertHistoryMaxY = 0;
        alertHistoryDataPoints = new DataPoint[alertHistoryNumPoints];
        Calendar calendar = Calendar.getInstance();
        for(int i = 0; i < alertHistoryNumPoints; i++) {
            Date date = calendar.getTime();
            alertHistoryXValues[alertHistoryNumPoints - (i + 1)] = date;
            alertHistoryYValues[i] = 0;
            alertHistoryDataPoints[alertHistoryNumPoints - (i + 1)] = new DataPoint(date.getTime(), 0);

            Log.d("XAXIS", simpleDateFormat.format(date.getTime()));
            calendar.add(Calendar.MINUTE, -AppConstants.ALERT_HISTORY_BIN.toInt());
        }
        Log.d(AppConstants.MAINACTIVITY_TAG.toString(), "Number in alert history: " + String.valueOf(alertHistoryNumPoints));
        Log.d(AppConstants.MAINACTIVITY_TAG.toString(), "Actual number of entries in alertHistory: " + alertHistoryDataPoints.length);

        // Update graph with initial values
        alertHistorySeries = new BarGraphSeries<>(alertHistoryDataPoints);
        graph.addSeries(alertHistorySeries);

        // Set bounds on graph
        graph.getViewport().setMinX(alertHistoryXValues[0].getTime() - (AppConstants.ALERT_HISTORY_BIN.toInt() * 60 * 1000));
        graph.getViewport().setMaxX(alertHistoryXValues[alertHistoryNumPoints - 1].getTime() + (AppConstants.ALERT_HISTORY_BIN.toInt() * 60 * 1000));
        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setMinY(0);
        graph.getViewport().setMaxY(10);
        graph.getViewport().setYAxisBoundsManual(true);

        // Style of graph
        //alertHistorySeries.setDrawDataPoints(true);
        //alertHistorySeries.setDataPointsRadius(5);

        // set date label formatter
        //graph.getGridLabelRenderer().setNumHorizontalLabels(5); // only 4 because of the space

        //graph.getViewport().setXAxisBoundsManual(true);
    }

    /**
     * Clean up for when the alert history graph is supposed to stop updating
     */
    private void deinitAlertHistory() {
        endAlertHistoryUpdateTimer();
    }

    /**
     * Starts the alert history update timer
     */
    private void startAlertHistoryUpdateTimer() {
        // Initialize runnable to update the alert history graph
        alertHistoryUpdateLegShakeRunnable = new Runnable() {
            @Override
            public void run() {
                alertHistoryUpdateLegShaking();
            }
        };

        // Initialize timer to update the graph (shift time series) right every specified interval
        alertHistoryHandler = new Handler();
        alertHistoryRunnable = new Runnable() {
            @Override
            public void run() {
                updateAlertHistoryTime();
                alertHistoryHandler.postDelayed(alertHistoryRunnable, AppConstants.ALERT_HISTORY_BIN.toInt() * 60 * 1000);
            }
        };

        alertHistoryHandler.postDelayed(alertHistoryRunnable, AppConstants.ALERT_HISTORY_BIN.toInt() * 60 * 1000);
    }

    /**
     * Ends the alert history update timer
     */
    private void endAlertHistoryUpdateTimer() {
        alertHistoryHandler.removeCallbacks(alertHistoryRunnable);
    }

    /**
     * Updates alert history in Time by shifting the graph in time
     */
    private void updateAlertHistoryTime() {

        // Shift graph
        for(int i = 0; i < alertHistoryNumPoints - 1; i++) {
            alertHistoryDataPoints[i] = alertHistoryDataPoints[i + 1];
            alertHistoryYValues[i] = alertHistoryYValues[i + 1];
            alertHistoryXValues[i] = alertHistoryXValues[i + 1];
        }

        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        alertHistoryDataPoints[alertHistoryNumPoints - 1] = new DataPoint(date.getTime(), 0);
        alertHistoryYValues[alertHistoryNumPoints - 1] = 0;
        alertHistoryXValues[alertHistoryNumPoints - 1] = date;

        // Find new max value and keep the max Y axis value to be a little above it to keep the graph looking nice
        int max = 0;
        for(int i = 0; i < alertHistoryNumPoints; i++) {
            if(alertHistoryYValues[i] > max) {
                max = alertHistoryYValues[i];
            }
        }
        int maxResidual = 10 - (max % 10);
        graph.getViewport().setMaxY(max + maxResidual);
        // Set new min and max
        graph.getViewport().setMinX(alertHistoryXValues[0].getTime() - (AppConstants.ALERT_HISTORY_BIN.toInt() * 60 * 1000));
        graph.getViewport().setMaxX(alertHistoryXValues[alertHistoryNumPoints - 1].getTime() + (AppConstants.ALERT_HISTORY_BIN.toInt() * 60 * 1000));
        alertHistoryMaxY = max;

        // Update with timeshift
        alertHistorySeries.resetData(alertHistoryDataPoints);
        graph.removeAllSeries();
        graph.addSeries(alertHistorySeries);

        // Debugging: print out x axis entries
        Log.d(AppConstants.MAINACTIVITY_TAG.toString(), "Update Time");
        Log.d(AppConstants.MAINACTIVITY_TAG.toString(), "Number in alert history: " + String.valueOf(alertHistoryNumPoints));
        Log.d(AppConstants.MAINACTIVITY_TAG.toString(), "Actual number of entries in alertHistory: " + alertHistoryDataPoints.length);
        for(int i = 0; i < alertHistoryNumPoints; i++) {
            Log.d("XAXIS", simpleDateFormat.format(alertHistoryDataPoints[i].getX()));
        }
    }

    /**
     * Update graph after leg shake alert
     */
    private void alertHistoryUpdateLegShaking() {

        // Each time this function is called, log one more leg shake for this time interval
        alertHistoryYValues[alertHistoryNumPoints - 1] = alertHistoryYValues[alertHistoryNumPoints - 1] + 1;

        Log.d("test", alertHistoryYValues[alertHistoryNumPoints - 1] + "");

        // See if this is a new maximum. If so, readjust axes of graph
        if(alertHistoryYValues[alertHistoryNumPoints - 1] > alertHistoryMaxY) {
            alertHistoryMaxY = alertHistoryYValues[alertHistoryNumPoints - 1];
            int maxResidual = 10 - (alertHistoryMaxY % 10);
            graph.getViewport().setMaxY(alertHistoryMaxY  + maxResidual);
        }

        // Update graph plot
        alertHistoryDataPoints[alertHistoryNumPoints - 1] = new DataPoint(alertHistoryXValues[alertHistoryNumPoints - 1].getTime(), alertHistoryYValues[alertHistoryNumPoints - 1]);
        alertHistorySeries.resetData(alertHistoryDataPoints);
        graph.removeAllSeries();
        graph.addSeries(alertHistorySeries);

        // Debugging: print out x axis entries
        Log.d(AppConstants.MAINACTIVITY_TAG.toString(), "Number in alert history: " + String.valueOf(alertHistoryNumPoints));
        Log.d(AppConstants.MAINACTIVITY_TAG.toString(), "Actual number of entries in alertHistory: " + alertHistoryDataPoints.length);
        for(int i = 0; i < alertHistoryNumPoints; i++) {
            Log.d("XAXIS", simpleDateFormat.format(alertHistoryDataPoints[i].getX()));
        }
    }

    // Check if location permission has been granted, otherwise ask for it
    public void gpsPermissionCheck() {

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            new AlertDialog.Builder(this)
                    .setTitle("Grant Location Permission?")
                    .setMessage("We are looking to collect various sensor data from each user for purely research purposes. Your information will not be used for commercial purposes. Would you be able to grant us permission to access your location data?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //Prompt the user once explanation has been shown
                            ActivityCompat.requestPermissions(MainActivity.this,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                    AppConstants.LOCATION_PERMISSION_ID.toInt());
                        }
                    }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(final DialogInterface dialog, final int id) {
                            dialog.cancel();
                        }
                    })
                    .create()
                    .show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        if(requestCode == AppConstants.LOCATION_PERMISSION_ID.toInt()) {

            // If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                // permission was granted, so check to see if bluetooth is enabled.
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                    Log.d(AppConstants.MAINACTIVITY_TAG.toString(), "Location Permission granted");
                    gpsStatusCheck();
                }

            } else {
                Log.d(AppConstants.MAINACTIVITY_TAG.toString(), "Location Permission not granted");
            }

        }
    }

    // Check if gps is turned on, if not ask for it
    public void gpsStatusCheck() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();

        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    // Enables the timer to query for geolocation using google geolocation api
    private void enableGeolocationTimer() {

        // Initialize timer to call google geolocation api every interval to update coarse location estimates
        googleGeolocationHandler = new Handler();
        googleGeolocationRunnable = new Runnable() {
            @Override
            public void run() {

                // Start service to call geolocation api
                startService(new Intent(MainActivity.this, GeolocationService.class));
                googleGeolocationHandler.postDelayed(googleGeolocationRunnable, AppConstants.GEOLOCATION_INTERVAL.toInt());
            }
        };

        googleGeolocationHandler.postDelayed(googleGeolocationRunnable, 0);
    }

    private void deinitGeolocationTimer() {
        googleGeolocationHandler.removeCallbacks(googleGeolocationRunnable);
    }
}