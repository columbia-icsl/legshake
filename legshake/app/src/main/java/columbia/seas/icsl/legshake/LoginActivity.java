package columbia.seas.icsl.legshake;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import columbia.seas.icsl.legshake.constants.AppConstants;
import columbia.seas.icsl.legshake.interfaces.AsyncResponse;
import columbia.seas.icsl.legshake.util.httpModule;

import org.json.JSONObject;

import java.security.NoSuchAlgorithmException;

/**
 * Created by stephen on 1/31/2018.
 */

public class LoginActivity extends Activity implements AsyncResponse {

    Context context;
    Activity _this = this;

    private String email_tmp;
    private String password_tmp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this.getApplicationContext();

        setContentView(R.layout.activity_login);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Button loginButton = (Button) findViewById(R.id.activity_login_LoginButton);
        Button cancelButton = (Button) findViewById(R.id.activity_login_CancelButton);

        // Email and password entries
        final EditText emailEditText = (EditText) findViewById(R.id.activity_login_email);
        final EditText passwordEditText = (EditText) findViewById(R.id.activity_login_password);

        // Login with credentials
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Ensure there is an email and password
                if(emailEditText.getText().toString().matches("")) {
                    Toast.makeText(_this, "Please enter your email", Toast.LENGTH_SHORT).show();
                } else if(passwordEditText.getText().toString().matches("")) {
                    Toast.makeText(_this, "Please enter your password", Toast.LENGTH_SHORT).show();
                }

                // Email and password present, so begin login process process
                else {

                    String email = emailEditText.getText().toString();
                    String password = passwordEditText.getText().toString();

                    Log.d("email", emailEditText.getText().toString());
                    Log.d("pass", passwordEditText.getText().toString());

                    ////////////////////////////////////////////////
                    try {
                        password = SHAhash.sha1(password);
                    } catch (NoSuchAlgorithmException e) {
                        password = password;
                    }
                    ////////////////////////////////////////////////

                    email_tmp = email;
                    password_tmp = password;
                    login(email, password);
                }
            }
        });

        // Go back to launch page
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(LoginActivity.this, FrontPageActivity.class);
                _this.startActivity(intent);
                _this.finish();
            }
        });
    }

    /**
     * Attempt to login
     * @param email
     * @param password
     */
    protected void login(String email, String password) {
        Log.i(AppConstants.LOGINACTIVITY_TAG.toString(), "Beginning login process");

        httpModule.run_http_post_login(AppConstants.LOGIN_URL.toString(), email, password, this);
    }

    /**
     * Receive results from login. True means that the username was successfully added to database.
     * @param retVal
     */
    public void processFinish(String retVal) {
        try {
            JSONObject json_result = new JSONObject(retVal);
            int result = json_result.getInt(AppConstants.LOGIN_JSON_TAG.toString());
            Log.d(AppConstants.HTTP_TAG.toString(), "Login Success: " + String.valueOf(retVal));

            // Successful login
            if(result > 0) {
                Log.i(AppConstants.LOGINACTIVITY_TAG.toString(), "Login successful");
                Toast toast = Toast.makeText(_this, "Login successful", Toast.LENGTH_SHORT);
                toast.show();

                // Save username, password, and apikey so that you don't have to relogin every single time.
                Log.i(AppConstants.LOGINACTIVITY_TAG.toString(), "Saving user, password, and geolocation api key");
                String geoApiKey = json_result.getString(AppConstants.GEOLOCATION_JSON_TAG.toString());
                saveProfile(email_tmp, password_tmp, geoApiKey);

                //startService(new Intent(this, ShakeDetectService.class));
                this.startActivity(new Intent(LoginActivity.this, MainActivity.class));
                this.finish();
            }

            // Unsuccessful
            else if(result == 0) {
                Log.i(AppConstants.LOGINACTIVITY_TAG.toString(), "Invalid username/password");
                Toast toast = Toast.makeText(_this, "Invalid username/password", Toast.LENGTH_SHORT);
                toast.show();
            }

            // Internal error
            else {
                Log.d(AppConstants.LOGINACTIVITY_TAG.toString(), "Login unsuccessful, server error");
                Toast toast = Toast.makeText(_this, "Login unsuccessful, server error", Toast.LENGTH_SHORT);
                toast.show();
            }
        } catch (Exception e) {
            Log.d(AppConstants.LOGINACTIVITY_TAG.toString(), AppConstants.ERROR_TAG.toString() + e.getMessage());
            Toast toast = Toast.makeText(_this, "Login unsuccessful, internal error", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    protected void saveProfile(String email, String password, String geolocationApiKey){
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.preference_email), email);
        editor.putString(getString(R.string.preference_password), password);
        editor.putString(getString(R.string.preference_geolocationapiKey), geolocationApiKey);
        editor.commit();
    }
}
