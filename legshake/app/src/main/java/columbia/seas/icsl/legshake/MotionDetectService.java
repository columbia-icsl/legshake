package columbia.seas.icsl.legshake;

import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;

import columbia.seas.icsl.legshake.constants.AppConstants;

import java.util.List;

/**
 * Created by yanlu on 11/2/16.
 * Keep running as back ground service, by implementing the batching, the service will be wake up every 6 second
 * When week up, it will collect acceleration data stored in batching, and send the data to ShakeAnalysis class for further processing
 */

public class MotionDetectService extends Service implements SensorEventListener {

    private SensorManager mSensorManager;
    private Sensor mSensor;
    private PowerManager.WakeLock wakeLock = null;
    int ptr=0;

    //  Decide the size of to be analysis, should be a power of 2
    private static int N = 8;
    //  Decide the sleep time
    private static int T;
    private static String userId;
    private static String password;

    // If power is above this threshold, then there is significant motion.
    private static double activityPowerThreshold = 0.002;

    // gForce
    double[] val;

    // Raw 3 axis data
    double[][] raw_val;

    int frequency;

    private long curr_time = 0;
    private long prev_time = 0;


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        // Set up accelerometer sensor data collection
        SharedPreferences sharedPref = this.getApplicationContext().getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        userId = sharedPref.getString(getString(R.string.preference_email),"");
        password = sharedPref.getString(getString(R.string.preference_password),"");
        PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                "MyWakelockTag");
        if (wakeLock.isHeld() == false)
            wakeLock.acquire();
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        float power = mSensor.getPower();
        if (mSensor.getFifoMaxEventCount() > 512) {T = 6;}
        else {T = 3;}
        val = new double[N];
        raw_val = new double[N][3];
        mSensorManager.registerListener(this, mSensor, 66666, T * 1000000 /* T * 1 seconds */);
    }

    public void onSensorChanged(SensorEvent event) {
        //Right in here is where you put code to read the current sensor values and
        //update any views you might have that are displaying the sensor information
        //You'd get accelerometer values like this:
        if (event.sensor.getType() != Sensor.TYPE_GYROSCOPE)
            return;

        double x = event.values[0] / 9.81;
        double y = event.values[1] / 9.81;
        double z = event.values[2] / 9.81;

        raw_val[ptr][0] = x;
        raw_val[ptr][1] = y;
        raw_val[ptr][2] = z;

        ptr = (ptr + 1) % N;

        if(ptr == 0) {

            prev_time = curr_time;
            curr_time = System.currentTimeMillis();
            Log.d("True Sampling Rate", "" + 1.0 * N / (curr_time - prev_time));

            // Subtract mean off of samples
            double mean_x = 0;
            double mean_y = 0;
            double mean_z = 0;
            for(int i = 0; i < N; i++) {
                mean_x = mean_x + raw_val[i][0];
                mean_y = mean_y + raw_val[i][1];
                mean_z = mean_z + raw_val[i][2];
            }
            mean_x = mean_x / N;
            mean_y = mean_y / N;
            mean_z = mean_z / N;
            for(int i = 0; i < N; i++) {
                raw_val[i][0] = raw_val[i][0] - mean_x;
                raw_val[i][1] = raw_val[i][1] - mean_y;
                raw_val[i][2] = raw_val[i][2] - mean_z;
            }

            // Find power
            double power = 0;
            for(int i = 0; i < N; i++) {
                power = power + Math.abs(raw_val[i][0]) + Math.abs(raw_val[i][1]) + Math.abs(raw_val[i][2]);
            }

            Log.d("Motion Dect", "Power: " + Double.valueOf(power));
            if(power > activityPowerThreshold) {
                Log.d("Motion Dect", "Not Still");
                if(!checkIfDetectServiceIsRunning()) {
                    Log.d("Motion Dect", "Starting Shake Detect Service");
                    Intent detectLegShake = new Intent(getBaseContext(), ShakeDetectService.class);
                    detectLegShake.putExtra("f", frequency);
                    startService(detectLegShake);
                    //Intent locationServiceIntent = new Intent(getBaseContext(), LocationService.class);
                    //startService(locationServiceIntent);
                }
            }
            else{
                if(checkIfDetectServiceIsRunning()) {
                    Log.d("Motion Dect", "Stopping Shake Detect service");
                    stopService(new Intent(getBaseContext(), ShakeDetectService.class));
                    stopService(new Intent(getBaseContext(), LocationService.class));
                }
            }

        }
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public void onDestroy() {
        super.onDestroy();
        if (wakeLock != null) {
            wakeLock.release();
            wakeLock = null;
        }
        mSensorManager.unregisterListener(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(intent != null) {
            Bundle b = intent.getExtras();
            if(b != null) {
                boolean is50 = b.getBoolean("f");
                if (is50)
                    frequency = 15;
                else
                    frequency = 50;
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }

    private boolean checkIfDetectServiceIsRunning(){
        boolean runningFlag = false;
        final ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        final List<ActivityManager.RunningServiceInfo> services = activityManager.getRunningServices(Integer.MAX_VALUE);
        for (int i = 0; i < services.size(); i++) {
            if ("columbia.seas.icsl.legshake.ShakeDetectService".equals(services.get(i).service.getClassName())) {
                runningFlag = true;
                break;
            }
        }

        // If we found a Shakedetectservice running, then check when was the last time ShakeAnalysis was called.
        // If it's been too long, or has never been called, stop the shakedetectservice and return false as if it is off.
        /*long currentTime = System.currentTimeMillis();
        if(runningFlag && currentTime - ShakeAnalysis.lastCalled > AppConstants.DETECTION_INACTIVITY_TIME.toInt()) {

            Log.d("MotionDetect", "Detect Service not responsive");
            Log.d("MotionDetect", "Last Called: " + String.valueOf(ShakeAnalysis.lastCalled));
            Log.d("MotionDetect", "Current time: " + String.valueOf(currentTime));

            runningFlag = false;
            stopService(new Intent(getBaseContext(), ShakeDetectService.class));
            stopService(new Intent(getBaseContext(), LocationService.class));
        }*/
        return runningFlag;
    }
}
