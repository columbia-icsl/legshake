package columbia.seas.icsl.legshake;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONObject;

import columbia.seas.icsl.legshake.constants.AppConstants;
import columbia.seas.icsl.legshake.interfaces.AsyncResponse;
import columbia.seas.icsl.legshake.util.httpModule;

/**
 * Created by stephen on 2/6/2018.
 *
 * Started to get geolocation of user.
 */

public class GeolocationService extends Service implements AsyncResponse{
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();

        // Get saved API key
        SharedPreferences sharedPref = this.getApplicationContext().getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        String apiKey = sharedPref.getString(getString(R.string.preference_geolocationapiKey),"");

        Log.d(AppConstants.GEOLOCATION_URL.toString(), "Geolocation Services initialized");

        // Run geolocation
        httpModule.run_http_post_to_server(AppConstants.GEOLOCATION_URL.toString() + apiKey, new JSONObject());
    }

    @Override
    public void processFinish(String retVal) {

        // Try to get longitude and latitude
        String longitude = "";
        String latitude = "";
        try{
            JSONObject json_result = new JSONObject(retVal);
            longitude = String.valueOf(json_result.getJSONObject("location").getDouble("lng"));
            latitude = String.valueOf(json_result.getJSONObject("location").getDouble("lat"));

            Log.d(AppConstants.GEOLOCATIONSERVICE_TAG.toString(), "New latitude: " + latitude);
            Log.d(AppConstants.GEOLOCATIONSERVICE_TAG.toString(), "New longitude: " + longitude);

        } catch(Exception e) {
            Log.i(AppConstants.GEOLOCATIONSERVICE_TAG.toString(), e.getLocalizedMessage());
        }

        // Send new longitude and latitude back to main
        Intent intent = new Intent(AppConstants.GEOLOCATION_INTENTFILTER_TAG.toString());
        intent.putExtra(AppConstants.LONGITUDE_JSON_TAG.toString(), longitude);
        intent.putExtra(AppConstants.LATITUDE_JSON_TAG.toString(), latitude);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

        // Destroy this service afterwards
        this.stopSelf();
    }
}
