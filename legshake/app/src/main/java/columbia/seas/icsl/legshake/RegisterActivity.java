package columbia.seas.icsl.legshake;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import columbia.seas.icsl.legshake.constants.AppConstants;
import columbia.seas.icsl.legshake.interfaces.AsyncResponse;
import columbia.seas.icsl.legshake.util.httpModule;

import org.json.JSONObject;

import java.security.NoSuchAlgorithmException;

/**
 * Created by stephen on 1/31/2018.
 */

public class RegisterActivity extends Activity implements AsyncResponse{

    private Context context;
    Activity _this = this;
    private String genderSelection = "";
    private String handednessSelection = "";
    private String ethnicitySelection = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this.getApplicationContext();

        setContentView(R.layout.activity_register);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Button registerButton = (Button) findViewById(R.id.activity_register_RegisterButton);
        Button cancelButton = (Button) findViewById(R.id.activity_register_CancelButton);

        // Required Entries
        final EditText emailEditText = (EditText) findViewById(R.id.activity_register_email);
        final EditText passwordEditText = (EditText) findViewById(R.id.activity_register_password);

        // Optional entries
        final EditText nameEditText = (EditText) findViewById(R.id.activity_register_name);
        final EditText ageEditText = (EditText) findViewById(R.id.activity_register_age);
        final EditText weightEditText = (EditText) findViewById(R.id.activity_register_weight);
        final EditText heightFtEditText = (EditText) findViewById(R.id.activity_register_height_ft);
        final EditText heightInEditText = (EditText) findViewById(R.id.activity_register_height_inches);
        Spinner ethnicityDropdown = (Spinner) findViewById(R.id.activity_register_ethnicity);
        Spinner genderDropdown = (Spinner) findViewById(R.id.activity_register_gender);
        Spinner handednessDropdown = (Spinner) findViewById(R.id.activity_register_handedness);

        // Setup drop-down for gender
        String[] itemsGender = new String[] {"", "M", "F", "Other"};
        ArrayAdapter<String> adapterGender = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, itemsGender);
        genderDropdown.setAdapter(adapterGender);
        genderDropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                genderSelection = (String) parent.getItemAtPosition(position);
                //Log.v("gender", (String) parent.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });


        // Setup drop-down for Handedness
        String[] itemsHandedness = new String[] {"", "Right-Handed", "Left-Handed"};
        ArrayAdapter<String> adapterHandedness = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, itemsHandedness);
        handednessDropdown.setAdapter(adapterHandedness);
        handednessDropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                handednessSelection = (String) parent.getItemAtPosition(position);
                //Log.v("handedness", (String) parent.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

        // Setup dropdown for ethnicity
        String[] itemsEthnicity = new String[] {"", "White", "Asian", "Native American", "African", "Hispanic", "Latino", "Middle Eastern", "Mixed"};
        ArrayAdapter<String> adapterEthnicity = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, itemsEthnicity);
        ethnicityDropdown.setAdapter(adapterEthnicity);
        ethnicityDropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ethnicitySelection = (String) parent.getItemAtPosition(position);
                //Log.v("handedness", (String) parent.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

        // Register with input credentials
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Ensure there is an email and password
                if(emailEditText.getText().toString().matches("")) {
                    Toast.makeText(_this, "Please enter a valid email", Toast.LENGTH_SHORT).show();
                } else if(passwordEditText.getText().toString().matches("")) {
                    Toast.makeText(_this, "Please enter a password", Toast.LENGTH_SHORT).show();
                }

                // Email and password present, so begin registration process
                else {

                    int weight_lbs = -1;
                    int age = -1;
                    int height_ft_entry = -1;
                    int height_in_entry = -1;
                    String email = emailEditText.getText().toString();
                    String password = passwordEditText.getText().toString();
                    String name = nameEditText.getText().toString();
                    String ethnicity = ethnicitySelection;
                    String gender = genderSelection;
                    String handedness = handednessSelection;

                    ////////////////////////////////////////////////
                    try {
                        password = SHAhash.sha1(password);
                    } catch (NoSuchAlgorithmException e) {
                        password = password;
                    }
                    ////////////////////////////////////////////////

                    // Convert weight to a number
                    if(!weightEditText.getText().toString().matches("")) {
                        weight_lbs = Integer.valueOf(weightEditText.getText().toString());
                    }

                    // Convert age to a number
                    if(!ageEditText.getText().toString().matches("")) {
                        age = Integer.valueOf(ageEditText.getText().toString());
                    }

                    // Convert height entries into a single height value in terms of inches
                    if(!heightFtEditText.getText().toString().matches("")) {
                        height_ft_entry = Integer.valueOf(heightFtEditText.getText().toString()) * 12;
                    }
                    else {
                        height_ft_entry = 0;
                    }

                    if(!heightInEditText.getText().toString().matches("")) {
                        height_in_entry = Integer.valueOf(heightInEditText.getText().toString());
                    }
                    int height_in_total = height_ft_entry + height_in_entry;

                    Log.d("email", emailEditText.getText().toString());
                    Log.d("pass", passwordEditText.getText().toString());
                    Log.d("name", nameEditText.getText().toString());
                    Log.d("age", ageEditText.getText().toString());
                    Log.d("weight", weightEditText.getText().toString());
                    Log.d("height_ft", heightFtEditText.getText().toString());
                    Log.d("height_in", heightInEditText.getText().toString());
                    Log.d("height_total_in", String.valueOf(height_in_total));
                    Log.d("ethnicity", ethnicitySelection);
                    Log.d("gender", genderSelection);
                    Log.d("handedness", handednessSelection);

                    register(email, password, name, handedness, age, weight_lbs, height_in_total, ethnicity, gender);
                }
            }
        });

        // Go back to launch page
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(RegisterActivity.this, FrontPageActivity.class);
                _this.startActivity(intent);
                _this.finish();
            }
        });
    }

    /**
     * Starts the name registration process. Checks with server to see if the user exists in database.
     * If not, then the server will automatically add to database.
     * @param
     */
    protected void register(String email, String password, String name, String handedness, int age, int weight, int height_in, String ethnicity, String gender) {
        Log.i(AppConstants.REGISTERACTIVITY_TAG.toString(), "Beginning Registration");

        httpModule.run_http_post_register(AppConstants.REGISTRATION_URL.toString(), email, password, name, handedness, age, weight, height_in, ethnicity, gender, this);
    }

    /**
     * Receive results from registration. True means that the username was successfully added to database.
     * @param retVal
     */
    public void processFinish(String retVal) {
        try {
            JSONObject json_result = new JSONObject(retVal);
            int result = json_result.getInt(AppConstants.REGISTER_JSON_TAG.toString());
            Log.d(AppConstants.HTTP_TAG.toString(), "Registration Success: " + String.valueOf(retVal));

            // Successful registration
            if(result > 0) {
                Log.i(AppConstants.REGISTERACTIVITY_TAG.toString(), "Registration successful");
                Toast toast = Toast.makeText(_this, "Registration successful, please log in", Toast.LENGTH_SHORT);
                toast.show();

                //startService(new Intent(this, ShakeDetectService.class));
                this.startActivity(new Intent(RegisterActivity.this, FrontPageActivity.class));
                this.finish();
            }

            // Email taken
            else if(result == 0) {
                Log.i(AppConstants.REGISTERACTIVITY_TAG.toString(), "Email taken");
                Toast toast = Toast.makeText(_this, "Email already in use", Toast.LENGTH_SHORT);
                toast.show();
            }

            // Internal error
            else {
                Log.d(AppConstants.REGISTERACTIVITY_TAG.toString(), "Registration unsuccessful, server error");
                Toast toast = Toast.makeText(_this, "Registration unsuccessful, server error", Toast.LENGTH_SHORT);
                toast.show();
            }
        } catch (Exception e) {
            Log.d(AppConstants.REGISTERACTIVITY_TAG.toString(), AppConstants.ERROR_TAG.toString() + e.getMessage());
            Toast toast = Toast.makeText(_this, "Registration unsuccessful, internal error", Toast.LENGTH_SHORT);
            toast.show();
        }
    }
}
