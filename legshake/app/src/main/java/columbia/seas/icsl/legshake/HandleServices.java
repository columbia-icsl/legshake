package columbia.seas.icsl.legshake;

import android.app.ActivityManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.location.ActivityRecognitionClient;

import net.danlew.android.joda.JodaTimeAndroid;

import org.joda.time.LocalTime;
import org.joda.time.Seconds;

import java.util.List;
import java.util.Stack;
import java.util.Timer;
import java.util.TimerTask;

import columbia.seas.icsl.legshake.constants.AppConstants;

/**
 * Created by jordanvega on 11/27/17.
 */

public class HandleServices extends Service {

    private ActivityRecognitionClient mActivityRecognitionClient;
    private PendingIntent pendingIntent;
    HandleServicesReceiver mReceiver;
    public static String GOOGLE = "google";
    public static String OFF = "off";
    private Stack<LocalTime> stack;
    private Timer timer;
    private BroadcastReceiver stillnessReceiver;

    boolean freq;
    boolean google;

    private PowerManager.WakeLock wakeLock = null;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        JodaTimeAndroid.init(getApplicationContext());
        // get an instance of the receiver in your service
        super.onCreate();

        PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                "MyWakelockTag");
        if (wakeLock.isHeld() == false)
            wakeLock.acquire();

        mReceiver = new HandleServicesReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(HandleServices.OFF)) {
                    Log.e("Motion Dect", "Stopping Services");
                    if (mActivityRecognitionClient != null) {
                        mActivityRecognitionClient.removeActivityUpdates(pendingIntent);
                    }
                    stopService(new Intent(getApplicationContext(), ActivityRecognizedService.class));
                    stopService(new Intent(getApplicationContext(), MotionDetectService.class));
                    stopService(new Intent(getApplicationContext(), ShakeDetectService.class));
                    stopService(new Intent(getApplicationContext(), LocationService.class));
                    stopService(new Intent(getApplicationContext(), ProximitySensorService.class));
                    stopService(new Intent(getApplicationContext(), RotationSensorService.class));
                    if (timer != null)
                        timer.cancel();
                } else if (intent.getAction().equals(HandleServices.GOOGLE)) {
                    Log.e("Motion Dect", "Google ping");
                    LocalTime localTime = new LocalTime();
                    if (stack != null) {
                        Log.e("Motion Dect", "Adding to stack");
                        stack.push(localTime);
                    }
                    //checkStack();
                }
            }
        };

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(HandleServices.GOOGLE);
        intentFilter.addAction(HandleServices.OFF);
        getApplication().registerReceiver(mReceiver, intentFilter);


        // Setup broadcast messenger for receiving updates on the "stillness" state of Detected Activity Api
        stillnessReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                int stillness = intent.getIntExtra(AppConstants.GOOGLEDETECTEDACTIVITY_STILLNESS_TAG.toString(), 0);

                // Complete still; start rotationsensor service if it hasn't been started yet.
                if(stillness > 0) {

                    if(!checkIfRotationSensorServiceRunning() && AppConstants.ROTATIONVECTOR_FLAG.toInt() > 0) {

                        // Must also check if proximity sensor is below threshold; only start if there is a chance the phone is in pocket (below threshold)
                        if(ProximitySensorService.proximity < AppConstants.PROXIMITYSENSOR_THRESHOLD.toDouble()) {
                            startService(new Intent(getApplicationContext(), RotationSensorService.class));
                        }
                    }

                    // If the rotation service is running, but there is no chance that the phone is in pocket (proximity sensor has a high value and rotation sensor has low z value), turn it off
                    else if(checkIfRotationSensorServiceRunning() && (ProximitySensorService.proximity > AppConstants.PROXIMITYSENSOR_THRESHOLD.toDouble() || RotationSensorService.rotation[2] < AppConstants.ROTATIONVECTOR_THRESHOLD.toDouble())) {
                        stopService(new Intent(getApplicationContext(), RotationSensorService.class));
                    }
                }

                // Not completely still, so turn off the rotation vector
                else {

                    stopService(new Intent(getApplicationContext(), RotationSensorService.class));
                }

            }
        };
        LocalBroadcastManager.getInstance(this.getApplicationContext()).registerReceiver(stillnessReceiver, new IntentFilter(AppConstants.GOOGLEDETECTEDACTIVITY_STILLNESS_INTENTFILTER.toString()));
        Log.i(AppConstants.HANDLESERVICES_TAG.toString(), "Stillness Receiver initialized.");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (wakeLock != null) {
            wakeLock.release();
            wakeLock = null;
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("HandleServ", "Starting Service...");
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                Log.d("HandleServ", "Starting motion...");
                freq = intent.getExtras().getBoolean("f");
                google = intent.getExtras().getBoolean("google");

                Intent motion = new Intent(getBaseContext(), MotionDetectService.class);
                motion.putExtra("f", freq);
                //startService(motion);

                if (google) {
                    Log.d("HandleServ", "Starting Google...");
                    Context c = getApplicationContext();
                    mActivityRecognitionClient = new ActivityRecognitionClient(c);
                    intent = new Intent(c, ActivityRecognizedService.class);
                    intent.putExtra("f", freq);
                    pendingIntent = PendingIntent.getService(c, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                    mActivityRecognitionClient.requestActivityUpdates(0, pendingIntent);
                    runCheckGoogleStatusTimer();
                }
            }
        }
        Log.e("MotionDect", "New Stack");
        stack = new Stack<>();

        // Start Proximity Sensor
        if(AppConstants.PROXIMITYSENSOR_FLAG.toInt() > 0) {
            Intent proximityIntent = new Intent(getApplicationContext(), ProximitySensorService.class);
            startService(proximityIntent);
        }

        return super.onStartCommand(intent, flags, startId);
    }

    private void runCheckGoogleStatusTimer() {
        Log.e("Timer", "Created Timer");
        final Handler handler=new Handler();
        timer = new Timer();
        TimerTask doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @SuppressWarnings("unchecked")
                    public void run() {
                        try {
                            Log.e("Timer", "Checking google status");
                            if(runMotionFromGoogleStatus()) {
                                Log.e("Timer", "Resetting Google Detected Activity from timer");
                                resetGoogleDetectedActivityApi();
                                //startService(new Intent(getApplicationContext(), MotionDetectService.class));
                                stack.clear();
                            }
                        }
                        catch (Exception e) {
                            // TODO Auto-generated catch block
                        }
                    }
                });
            }
        };
        timer.schedule(doAsynchronousTask, 0, 10000);

    }
    public boolean runMotionFromGoogleStatus() {
        if(stack.size() > 0) {
            LocalTime t0 = stack.peek();
            LocalTime t1 = new LocalTime();
            int diff = Math.abs(Seconds.secondsBetween(t1, t0).getSeconds());
            if (diff > 7)
                return true;
            else
                return false;
        }

        // If stack is empty, then return true to prompt to start google detected activity
        return true;
    }

    public void resetGoogleDetectedActivityApi() {

        Log.d(AppConstants.HANDLESERVICES_TAG.toString(), "Resetting Google Detected Api");

        // Google DetectedActivityApi off
        if(mActivityRecognitionClient != null && pendingIntent != null) {
            mActivityRecognitionClient.removeActivityUpdates(pendingIntent);
        }

        // Turn it back on.
        Context c = getApplicationContext();
        mActivityRecognitionClient = new ActivityRecognitionClient(c);
        Intent intent = new Intent(c, ActivityRecognizedService.class);
        intent.putExtra("f", freq);
        pendingIntent = PendingIntent.getService(c, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        mActivityRecognitionClient.requestActivityUpdates(0, pendingIntent);
    }

    // returns true if the rotationsensor service is running
    public boolean checkIfRotationSensorServiceRunning() {

        final ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        final List<ActivityManager.RunningServiceInfo> services = activityManager.getRunningServices(Integer.MAX_VALUE);
        for (int i = 0; i < services.size(); i++) {
            if ("columbia.seas.icsl.legshake.RotationSensorService".equals(services.get(i).service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}

/*public class HandleServices extends Service{

    private ActivityRecognitionClient mActivityRecognitionClient;
    private PendingIntent pendingIntent;
    HandleServicesReceiver mReceiver;
    public static String GOOGLE = "google";
    public static String OFF = "off";
    private Stack<LocalTime> stack;
    private Timer timer;

    boolean freq;
    boolean google;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        JodaTimeAndroid.init(getApplicationContext());
        // get an instance of the receiver in your service
        super.onCreate();
        mReceiver = new HandleServicesReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.getAction().equals(HandleServices.OFF)) {
                    Log.e("Motion Dect", "Stopping Services");
                    if(mActivityRecognitionClient != null) {
                        mActivityRecognitionClient.removeActivityUpdates(pendingIntent);
                    }
                    stopService(new Intent(getApplicationContext(), ActivityRecognizedService.class));
                    stopService(new Intent(getApplicationContext(), MotionDetectService.class));
                    stopService(new Intent(getApplicationContext(), ShakeDetectService.class));
                    stopService(new Intent(getApplicationContext(), LocationService.class));
                    if(timer != null)
                        timer.cancel();
                }
                else if(intent.getAction().equals(HandleServices.GOOGLE)){
                    Log.e("Motion Dect", "Google ping");
                    LocalTime localTime = new LocalTime();
                    if(stack != null) {
                        Log.e("Motion Dect", "Adding to stack");
                        stack.push(localTime);
                    }
                    checkStack();
                }
            }
        };

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(HandleServices.GOOGLE);
        intentFilter.addAction(HandleServices.OFF);
        getApplication().registerReceiver(mReceiver, intentFilter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("HandleServ", "Starting Service...");
        if(intent != null) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                Log.d("HandleServ", "Starting motion...");
                freq = intent.getExtras().getBoolean("f");
                google = intent.getExtras().getBoolean("google");

                Intent motion = new Intent(getBaseContext(), MotionDetectService.class);
                motion.putExtra("f", freq);
                //startService(motion);

                if (google) {
                    Log.d("HandleServ", "Starting Google...");
                    Context c = getApplicationContext();
                    mActivityRecognitionClient = new ActivityRecognitionClient(c);
                    intent = new Intent(c, ActivityRecognizedService.class);
                    intent.putExtra("f", freq);
                    pendingIntent = PendingIntent.getService(c, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                    mActivityRecognitionClient.requestActivityUpdates(0, pendingIntent);
                    runCheckGoogleStatusTimer();
                }
            }
        }
        Log.e("MotionDect", "New Stack");
        stack = new Stack<>();
        return super.onStartCommand(intent, flags, startId);
    }
    private void checkStack() {
        if(stack.size() > 1) {
            LocalTime t1 = stack.pop();
            LocalTime t2 = stack.peek();
            int seconds = Math.abs(Seconds.secondsBetween(t1, t2).getSeconds());
            Log.e("MotionDect", seconds + "difference and stack size: " + stack.size());
            if(seconds < 7 && stack.size() >= 5){
                if(checkIfMotionDetectServiceIsRunning()) {
                    Log.e("Handle Services", "Stopping Motion");
                    stopService(new Intent(this, MotionDetectService.class));
                }
            }
            else if (seconds > 7 && stack.size() < 5) {
                stack.clear();
            }
            else if(seconds > 7) {
                Log.e("Handle Services", "Starting Motion due to bad google detection pings");
                resetGoogleDetectedActivityApi();
                //startService(new Intent(this, MotionDetectService.class));
            }
            stack.push(t1);
        }
    }
    private boolean checkIfMotionDetectServiceIsRunning(){
        final ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        final List<ActivityManager.RunningServiceInfo> services = activityManager.getRunningServices(Integer.MAX_VALUE);
        for (int i = 0; i < services.size(); i++) {
            if ("columbia.seas.icsl.legshake.MotionDetectService".equals(services.get(i).service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void runCheckGoogleStatusTimer() {
        Log.e("Timer", "Created Timer");
        final Handler handler=new Handler();
        timer = new Timer();
        TimerTask doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @SuppressWarnings("unchecked")
                    public void run() {
                        try {
                            Log.e("Timer", "Checking google status");
                            if(runMotionFromGoogleStatus() && !checkIfMotionDetectServiceIsRunning() ) {
                                Log.e("Timer", "Starting motion from timer");
                                resetGoogleDetectedActivityApi();
                                //startService(new Intent(getApplicationContext(), MotionDetectService.class));
                                stack.clear();
                            }
                        }
                        catch (Exception e) {
                            // TODO Auto-generated catch block
                        }
                    }
                });
            }
        };
        timer.schedule(doAsynchronousTask, 0, 10000);

    }
    public boolean runMotionFromGoogleStatus() {
        if(stack.size() > 0) {
            LocalTime t0 = stack.peek();
            LocalTime t1 = new LocalTime();
            int diff = Math.abs(Seconds.secondsBetween(t1, t0).getSeconds());
            if (diff > 7)
                return true;
            else
                return false;
        }
        return false;
    }

    public void resetGoogleDetectedActivityApi() {

        Log.d(AppConstants.HANDLESERVICES_TAG.toString(), "Resetting Google Detected Api");

        // Google DetectedActivityApi off
        if(mActivityRecognitionClient != null && pendingIntent != null) {
            mActivityRecognitionClient.removeActivityUpdates(pendingIntent);
        }

        // Turn it back on.
        Context c = getApplicationContext();
        mActivityRecognitionClient = new ActivityRecognitionClient(c);
        Intent intent = new Intent(c, ActivityRecognizedService.class);
        intent.putExtra("f", freq);
        pendingIntent = PendingIntent.getService(c, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        mActivityRecognitionClient.requestActivityUpdates(0, pendingIntent);
    }
}*/