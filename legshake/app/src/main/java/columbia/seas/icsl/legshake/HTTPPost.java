package columbia.seas.icsl.legshake;

/**
 * Async HTTP Post Base class to connect to API endpoints.
 *
 * @params []string: [URL, Payload]
 * @returns Response body
 * @throw nothing, empty string on return
 * <p/>
 * Created by Xiaoqi Chen on Sep7/16.
 */

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class HTTPPost extends AsyncTask<String, Void, String> {
//  The API_Endpoint is hold on an old AWS EC2 and no longer valid, please re-deploy the end point
    String API_Endpoint = "http://icsl.ee.columbia.edu:1234/legShake/logDetection";
    public HTTPPost() {
        super();
    }

    @Override
    protected String doInBackground(String... params) {
        String status = "";
        //A URLConnection with support for HTTP-specific features. See the spec for details.
        //Uses of this class follow a pattern:
        HttpURLConnection urlConnection = null;
        try {
            String payload = params[0];
            //Obtain a new HttpURLConnection by calling URL.openConnection() and casting the result to HttpURLConnection.
            Log.d("HTTPPost", "Endpoint:" + API_Endpoint);
            Log.d("HTTPPost", "Payload:" + payload);
            URL url = new URL(API_Endpoint);

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoOutput(true);
            urlConnection.setFixedLengthStreamingMode(payload.length());

            OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
            out.write(payload.getBytes());
            out.close();
//                status = Integer.toString(urlConnection.getResponseCode());
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("HTTPPost", e.toString());
        } finally {
            //Disconnect. Once the response body has been read, the HttpURLConnection should be closed by calling disconnect(). Disconnecting releases the resources held by a connection so they may be closed or reused.
            if (urlConnection != null)
                urlConnection.disconnect();
        }
        return status;
    }

    @Override
    protected void onPostExecute(String result) {
        Log.d("HTTPPost", "Payload:" + result);
    }
}

