package columbia.seas.icsl.legshake;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import columbia.seas.icsl.legshake.constants.AppConstants;
import columbia.seas.icsl.legshake.constants.NotificationConstants;
import columbia.seas.icsl.legshake.util.EigenvalueDecomposition;
import columbia.seas.icsl.legshake.util.FFT2;
import columbia.seas.icsl.legshake.util.Matrix;
import columbia.seas.icsl.legshake.util.Utils;
import columbia.seas.icsl.legshake.util.httpModule;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.TreeMap;

import static columbia.seas.icsl.legshake.util.Utils.autoCorrelation;
import static columbia.seas.icsl.legshake.util.Utils.whiten_data;


/**
 * Created by yanlu on 10/6/16.
 * This is the core of app, it is used to judge whether the customer is shaking leg
 * Input: an array of size 256 acceleration data.
 * Process: FFT the input data, and analysis the peak frequency and its value
 * Output: Judge whether the customer is shaking leg, push notification if customer is shaking leg
 */

public class ShakeAnalysis extends AsyncTask<Object, Void, Long>{

    // Comparator object for Yan Lu's implementation; borrowed in Stephen's implementation as well.
    class Node {
        int index;
        double amp;
        public Node(int index, double amp) {
            this.index = index;
            this.amp = amp;
        }
    }

    // Comparator object for Stephen's implementation
    class EVDNode {
        double eigVal;
        Matrix eigVector;
        public EVDNode(double eigVal, Matrix eigVector) {
            this.eigVal = eigVal;
            this.eigVector = eigVector;
        }
    }
    public static final int NOTIFICATION_ID = 1;
    Context _this;
    int N = 128;
    int heapSize = 4;
    static String userName;
    static String password;
    static String last_message;
    static String longitude;
    static String latitude;
    static NotificationManager notificationManager;
    public ShakeAnalysis (Context applicationContext, String userName, String password, String longitude, String latitude) {
        _this = applicationContext;
        this.userName = userName;
        this.password = password;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    private static long latest_timestamp = 0;


    private static double eigThreshLower = 0.01;
    private static double eigThreshLowerGeneral = 0.01 - (0.00026 * (AppConstants.ACCEL_MAX_SAMPLING_FREQ.toDouble() - AppConstants.ACCEL_SAMPLING_FREQ.toDouble()));
    private static double eigThreshUpper = 17;

    // For storing when was the last time this method was called. Used for determining state of detection
    public static long lastCalled = 0;


    // For adaptive method: fund_freq, fund_threshold, harmonic_freq, harmonic_threshold - Stephen's sameleg
    Matrix mean_vector_positive = new Matrix(new double[][]{{11.9286}, {39.8552}, {26.6071}, {6.8326}});
    Matrix mean_vector_negative = new Matrix(new double[][]{{3.5161}, {15.1644}, {9.0000}, {5.7225}});
    Matrix covariance_matrix_positive = new Matrix(new double[][]{{157.6296, 534.3337, 351.9259, 84.9184}, {534.3337, 2239.9, 1204.7, 299.7703}, {351.9259, 1204.7, 821.4444, 176.3608}, {84.9184, 299.7703, 176.3608, 63.2581}});
    Matrix covariance_matrix_negative = new Matrix(new double[][]{{14.6630, 53.5650, 34.4457, 18.8815}, {53.5650, 293.4504, 136.0493, 86.0093}, {34.4457, 136.0493, 89.2500, 48.8974}, {18.8815, 86.0093, 48.8974, 39.0394}});
    Matrix covariance_matrix_positive_inverse = covariance_matrix_positive.inverse();
    Matrix covariance_matrix_negative_inverse = covariance_matrix_negative.inverse();
    double log_cov_det_positive = -0.5 * Math.log(covariance_matrix_positive.det());
    double log_cov_det_negative = -0.5 * Math.log(covariance_matrix_negative.det());
    double lambda = 50;
    double threshold_frac = 0.3;


    protected Long doInBackground(Object... vals) {

        lastCalled = System.currentTimeMillis();
        Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Called at: " + Long.valueOf(lastCalled));

        // 1-dim acceleration data for Yan's implementation
        double[] acceleration = (double[]) vals[0];

        // Whiten data
        //acceleration = com.example.yanlu.legshake.Utils.whiten_data(acceleration);
        acceleration = whiten_data(acceleration);

        // Data for Stephen's Implementation
        double[][]raw_data = (double[][]) vals[1];

        // For determining what frequency we are running the detection algorithm at
        int frequency = (int)vals[2];

        // All other sensor data
        TreeMap<String, ArrayList<float[]>> allSensorDataContainer = (TreeMap<String, ArrayList<float[]>>) vals[3];

        // Run 50 hz
        if(frequency == 50) {
            //return judge(0, acceleration);
            //return judge_naive(0, acceleration);
            //return detectLegShake(raw_data, acceleration, allSensorDataContainer);
            //return otherImplementation(raw_data);
            return detectLegShakeAdatptive(raw_data, acceleration, allSensorDataContainer);
        }

        // Run 15 hz
        else {
            return detectLegShakeGeneral(raw_data, acceleration, allSensorDataContainer);
        }
    }

    /**
     * Stephen's implementation of leg shaking detection using adaptive method
     * @param raw_3axes_data: N x 3 3 axis accelerometer data
     * @return 0 if failed, or the UTC time of the event
     */
    private Long detectLegShakeAdatptive(double[][] raw_3axes_data, double[] gforce, TreeMap<String, ArrayList<float[]>> otherSensorData) {
        double[] dataOneDim = new double[N];
        //double energyFraction = 0.05;
        double firstPeakFraction = 0.1;
        double secondPeakFraction = 0.7;
        //double secondPeakFraction = 1;
        //double firstPeakFraction = 1;
        double firstPeakMin = 2.5;
        double secondPeakMin = 1;
        double squaredMin = 5;
        double minFirstEigVal = 5;

        // Thresholding for determining the contour of the spectrum
        double peakBaseMaxFrac = 0.3;
        double maxSpectrumFrac = 0.25;

        //double peakBaseMaxFrac = 1;
        //double maxSpectrumFrac = 1;

        int lowerbound;
        int upperbound;

        // General purpose flag
        boolean flag = false;

        // flag to determine if the fundamental is strongest or the 1st harmonic. The 2nd
        // harmonic should never be the strongest after mapping.
        boolean fundamentalStrongest = false;

        int fundamentalFreqIndex = 0;
        double fundamentalFreqMag = 0;
        int firstHarmonicFreqIndex = 0;
        double firstHarmonicFreqMag = 0;
        int secondHarmonicFreqIndex = 0;
        double secondHarmonicFreqMag = 0;
        int thirdHarmonicFreqIndex = 0;
        double thirdHarmonicFreqMag = 0;

        double secondPeakAmplitude = 0.0;
        int secondPeakFreqIndex = 0;

        double peakAmplitude = 0.0;
        int peakFreqIndex = 0;

        //Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Start Computing");

        // Whiten the axis data
        Matrix raw_3axes_mat = new Matrix(raw_3axes_data);
        double[][] row_3axes_data = new double[3][N];
        row_3axes_data[0] = whiten_data(raw_3axes_mat.getMatrix(0, N - 1, 0, 0).getColumnPackedCopy());
        row_3axes_data[1] = whiten_data(raw_3axes_mat.getMatrix(0, N - 1, 1, 1).getColumnPackedCopy());
        row_3axes_data[2] = whiten_data(raw_3axes_mat.getMatrix(0, N - 1, 2, 2).getColumnPackedCopy());
        raw_3axes_mat = new Matrix(row_3axes_data);
        raw_3axes_mat = raw_3axes_mat.transpose();

        // With the raw axis data, X, find the EVD of X.T * X, which will yield 3 eigenvalues and 3 eigenvectors
        EigenvalueDecomposition eigValCalculator = new EigenvalueDecomposition(raw_3axes_mat.transpose().times(raw_3axes_mat));
        Matrix eigVectors = eigValCalculator.getV();
        Matrix eigValues = eigValCalculator.getD();

        // Sort to find the eigenvalue and eigenvector pair with the highest variance
        Queue<EVDNode> eigQueue = new PriorityQueue<>(3, new Comparator<EVDNode>() {
            public int compare(EVDNode a, EVDNode b) {

                //Since queues returns the 'lowest' value first, the higher eigenvalues will get ranked lower than the smaller ones
                return a.eigVal - b.eigVal >0 ? -1: 1;
            }
        });

        // Offer the three pairs of eigenvalues and eigenvectors to queue for sorting
        eigQueue.offer(new EVDNode(eigValues.get(0, 0), eigVectors.getMatrix(0, 2, 0, 0)));
        eigQueue.offer(new EVDNode(eigValues.get(1, 1), eigVectors.getMatrix(0, 2, 1, 1)));
        eigQueue.offer(new EVDNode(eigValues.get(2, 2), eigVectors.getMatrix(0, 2, 2, 2)));
        EVDNode firstDirection = eigQueue.poll();
        EVDNode secondDirection = eigQueue.poll();
        EVDNode thirdDirection = eigQueue.poll();
        //Log.d("detectLegShake", String.valueOf(firstDirection.eigVal));
        //Log.d("detectLegShake", String.valueOf(firstDirection.eigVector.getColumnPackedCopy().length));
        //Log.d("detectLegShake", String.valueOf(secondDirection.eigVal));
        //Log.d("detectLegShake", String.valueOf(secondDirection.eigVector.getColumnPackedCopy().length));
        //Log.d("detectLegShake", String.valueOf(thirdDirection.eigVal));
        //Log.d("detectLegShake", String.valueOf(thirdDirection.eigVector.getColumnPackedCopy().length));

        //Log.d("First Direction", String.valueOf(firstDirection.eigVal));
        //Log.d("Second Direction", String.valueOf(secondDirection.eigVal));
        //Log.d("Third Direction", String.valueOf(thirdDirection.eigVal));
        // Map to the dimension of highest variance
        for(int i = 0; i < N; i++) {
            dataOneDim[i] = firstDirection.eigVector.transpose().times(raw_3axes_mat.getMatrix(i, i, 0, 2).transpose()).get(0, 0);
        }

        // Whiten the data
        dataOneDim = whiten_data(dataOneDim);

        // Compute single-sided FFT using FFT2
        double[] dataOneDim_re = new double[N];
        double[] dataOneDim_im = new double[N];
        System.arraycopy(dataOneDim, 0, dataOneDim_re, 0, N);

        FFT2 fft_computer = new FFT2(N);
        fft_computer.fft(dataOneDim_re, dataOneDim_im);
        double[] dataFreqMagnitude = new double[N / 2];
        dataFreqMagnitude[0] = Math.sqrt((dataOneDim_re[0] * dataOneDim_re[0]) + (dataOneDim_im[0] * dataOneDim_im[0]));
        for(int i = 0; i < N / 2; i++) {
            dataFreqMagnitude[i] = 2 * Math.sqrt((dataOneDim_re[i] * dataOneDim_re[i]) + (dataOneDim_im[i] * dataOneDim_im[i]));
        }

        // Compute double-sided FFT
        /*Complex[] sensor_data = new Complex[N];
        for (int i = 0; i < N; i++) {
            sensor_data[i] = new Complex(dataOneDim[i], 0);
        }
        Complex[] fft_result = FFT.fft(sensor_data);

        // Convert to single-sided Magnitude spectrum
        double[] dataFreqMagnitude = new double[N / 2];
        dataFreqMagnitude[0] = fft_result[0].abs();
        for(int i = 1; i < N / 2; i++) {
            dataFreqMagnitude[i] = 2 * fft_result[i].abs();
        }*/

        // Compute magnitude spectrum of gforce for logging purposes
        double[] gforceMagSpectrum = new double[N / 2];
        double[] gforce_im = new double[N];
        fft_computer.fft(gforce, gforce_im);
        gforceMagSpectrum[0] = Math.sqrt((gforce[0] * gforce[0]) + (gforce_im[0] * gforce_im[0]));
        for(int i = 1; i < N / 2; i++) {
            gforceMagSpectrum[i] = 2 * Math.sqrt((gforce[i] * gforce[i]) + (gforce_im[i] * gforce_im[i]));
        }

        // Send axis data and accelerometer data back to main for loggin false negatives
        long timestamp_primitive = System.currentTimeMillis() / 1000;
        Long timestamp = new Long(timestamp_primitive);
        double[] x_axis = raw_3axes_mat.getMatrix(0, N - 1, 0, 0).getColumnPackedCopy();
        double[] y_axis = raw_3axes_mat.getMatrix(0, N - 1, 1, 1).getColumnPackedCopy();
        double[] z_axis = raw_3axes_mat.getMatrix(0, N - 1, 2, 2).getColumnPackedCopy();
        double[] eigVals = new double[3];
        double[] eigVec1 = firstDirection.eigVector.getColumnPackedCopy();
        double[] eigVec2 = secondDirection.eigVector.getColumnPackedCopy();
        double[] eigVec3 = thirdDirection.eigVector.getColumnPackedCopy();
        eigVals[0] = firstDirection.eigVal;
        eigVals[1] = secondDirection.eigVal;
        eigVals[2] = thirdDirection.eigVal;

        // Find peak frequency.
        for(int i = 0; i < N/2; i++) {
            if(dataFreqMagnitude[i] > peakAmplitude) {
                peakAmplitude = dataFreqMagnitude[i];
                peakFreqIndex = i;
            }
        }

        //Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Peak Amplitude: " + String.valueOf(peakAmplitude));
        //Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Peak Freq: " + String.valueOf(peakFreqIndex));

        // Find second peak frequency
        // Find second peak frequencdy outside of the area around the first peak.
        //double secondPeakAmplitude = 0.0;
        //int secondPeakFreqIndex = 0;
        for(int i = 0; i < peakFreqIndex - 3; i++) {
            if(dataFreqMagnitude[i] > secondPeakAmplitude) {
                secondPeakAmplitude = dataFreqMagnitude[i];
                secondPeakFreqIndex = i;
            }
        }
        for(int i = peakFreqIndex + 3 + 1; i < N / 2; i++) {
            if(dataFreqMagnitude[i] > secondPeakAmplitude) {
                secondPeakAmplitude = dataFreqMagnitude[i];
                secondPeakFreqIndex = i;
            }
        }
        //Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Second Amplitude: " + String.valueOf(secondPeakAmplitude));
        //Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Second Freq: " + String.valueOf(secondPeakFreqIndex));

        // If eigenvalues are too small, then the phone is not moving. Return
        if(firstDirection.eigVal < eigThreshLower) {
            //Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "EigVal too small");

            // Log in case for false negatives
            sendWindowToMain(
                    userName,
                    password,
                    timestamp,
                    x_axis,
                    y_axis,
                    z_axis,
                    eigVals,
                    eigVec1,
                    eigVec2,
                    eigVec3,
                    gforceMagSpectrum,
                    dataFreqMagnitude,
                    longitude,
                    latitude,
                    otherSensorData,
                    0);

            return new Long(0);
        }

        // If peak is harmonic
        if(peakFreqIndex > secondPeakFreqIndex) {
            fundamentalFreqIndex = secondPeakFreqIndex;
            fundamentalFreqMag = secondPeakAmplitude;
            firstHarmonicFreqIndex = peakFreqIndex;
            firstHarmonicFreqMag = peakAmplitude;
        }
        // Otherwise fundamental is peak
        else {
            fundamentalFreqIndex = peakFreqIndex;
            fundamentalFreqMag = peakAmplitude;
            firstHarmonicFreqIndex = secondPeakFreqIndex;
            firstHarmonicFreqMag = secondPeakAmplitude;
        }

        // Compute power for thresholding
        double avgAmplitude = 0.0;
        dataFreqMagnitude[fundamentalFreqIndex] = 0.0;
        dataFreqMagnitude[firstHarmonicFreqIndex] = 0.0;
        for(int i = 0; i < N / 2; i++) {
            avgAmplitude += dataFreqMagnitude[i];
        }
        avgAmplitude /= ((N / 2) - 2);

        double harmonicThreshold = firstHarmonicFreqMag / avgAmplitude;
        double fundamentalThreshold = fundamentalFreqMag / avgAmplitude;

        Matrix observation_vector = new Matrix(new double[][]{{fundamentalFreqIndex}, {fundamentalThreshold}, {firstHarmonicFreqIndex}, {harmonicThreshold}});

        // The peak should be well above all other non-peaks
        for(int i = 0; i < fundamentalFreqIndex - 7; i++) {
            double amp_divided_by_peak = dataFreqMagnitude[i] / peakAmplitude;
            if(amp_divided_by_peak > maxSpectrumFrac) {
                // Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Amp " + String.valueOf(i) + " / peak " + String.valueOf(peakFreqIndex) + " = " + String.valueOf(amp_divided_by_peak));

                // Log in case for false negatives
                sendWindowToMain(
                        userName,
                        password,
                        timestamp,
                        x_axis,
                        y_axis,
                        z_axis,
                        eigVals,
                        eigVec1,
                        eigVec2,
                        eigVec3,
                        gforceMagSpectrum,
                        dataFreqMagnitude,
                        longitude,
                        latitude,
                        otherSensorData,
                        0);

                return new Long(0);
            }
        }
        for(int i = fundamentalFreqIndex + 7 + 1; i < firstHarmonicFreqIndex - 2; i++) {
            double amp_divided_by_peak = dataFreqMagnitude[i] / peakAmplitude;
            if(amp_divided_by_peak > maxSpectrumFrac) {
                // Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Amp " + String.valueOf(i) + " / peak " + String.valueOf(peakFreqIndex) + " = " + String.valueOf(amp_divided_by_peak));

                // Log in case for false negatives
                sendWindowToMain(
                        userName,
                        password,
                        timestamp,
                        x_axis,
                        y_axis,
                        z_axis,
                        eigVals,
                        eigVec1,
                        eigVec2,
                        eigVec3,
                        gforceMagSpectrum,
                        dataFreqMagnitude,
                        longitude,
                        latitude,
                        otherSensorData,
                        0);

                return new Long(0);
            }
        }
        for(int i = firstHarmonicFreqIndex + 2 + 1; i < N / 2; i++) {
            double amp_divided_by_peak = dataFreqMagnitude[i] / peakAmplitude;
            if(amp_divided_by_peak > maxSpectrumFrac) {
                // Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Amp " + String.valueOf(i) + " / peak " + String.valueOf(peakFreqIndex) + " = " + String.valueOf(amp_divided_by_peak));

                // Log in case for false negatives
                sendWindowToMain(
                        userName,
                        password,
                        timestamp,
                        x_axis,
                        y_axis,
                        z_axis,
                        eigVals,
                        eigVec1,
                        eigVec2,
                        eigVec3,
                        gforceMagSpectrum,
                        dataFreqMagnitude,
                        longitude,
                        latitude,
                        otherSensorData,
                        0);

                return new Long(0);
            }
        }

        // Determine if it is leg shaking or not
        if(HypothesisTest(observation_vector)) {

            // Send the data to server since this is leg shaking
            dataFreqMagnitude[fundamentalFreqIndex] = fundamentalFreqMag;
            dataFreqMagnitude[firstHarmonicFreqIndex] = firstHarmonicFreqMag;
            dataFreqMagnitude[secondHarmonicFreqIndex] = secondHarmonicFreqMag;
            if (AppConstants.SERVER_FLAG.toInt() > 0) {
                JSONObject json_parameters = httpModule.packageParametersDetectionEvent(
                        userName,
                        password,
                        timestamp_primitive,
                        x_axis, y_axis,
                        z_axis,
                        eigVals,
                        eigVec1,
                        eigVec2,
                        eigVec3,
                        gforceMagSpectrum,
                        dataFreqMagnitude,
                        longitude,
                        latitude,
                        otherSensorData,
                        0);
                httpModule.run_http_post_to_server(AppConstants.LOGDETECTION_URL.toString(), json_parameters);
            }

            return timestamp;
        }
        //Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Done Init Loggin");
        //Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Finish Computing");
        return new Long(0);
    }

    /**
     * Stephen's implementation of leg shaking detection
     * @param raw_3axes_data: N x 3 3 axis accelerometer data
     * @return 0 if failed, or the UTC time of the event
     */
    private Long detectLegShake(double[][] raw_3axes_data, double[] gforce, TreeMap<String, ArrayList<float[]>> otherSensorData) {
        double[] dataOneDim = new double[N];
        //double energyFraction = 0.05;
        double firstPeakFraction = 0.1;
        double secondPeakFraction = 0.7;
        //double secondPeakFraction = 1;
        //double firstPeakFraction = 1;
        double firstPeakMin = 2.5;
        double secondPeakMin = 1;
        double squaredMin = 5;
        double minFirstEigVal = 5;

        // Thresholding for determining the contour of the spectrum
        double peakBaseMaxFrac = 0.3;
        double maxSpectrumFrac = 0.25;

        //double peakBaseMaxFrac = 1;
        //double maxSpectrumFrac = 1;

        int lowerbound;
        int upperbound;

        // General purpose flag
        boolean flag = false;

        // flag to determine if the fundamental is strongest or the 1st harmonic. The 2nd
        // harmonic should never be the strongest after mapping.
        boolean fundamentalStrongest = false;

        int fundamentalFreqIndex = 0;
        double fundamentalFreqMag = 0;
        int firstHarmonicFreqIndex = 0;
        double firstHarmonicFreqMag = 0;
        int secondHarmonicFreqIndex = 0;
        double secondHarmonicFreqMag = 0;
        int thirdHarmonicFreqIndex = 0;
        double thirdHarmonicFreqMag = 0;

        double secondPeakAmplitude = 0.0;
        int secondPeakFreqIndex = 0;

        double peakAmplitude = 0.0;
        int peakFreqIndex = 0;

        //Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Start Computing");

        // Whiten the axis data
        Matrix raw_3axes_mat = new Matrix(raw_3axes_data);
        double[][] row_3axes_data = new double[3][N];
        row_3axes_data[0] = whiten_data(raw_3axes_mat.getMatrix(0, N - 1, 0, 0).getColumnPackedCopy());
        row_3axes_data[1] = whiten_data(raw_3axes_mat.getMatrix(0, N - 1, 1, 1).getColumnPackedCopy());
        row_3axes_data[2] = whiten_data(raw_3axes_mat.getMatrix(0, N - 1, 2, 2).getColumnPackedCopy());
        raw_3axes_mat = new Matrix(row_3axes_data);
        raw_3axes_mat = raw_3axes_mat.transpose();

        // With the raw axis data, X, find the EVD of X.T * X, which will yield 3 eigenvalues and 3 eigenvectors
        EigenvalueDecomposition eigValCalculator = new EigenvalueDecomposition(raw_3axes_mat.transpose().times(raw_3axes_mat));
        Matrix eigVectors = eigValCalculator.getV();
        Matrix eigValues = eigValCalculator.getD();

        // Sort to find the eigenvalue and eigenvector pair with the highest variance
        Queue<EVDNode> eigQueue = new PriorityQueue<>(3, new Comparator<EVDNode>() {
            public int compare(EVDNode a, EVDNode b) {

                //Since queues returns the 'lowest' value first, the higher eigenvalues will get ranked lower than the smaller ones
                return a.eigVal - b.eigVal >0 ? -1: 1;
            }
        });

        // Offer the three pairs of eigenvalues and eigenvectors to queue for sorting
        eigQueue.offer(new EVDNode(eigValues.get(0, 0), eigVectors.getMatrix(0, 2, 0, 0)));
        eigQueue.offer(new EVDNode(eigValues.get(1, 1), eigVectors.getMatrix(0, 2, 1, 1)));
        eigQueue.offer(new EVDNode(eigValues.get(2, 2), eigVectors.getMatrix(0, 2, 2, 2)));
        EVDNode firstDirection = eigQueue.poll();
        EVDNode secondDirection = eigQueue.poll();
        EVDNode thirdDirection = eigQueue.poll();
        //Log.d("detectLegShake", String.valueOf(firstDirection.eigVal));
        //Log.d("detectLegShake", String.valueOf(firstDirection.eigVector.getColumnPackedCopy().length));
        //Log.d("detectLegShake", String.valueOf(secondDirection.eigVal));
        //Log.d("detectLegShake", String.valueOf(secondDirection.eigVector.getColumnPackedCopy().length));
        //Log.d("detectLegShake", String.valueOf(thirdDirection.eigVal));
        //Log.d("detectLegShake", String.valueOf(thirdDirection.eigVector.getColumnPackedCopy().length));

        //Log.d("First Direction", String.valueOf(firstDirection.eigVal));
        //Log.d("Second Direction", String.valueOf(secondDirection.eigVal));
        //Log.d("Third Direction", String.valueOf(thirdDirection.eigVal));
        // Map to the dimension of highest variance
        for(int i = 0; i < N; i++) {
            dataOneDim[i] = firstDirection.eigVector.transpose().times(raw_3axes_mat.getMatrix(i, i, 0, 2).transpose()).get(0, 0);
        }

        // Whiten the data
        dataOneDim = whiten_data(dataOneDim);

        // Compute single-sided FFT using FFT2
        double[] dataOneDim_re = new double[N];
        double[] dataOneDim_im = new double[N];
        System.arraycopy(dataOneDim, 0, dataOneDim_re, 0, N);

        FFT2 fft_computer = new FFT2(N);
        fft_computer.fft(dataOneDim_re, dataOneDim_im);
        double[] dataFreqMagnitude = new double[N / 2];
        dataFreqMagnitude[0] = Math.sqrt((dataOneDim_re[0] * dataOneDim_re[0]) + (dataOneDim_im[0] * dataOneDim_im[0]));
        for(int i = 0; i < N / 2; i++) {
            dataFreqMagnitude[i] = 2 * Math.sqrt((dataOneDim_re[i] * dataOneDim_re[i]) + (dataOneDim_im[i] * dataOneDim_im[i]));
        }

        // Compute double-sided FFT
        /*Complex[] sensor_data = new Complex[N];
        for (int i = 0; i < N; i++) {
            sensor_data[i] = new Complex(dataOneDim[i], 0);
        }
        Complex[] fft_result = FFT.fft(sensor_data);

        // Convert to single-sided Magnitude spectrum
        double[] dataFreqMagnitude = new double[N / 2];
        dataFreqMagnitude[0] = fft_result[0].abs();
        for(int i = 1; i < N / 2; i++) {
            dataFreqMagnitude[i] = 2 * fft_result[i].abs();
        }*/

        // Compute magnitude spectrum of gforce for logging purposes
        double[] gforceMagSpectrum = new double[N / 2];
        double[] gforce_im = new double[N];
        fft_computer.fft(gforce, gforce_im);
        gforceMagSpectrum[0] = Math.sqrt((gforce[0] * gforce[0]) + (gforce_im[0] * gforce_im[0]));
        for(int i = 1; i < N / 2; i++) {
            gforceMagSpectrum[i] = 2 * Math.sqrt((gforce[i] * gforce[i]) + (gforce_im[i] * gforce_im[i]));
        }

        // Send axis data and accelerometer data back to main for loggin false negatives
        long timestamp_primitive = System.currentTimeMillis() / 1000;
        Long timestamp = new Long(timestamp_primitive);
        double[] x_axis = raw_3axes_mat.getMatrix(0, N - 1, 0, 0).getColumnPackedCopy();
        double[] y_axis = raw_3axes_mat.getMatrix(0, N - 1, 1, 1).getColumnPackedCopy();
        double[] z_axis = raw_3axes_mat.getMatrix(0, N - 1, 2, 2).getColumnPackedCopy();
        double[] eigVals = new double[3];
        double[] eigVec1 = firstDirection.eigVector.getColumnPackedCopy();
        double[] eigVec2 = secondDirection.eigVector.getColumnPackedCopy();
        double[] eigVec3 = thirdDirection.eigVector.getColumnPackedCopy();
        eigVals[0] = firstDirection.eigVal;
        eigVals[1] = secondDirection.eigVal;
        eigVals[2] = thirdDirection.eigVal;

        // Find peak frequency.
        for(int i = 0; i < N/2; i++) {
            if(dataFreqMagnitude[i] > peakAmplitude) {
                peakAmplitude = dataFreqMagnitude[i];
                peakFreqIndex = i;
            }
        }

        //Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Peak Amplitude: " + String.valueOf(peakAmplitude));
        //Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Peak Freq: " + String.valueOf(peakFreqIndex));

        // Find second peak frequency
        // Find second peak frequencdy outside of the area around the first peak.
        //double secondPeakAmplitude = 0.0;
        //int secondPeakFreqIndex = 0;
        for(int i = 0; i < peakFreqIndex - 7; i++) {
            if(dataFreqMagnitude[i] > secondPeakAmplitude) {
                secondPeakAmplitude = dataFreqMagnitude[i];
                secondPeakFreqIndex = i;
            }
        }
        for(int i = peakFreqIndex + 7 + 1; i < N / 2; i++) {
            if(dataFreqMagnitude[i] > secondPeakAmplitude) {
                secondPeakAmplitude = dataFreqMagnitude[i];
                secondPeakFreqIndex = i;
            }
        }
        //Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Second Amplitude: " + String.valueOf(secondPeakAmplitude));
        //Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Second Freq: " + String.valueOf(secondPeakFreqIndex));

        // If eigenvalues are too small, then the phone is not moving. Return
        if(firstDirection.eigVal < eigThreshLower) {
            //Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "EigVal too small");

            // Log in case for false negatives
            sendWindowToMain(
                    userName,
                    password,
                    timestamp,
                    x_axis,
                    y_axis,
                    z_axis,
                    eigVals,
                    eigVec1,
                    eigVec2,
                    eigVec3,
                    gforceMagSpectrum,
                    dataFreqMagnitude,
                    longitude,
                    latitude,
                    otherSensorData,
                    0);

            return new Long(0);
        }

        // Order the first and second peaks
        // Case where the first peak is the fundamental and the second peak is either the 1st or second harmonic.
        flag = false;
        if(peakFreqIndex >= 10 && peakFreqIndex <= 17) {
            fundamentalStrongest = true;
            fundamentalFreqIndex = peakFreqIndex;
            fundamentalFreqMag = peakAmplitude;

            // Second Peak is the first harmonic
            if(secondPeakFreqIndex <= (2 * peakFreqIndex) + 3 &&
                    secondPeakFreqIndex >= (2 * peakFreqIndex) - 3) {
                flag = true;
                firstHarmonicFreqIndex = secondPeakFreqIndex;
                firstHarmonicFreqMag = secondPeakAmplitude;

                // Find second harmonic
                secondHarmonicFreqIndex = 0;
                secondHarmonicFreqMag = 0;
                for(int i = (3 * fundamentalFreqIndex) - 3; i < (3 * fundamentalFreqIndex) + 3 + 1; i++) {
                    if(dataFreqMagnitude[i] > secondHarmonicFreqMag) {
                        secondHarmonicFreqIndex = i;
                        secondHarmonicFreqMag = dataFreqMagnitude[i];
                    }
                }
            }

            // Peak is the second harmonic
            else if(
                    secondPeakFreqIndex <= (3 * peakFreqIndex) + 3 &&
                    secondPeakFreqIndex >= (3 * peakFreqIndex) - 3) {
                flag = true;
                secondHarmonicFreqIndex = secondPeakFreqIndex;
                secondHarmonicFreqMag = secondPeakAmplitude;

                // Find first harmonic
                firstHarmonicFreqIndex = 0;
                firstHarmonicFreqMag = 0;
                for(int i = (3 * fundamentalFreqIndex) - 3; i < (3 * fundamentalFreqIndex) + 3 + 1; i++) {
                    if(dataFreqMagnitude[i] > firstHarmonicFreqMag) {
                        firstHarmonicFreqIndex = i;
                        firstHarmonicFreqMag = dataFreqMagnitude[i];
                    }
                }

            }
        }

        // first harmonic is the highest frequency
        else if(peakFreqIndex >= 20 && peakFreqIndex <= 35){
            firstHarmonicFreqIndex = peakFreqIndex;
            firstHarmonicFreqMag = peakAmplitude;

            // The second peak should be the fundamental, though the second harmonic and the fundamental could be close.
            if(secondPeakFreqIndex <= (peakFreqIndex / 2) + 3 &&
                    secondPeakFreqIndex >= (peakFreqIndex / 2) - 3) {
                flag = true;
                fundamentalFreqIndex = secondPeakFreqIndex;
                fundamentalFreqMag = secondPeakAmplitude;

                // Find second harmonic
                secondHarmonicFreqIndex = 0;
                secondHarmonicFreqMag = 0;
                for(int i = (3 * fundamentalFreqIndex) - 3; i < (3 * fundamentalFreqIndex) + 3 + 1; i++) {
                    if(dataFreqMagnitude[i] > secondHarmonicFreqMag) {
                        secondHarmonicFreqIndex = i;
                        secondHarmonicFreqMag = dataFreqMagnitude[i];
                    }
                }
            }

            // The second peak is the second harmonic
            else if(secondPeakFreqIndex <= (3 * peakFreqIndex / 2) + 3 &&
                    secondPeakFreqIndex >= (3 * peakFreqIndex / 2) - 3) {
                flag = true;
                secondHarmonicFreqIndex = secondPeakFreqIndex;
                secondHarmonicFreqMag = secondPeakAmplitude;

                // Find the fundamental
                fundamentalFreqIndex = 0;
                fundamentalFreqMag = 0;
                for(int i = (firstHarmonicFreqIndex / 2) - 3; i < (firstHarmonicFreqIndex / 2) + 3; i++) {
                    if(dataFreqMagnitude[i] > fundamentalFreqMag) {
                        fundamentalFreqIndex = i;
                        fundamentalFreqMag = dataFreqMagnitude[i];
                    }
                }
            }
        }

        // If none of the cases are satisfied, then this is not leg shaking
        if(!flag) {
            //Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "None of cases met");

            // Log in case for false negatives
            sendWindowToMain(
                    userName,
                    password,
                    timestamp,
                    x_axis,
                    y_axis,
                    z_axis,
                    eigVals,
                    eigVec1,
                    eigVec2,
                    eigVec3,
                    gforceMagSpectrum,
                    dataFreqMagnitude,
                    longitude,
                    latitude,
                    otherSensorData,
                    0);

            return new Long(0);
        }

        // Check peak thresholding requirements
        if(!(fundamentalAndFirstHarmonicCheck(fundamentalFreqMag, firstHarmonicFreqMag, firstDirection.eigVal) &&
             fundamentalAndSecondHarmonicCheck(fundamentalFreqMag, secondHarmonicFreqMag, firstDirection.eigVal))) {
           // Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Peak Thresholding not met.");

            // Log in case for false negatives
            sendWindowToMain(
                    userName,
                    password,
                    timestamp,
                    x_axis,
                    y_axis,
                    z_axis,
                    eigVals,
                    eigVec1,
                    eigVec2,
                    eigVec3,
                    gforceMagSpectrum,
                    dataFreqMagnitude,
                    longitude,
                    latitude,
                    otherSensorData,
                    0);

            return new Long(0);
        }

        // The area 1 to 1.5 hz away from peaks should not have any significant values
        lowerbound = fundamentalFreqIndex - 7;
        upperbound = fundamentalFreqIndex - 5;
        for(int i = lowerbound; i <= upperbound; i++) {
            double amp_divided_by_peak = dataFreqMagnitude[i] / peakAmplitude;
            if(amp_divided_by_peak > peakBaseMaxFrac) {
                //Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "peak " + String.valueOf(peakFreqIndex) + " / Amp " + String.valueOf(i) + " = " + String.valueOf(amp_divided_by_peak));

                // Log in case for false negatives
                sendWindowToMain(
                        userName,
                        password,
                        timestamp,
                        x_axis,
                        y_axis,
                        z_axis,
                        eigVals,
                        eigVec1,
                        eigVec2,
                        eigVec3,
                        gforceMagSpectrum,
                        dataFreqMagnitude,
                        longitude,
                        latitude,
                        otherSensorData,
                        0);

                return new Long(0);
            }
        }
        lowerbound = fundamentalFreqIndex + 5;
        upperbound = fundamentalFreqIndex + 7;
        for(int i = lowerbound; i <= upperbound; i++) {
            double amp_divided_by_peak = dataFreqMagnitude[i] / peakAmplitude;
            if(amp_divided_by_peak > peakBaseMaxFrac) {
               // Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Amp " + String.valueOf(i) + " / peak " + String.valueOf(peakFreqIndex) + " = " + String.valueOf(amp_divided_by_peak));

                // Log in case for false negatives
                sendWindowToMain(
                        userName,
                        password,
                        timestamp,
                        x_axis,
                        y_axis,
                        z_axis,
                        eigVals,
                        eigVec1,
                        eigVec2,
                        eigVec3,
                        gforceMagSpectrum,
                        dataFreqMagnitude,
                        longitude,
                        latitude,
                        otherSensorData,
                        0);

                return new Long(0);
            }
        }

        lowerbound = firstHarmonicFreqIndex - 7;
        upperbound = firstHarmonicFreqIndex - 5;
        for(int i = lowerbound; i <= upperbound; i++) {
            double amp_divided_by_peak = dataFreqMagnitude[i] / peakAmplitude;
            if(amp_divided_by_peak > peakBaseMaxFrac) {
              //  Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "peak " + String.valueOf(peakFreqIndex) + " / Amp " + String.valueOf(i) + " = " + String.valueOf(amp_divided_by_peak));

                // Log in case for false negatives
                sendWindowToMain(
                        userName,
                        password,
                        timestamp,
                        x_axis,
                        y_axis,
                        z_axis,
                        eigVals,
                        eigVec1,
                        eigVec2,
                        eigVec3,
                        gforceMagSpectrum,
                        dataFreqMagnitude,
                        longitude,
                        latitude,
                        otherSensorData,
                        0);

                return new Long(0);
            }
        }
        lowerbound = firstHarmonicFreqIndex + 5;
        upperbound = firstHarmonicFreqIndex + 7;
        for(int i = lowerbound; i <= upperbound; i++) {
            double amp_divided_by_peak = dataFreqMagnitude[i] / peakAmplitude;
            if(amp_divided_by_peak > peakBaseMaxFrac) {
               // Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Amp " + String.valueOf(i) + " / peak " + String.valueOf(peakFreqIndex) + " = " + String.valueOf(amp_divided_by_peak));

                // Log in case for false negatives
                sendWindowToMain(
                        userName,
                        password,
                        timestamp,
                        x_axis,
                        y_axis,
                        z_axis,
                        eigVals,
                        eigVec1,
                        eigVec2,
                        eigVec3,
                        gforceMagSpectrum,
                        dataFreqMagnitude,
                        longitude,
                        latitude,
                        otherSensorData,
                        0);

                return new Long(0);
            }
        }

        lowerbound = secondHarmonicFreqIndex - 7;
        upperbound = secondHarmonicFreqIndex - 5;
        for(int i = lowerbound; i <= upperbound; i++) {
            double amp_divided_by_peak = dataFreqMagnitude[i] / peakAmplitude;
            if(amp_divided_by_peak > peakBaseMaxFrac) {
               // Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "peak " + String.valueOf(peakFreqIndex) + " / Amp " + String.valueOf(i) + " = " + String.valueOf(amp_divided_by_peak));

                // Log in case for false negatives
                sendWindowToMain(
                        userName,
                        password,
                        timestamp,
                        x_axis,
                        y_axis,
                        z_axis,
                        eigVals,
                        eigVec1,
                        eigVec2,
                        eigVec3,
                        gforceMagSpectrum,
                        dataFreqMagnitude,
                        longitude,
                        latitude,
                        otherSensorData,
                        0);

                return new Long(0);
            }
        }
        lowerbound = secondHarmonicFreqIndex + 5;
        upperbound = secondHarmonicFreqIndex + 7;
        for(int i = lowerbound; i <= upperbound; i++) {
            double amp_divided_by_peak = dataFreqMagnitude[i] / peakAmplitude;
            if(amp_divided_by_peak > peakBaseMaxFrac) {
             //   Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Amp " + String.valueOf(i) + " / peak " + String.valueOf(peakFreqIndex) + " = " + String.valueOf(amp_divided_by_peak));

                // Log in case for false negatives
                sendWindowToMain(
                        userName,
                        password,
                        timestamp,
                        x_axis,
                        y_axis,
                        z_axis,
                        eigVals,
                        eigVec1,
                        eigVec2,
                        eigVec3,
                        gforceMagSpectrum,
                        dataFreqMagnitude,
                        longitude,
                        latitude,
                        otherSensorData,
                        0);

                return new Long(0);
            }
        }



        // Determine if the peak is between 3.9 to 6.8 Hz or, which is the fundamental frequency of most people when they shake their legs.
        // Mapping to the direction of highest variance should have isolated the fundamental frequency.
        //if(peakFreqIndex < 20 || peakFreqIndex > 35) {
        //    return false;
        //}

        // Since humans shake imperfectly, there will always be a harmonic. This harmonic peak should be the second highest peak.
        // Search for the second highest peak, outside of the fundamental frequency interval.
        //double secondPeakAmplitude = 0.0;
        //int secondPeakFreqIndex = 0;
        //for(int i = 0; i < 20; i++) {
        //    if(dataFreqMagnitude[i] > secondPeakAmplitude) {
        //        secondPeakAmplitude = dataFreqMagnitude[i];
        //        secondPeakFreqIndex = i;
        //    }
        //}
        //for(int i = 36; i < N / 2; i++) {
        //   if(dataFreqMagnitude[i] > secondPeakAmplitude) {
        //       secondPeakAmplitude = dataFreqMagnitude[i];
        //       secondPeakFreqIndex = i;
        //    }
        // }

        //Log.d("Detect", "Second Amplitude: " + String.valueOf(secondPeakAmplitude));
        // If the second peak is within ~ +-1 hz where the harmonic should be, then move on
        //if(secondPeakFreqIndex < (2 * peakFreqIndex) - 5 || secondPeakFreqIndex > (2 * peakFreqIndex) + 5) {
        //    return false;
        //}

        // Determine if the peaks are significant enough for someone to shake their leg.
        // This is done by computing the power of the spectrum and seeing if it is small enough
        // compared to the power of white noise with amplitude equal to the amplitude of the peak.
        //double powerWhiteNoise = (N / 2) * Math.pow(peakAmplitude, 2);
        //double power = 0.0;
        //dataFreqMagnitude[peakFreqIndex] = 0.0;
        //dataFreqMagnitude[secondPeakFreqIndex] = 0.0;
        //for(int i = 0; i < N / 2; i++) {
        //    power += Math.pow(dataFreqMagnitude[i], 2);
        //}

        //if(power > energyFraction * powerWhiteNoise) {
        //    return false;
        //}

        // Compute average amplitude. Should be small compared to the peaks
        double avgAmplitude = 0.0;
        dataFreqMagnitude[fundamentalFreqIndex] = 0.0;
        dataFreqMagnitude[firstHarmonicFreqIndex] = 0.0;
        dataFreqMagnitude[secondHarmonicFreqIndex] = 0.0;
        for(int i = 0; i < N / 2; i++) {
            avgAmplitude += dataFreqMagnitude[i];
        }
        avgAmplitude /= ((N / 2) - 3);
        //Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Peak Amplitude: " + String.valueOf(peakAmplitude));
        //Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Second Amplitude: " + String.valueOf(secondPeakAmplitude));
        //Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "avg / Peak: " + String.valueOf(avgAmplitude / peakAmplitude));
        //Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "avg / Second Peak: " + String.valueOf(avgAmplitude / secondPeakAmplitude));
        if(!(avgAmplitude / peakAmplitude < firstPeakFraction && avgAmplitude / secondPeakAmplitude < secondPeakFraction)) {
            Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Power of Peaks not significant enough.");

            // Log in case for false negatives
            sendWindowToMain(
                    userName,
                    password,
                    timestamp,
                    x_axis,
                    y_axis,
                    z_axis,
                    eigVals,
                    eigVec1,
                    eigVec2,
                    eigVec3,
                    gforceMagSpectrum,
                    dataFreqMagnitude,
                    longitude,
                    latitude,
                    otherSensorData,
                    0);

            return new Long(0);
        }
       // Log.d("Shake Analysis", "Fundamental: " + String.valueOf(fundamentalFreqIndex));
       // Log.d("Shake Analysis", "first: " + String.valueOf(firstHarmonicFreqIndex));
       // Log.d("Shake Analysis", "Fundamental: " + String.valueOf(secondHarmonicFreqIndex));

        // The peak should be well above all other non-peaks
        for(int i = 0; i < fundamentalFreqIndex - 7; i++) {
            double amp_divided_by_peak = dataFreqMagnitude[i] / peakAmplitude;
                if(amp_divided_by_peak > maxSpectrumFrac) {
                   // Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Amp " + String.valueOf(i) + " / peak " + String.valueOf(peakFreqIndex) + " = " + String.valueOf(amp_divided_by_peak));

                    // Log in case for false negatives
                    sendWindowToMain(
                            userName,
                            password,
                            timestamp,
                            x_axis,
                            y_axis,
                            z_axis,
                            eigVals,
                            eigVec1,
                            eigVec2,
                            eigVec3,
                            gforceMagSpectrum,
                            dataFreqMagnitude,
                            longitude,
                            latitude,
                            otherSensorData,
                            0);

                    return new Long(0);
                }
        }
        for(int i = fundamentalFreqIndex + 7 + 1; i < firstHarmonicFreqIndex - 2; i++) {
            double amp_divided_by_peak = dataFreqMagnitude[i] / peakAmplitude;
            if(amp_divided_by_peak > maxSpectrumFrac) {
               // Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Amp " + String.valueOf(i) + " / peak " + String.valueOf(peakFreqIndex) + " = " + String.valueOf(amp_divided_by_peak));

                // Log in case for false negatives
                sendWindowToMain(
                        userName,
                        password,
                        timestamp,
                        x_axis,
                        y_axis,
                        z_axis,
                        eigVals,
                        eigVec1,
                        eigVec2,
                        eigVec3,
                        gforceMagSpectrum,
                        dataFreqMagnitude,
                        longitude,
                        latitude,
                        otherSensorData,
                        0);

                return new Long(0);
            }
        }
        for(int i = firstHarmonicFreqIndex + 2 + 1; i < secondHarmonicFreqIndex - 2; i++) {
            double amp_divided_by_peak = dataFreqMagnitude[i] / peakAmplitude;
            if(amp_divided_by_peak > maxSpectrumFrac) {
               // Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Amp " + String.valueOf(i) + " / peak " + String.valueOf(peakFreqIndex) + " = " + String.valueOf(amp_divided_by_peak));

                // Log in case for false negatives
                sendWindowToMain(
                        userName,
                        password,
                        timestamp,
                        x_axis,
                        y_axis,
                        z_axis,
                        eigVals,
                        eigVec1,
                        eigVec2,
                        eigVec3,
                        gforceMagSpectrum,
                        dataFreqMagnitude,
                        longitude,
                        latitude,
                        otherSensorData,
                        0);

                return new Long(0);
            }
        }
        for(int i = secondHarmonicFreqIndex + 2 + 1; i < N / 2; i++) {
            double amp_divided_by_peak = dataFreqMagnitude[i] / peakAmplitude;
            if(amp_divided_by_peak > maxSpectrumFrac) {
               // Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Amp " + String.valueOf(i) + " / peak " + String.valueOf(peakFreqIndex) + " = " + String.valueOf(amp_divided_by_peak));

                // Log in case for false negatives
                sendWindowToMain(
                        userName,
                        password,
                        timestamp,
                        x_axis,
                        y_axis,
                        z_axis,
                        eigVals,
                        eigVec1,
                        eigVec2,
                        eigVec3,
                        gforceMagSpectrum,
                        dataFreqMagnitude,
                        longitude,
                        latitude,
                        otherSensorData,
                        0);

                return new Long(0);
            }
        }
       // Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Leg Shaking Event: Logging to server");

        // Send the data to server
        dataFreqMagnitude[fundamentalFreqIndex] = fundamentalFreqMag;
        dataFreqMagnitude[firstHarmonicFreqIndex] = firstHarmonicFreqMag;
        dataFreqMagnitude[secondHarmonicFreqIndex] = secondHarmonicFreqMag;
        if(AppConstants.SERVER_FLAG.toInt() > 0) {
            JSONObject json_parameters = httpModule.packageParametersDetectionEvent(
                    userName,
                    password,
                    timestamp_primitive,
                    x_axis, y_axis,
                    z_axis,
                    eigVals,
                    eigVec1,
                    eigVec2,
                    eigVec3,
                    gforceMagSpectrum,
                    dataFreqMagnitude,
                    longitude,
                    latitude,
                    otherSensorData,
                    0);
            httpModule.run_http_post_to_server(AppConstants.LOGDETECTION_URL.toString(), json_parameters);
        }
        //Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Done Init Loggin");
        //Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Finish Computing");
        return timestamp;
    }

    /**
     * Stephen's implementation of leg shaking detection for general sampling frequency.
     * @param raw_3axes_data: N x 3 3 axis accelerometer data
     * @return 0 if failed, or the UTC time of the event
     */
    private Long detectLegShakeGeneral(double[][] raw_3axes_data, double[] gforce, TreeMap<String, ArrayList<float[]>> otherSensorData) {
        double[] dataOneDim = new double[AppConstants.N_FFT.toInt()];
        double firstPeakFraction = 0.1;
        double secondPeakFraction = 0.7;

        // Thresholding for determining the contour of the spectrum
        double peakBaseMaxFrac = 0.3;
        double maxSpectrumFrac = 0.25;
        int lowerbound;
        int upperbound;

        // Accounting for Spectral leakage
        if(AppConstants.ACCEL_SAMPLING_FREQ.toDouble() < AppConstants.ACCEL_MAX_SAMPLING_FREQ.toDouble()) {
            firstPeakFraction = firstPeakFraction + (0.0032 * (AppConstants.ACCEL_MAX_SAMPLING_FREQ.toDouble() - AppConstants.ACCEL_SAMPLING_FREQ.toDouble()));
            secondPeakFraction = secondPeakFraction + (.0086 * (AppConstants.ACCEL_MAX_SAMPLING_FREQ.toDouble() - AppConstants.ACCEL_SAMPLING_FREQ.toDouble()));
            peakBaseMaxFrac = peakBaseMaxFrac + (.0171 * (AppConstants.ACCEL_MAX_SAMPLING_FREQ.toDouble() - AppConstants.ACCEL_SAMPLING_FREQ.toDouble()));
        }

        // General purpose flag
        boolean flag = false;

        // flag to determine if the fundamental is strongest or the 1st harmonic. The 2nd
        // harmonic should never be the strongest after mapping.
        boolean fundamentalStrongest = false;

        int fundamentalFreqIndex = 0;
        double fundamentalFreqMag = 0;
        //int firstHarmonicFreqIndex = 0;
        //double firstHarmonicFreqMag = 0;

        //double secondPeakAmplitude = 0.0;
        //int secondPeakFreqIndex = 0;

        double peakAmplitude = 0.0;
        int peakFreqIndex = 0;

        Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Start Computing");

        // Whiten the axis data and zero pad to a power of 2.
        Matrix raw_3axes_mat = new Matrix(raw_3axes_data);
        double[][] row_3axes_data = new double[3][AppConstants.N_FFT.toInt()];
        row_3axes_data[0] = Utils.zeroPad(whiten_data(raw_3axes_mat.getMatrix(0, AppConstants.N_DATA.toInt() - 1, 0, 0).getColumnPackedCopy()), AppConstants.N_FFT.toInt() - AppConstants.N_DATA.toInt());
        row_3axes_data[1] = Utils.zeroPad(whiten_data(raw_3axes_mat.getMatrix(0, AppConstants.N_DATA.toInt() - 1, 1, 1).getColumnPackedCopy()), AppConstants.N_FFT.toInt() - AppConstants.N_DATA.toInt());
        row_3axes_data[2] = Utils.zeroPad(whiten_data(raw_3axes_mat.getMatrix(0, AppConstants.N_DATA.toInt() - 1, 2, 2).getColumnPackedCopy()), AppConstants.N_FFT.toInt() - AppConstants.N_DATA.toInt());
        raw_3axes_mat = new Matrix(row_3axes_data);
        raw_3axes_mat = raw_3axes_mat.transpose();

        // With the raw axis data, X, find the EVD of X.T * X, which will yield 3 eigenvalues and 3 eigenvectors
        EigenvalueDecomposition eigValCalculator = new EigenvalueDecomposition(raw_3axes_mat.transpose().times(raw_3axes_mat));
        Matrix eigVectors = eigValCalculator.getV();
        Matrix eigValues = eigValCalculator.getD();

        // Sort to find the eigenvalue and eigenvector pair with the highest variance
        Queue<EVDNode> eigQueue = new PriorityQueue<>(3, new Comparator<EVDNode>() {
            public int compare(EVDNode a, EVDNode b) {

                //Since queues returns the 'lowest' value first, the higher eigenvalues will get ranked lower than the smaller ones
                return a.eigVal - b.eigVal >0 ? -1: 1;
            }
        });

        // Offer the three pairs of eigenvalues and eigenvectors to queue for sorting
        eigQueue.offer(new EVDNode(eigValues.get(0, 0), eigVectors.getMatrix(0, 2, 0, 0)));
        eigQueue.offer(new EVDNode(eigValues.get(1, 1), eigVectors.getMatrix(0, 2, 1, 1)));
        eigQueue.offer(new EVDNode(eigValues.get(2, 2), eigVectors.getMatrix(0, 2, 2, 2)));
        EVDNode firstDirection = eigQueue.poll();
        EVDNode secondDirection = eigQueue.poll();
        EVDNode thirdDirection = eigQueue.poll();
        //Log.d("detectLegShake", String.valueOf(firstDirection.eigVal));
        //Log.d("detectLegShake", String.valueOf(firstDirection.eigVector.getColumnPackedCopy().length));
        //Log.d("detectLegShake", String.valueOf(secondDirection.eigVal));
        //Log.d("detectLegShake", String.valueOf(secondDirection.eigVector.getColumnPackedCopy().length));
        //Log.d("detectLegShake", String.valueOf(thirdDirection.eigVal));
        //Log.d("detectLegShake", String.valueOf(thirdDirection.eigVector.getColumnPackedCopy().length));

        Log.d("First Direction", String.valueOf(firstDirection.eigVal));
        Log.d("Second Direction", String.valueOf(secondDirection.eigVal));
        Log.d("Third Direction", String.valueOf(thirdDirection.eigVal));
        // Map to the dimension of highest variance
        for(int i = 0; i < AppConstants.N_FFT.toInt(); i++) {
            dataOneDim[i] = firstDirection.eigVector.transpose().times(raw_3axes_mat.getMatrix(i, i, 0, 2).transpose()).get(0, 0);
        }

        // Whiten the data
        dataOneDim = whiten_data(dataOneDim);

        // Compute single-sided FFT using FFT2
        double[] dataOneDim_re = new double[AppConstants.N_FFT.toInt()];
        double[] dataOneDim_im = new double[AppConstants.N_FFT.toInt()];
        System.arraycopy(dataOneDim, 0, dataOneDim_re, 0, AppConstants.N_FFT.toInt());

        Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "FFT Start");
        FFT2 fft_computer = new FFT2(AppConstants.N_FFT.toInt());
        fft_computer.fft(dataOneDim_re, dataOneDim_im);
        double[] dataFreqMagnitude = new double[AppConstants.N_FFT.toInt() / 2];
        dataFreqMagnitude[0] = Math.sqrt((dataOneDim_re[0] * dataOneDim_re[0]) + (dataOneDim_im[0] * dataOneDim_im[0]));
        for(int i = 0; i < AppConstants.N_FFT.toInt() / 2; i++) {
            dataFreqMagnitude[i] = 2 * Math.sqrt((dataOneDim_re[i] * dataOneDim_re[i]) + (dataOneDim_im[i] * dataOneDim_im[i]));
        }
        Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "FFT End");

        // Compute magnitude spectrum of gforce for logging purposes
        Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "FFT Gforce Start");
        double[] gforceMagSpectrum = new double[AppConstants.N_FFT.toInt() / 2];
        double[] gforce_im = new double[AppConstants.N_FFT.toInt()];
        gforce = Utils.zeroPad(gforce, AppConstants.N_FFT.toInt() - AppConstants.N_DATA.toInt());
        fft_computer.fft(gforce, gforce_im);
        gforceMagSpectrum[0] = Math.sqrt((gforce[0] * gforce[0]) + (gforce_im[0] * gforce_im[0]));
        for(int i = 1; i < AppConstants.N_FFT.toInt() / 2; i++) {
            gforceMagSpectrum[i] = 2 * Math.sqrt((gforce[i] * gforce[i]) + (gforce_im[i] * gforce_im[i]));
        }
        Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "FFT Gforce End");

        // Send axis data and accelerometer data back to main for loggin false negatives
        Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Send back to main start");
        long timestamp_primitive = System.currentTimeMillis() / 1000;
        Long timestamp = new Long(timestamp_primitive);
        double[] x_axis = raw_3axes_mat.getMatrix(0, AppConstants.N_FFT.toInt() - 1, 0, 0).getColumnPackedCopy();
        double[] y_axis = raw_3axes_mat.getMatrix(0, AppConstants.N_FFT.toInt() - 1, 1, 1).getColumnPackedCopy();
        double[] z_axis = raw_3axes_mat.getMatrix(0, AppConstants.N_FFT.toInt() - 1, 2, 2).getColumnPackedCopy();
        double[] eigVals = new double[3];
        double[] eigVec1 = firstDirection.eigVector.getColumnPackedCopy();
        double[] eigVec2 = secondDirection.eigVector.getColumnPackedCopy();
        double[] eigVec3 = thirdDirection.eigVector.getColumnPackedCopy();
        eigVals[0] = firstDirection.eigVal;
        eigVals[1] = secondDirection.eigVal;
        eigVals[2] = thirdDirection.eigVal;
        sendWindowToMain(
                userName,
                password,
                timestamp,
                x_axis,
                y_axis,
                z_axis,
                eigVals,
                eigVec1,
                eigVec2,
                eigVec3,
                gforceMagSpectrum,
                dataFreqMagnitude,
                longitude,
                latitude,
                otherSensorData,
                0);
        Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Send back to main end");

        // Find peak frequency.
        for(int i = 0; i < AppConstants.N_FFT.toInt() / 2; i++) {
            if(dataFreqMagnitude[i] > peakAmplitude) {
                peakAmplitude = dataFreqMagnitude[i];
                peakFreqIndex = i;
            }
        }

        Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Peak Amplitude: " + String.valueOf(peakAmplitude));
        Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Peak Freq: " + String.valueOf(peakFreqIndex));

        /*// Find second peak frequency
        // Find second peak frequencdy outside of the area around the first peak.
        //double secondPeakAmplitude = 0.0;
        //int secondPeakFreqIndex = 0;
        for(int i = 0; i < peakFreqIndex - AppConstants.HIGHEST_PEAK_BUFFER_FREQ_IDX.toInt(); i++) {
            if(dataFreqMagnitude[i] > secondPeakAmplitude) {
                secondPeakAmplitude = dataFreqMagnitude[i];
                secondPeakFreqIndex = i;
            }
        }
        for(int i = peakFreqIndex + AppConstants.HIGHEST_PEAK_BUFFER_FREQ_IDX.toInt() + 1; i < AppConstants.N_FFT.toInt() / 2; i++) {
            if(dataFreqMagnitude[i] > secondPeakAmplitude) {
                secondPeakAmplitude = dataFreqMagnitude[i];
                secondPeakFreqIndex = i;
            }
        }
        Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Second Amplitude: " + String.valueOf(secondPeakAmplitude));
        Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Second Freq: " + String.valueOf(secondPeakFreqIndex));*/

        // If eigenvalues are too small, then the phone is not moving. Return
        if(firstDirection.eigVal < eigThreshLowerGeneral) {
            Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "EigVal too small");
            return new Long(0);
        }

        // Order the first and second peaks, but first determine if the harmonic is aliased
        boolean localAliasFirstHarmonicFlag = false;
        int first_harmonic_center = AppConstants.CENTRAL_FIRST_HARMONIC_FREQ_IDX.toInt();
        if(AppConstants.ALIAS_FIRST_HARMONIC_FLAG.toBoolean()) {
            first_harmonic_center = AppConstants.N_FFT.toInt() - first_harmonic_center;
            localAliasFirstHarmonicFlag = true;
        }

        // Case where the first peak is the fundamental and the second peak is either the 1st or second harmonic.
        flag = false;
        if(peakFreqIndex >= AppConstants.CENTRAL_FUNDAMENTAL_FREQ_IDX.toInt() - AppConstants.FUNDAMENTAL_FREQ_HALF_BAND_IDX.toInt()
                && peakFreqIndex <= AppConstants.CENTRAL_FUNDAMENTAL_FREQ_IDX.toInt() + AppConstants.FUNDAMENTAL_FREQ_HALF_BAND_IDX.toInt()) {
            fundamentalStrongest = true;
            fundamentalFreqIndex = peakFreqIndex;
            fundamentalFreqMag = peakAmplitude;
            flag = true;

            /*// Second Peak is the first harmonic; check if aliased first
            first_harmonic_center = 2 * peakFreqIndex;
            if(AppConstants.N_FFT.toInt() / 2.0 < first_harmonic_center) {
                first_harmonic_center = AppConstants.N_FFT.toInt() - first_harmonic_center;
                localAliasFirstHarmonicFlag = true;
            }
            if(secondPeakFreqIndex <= first_harmonic_center + AppConstants.HALF_BAND_SECONDARY_PEAK_IDX.toInt() &&
                    secondPeakFreqIndex >= first_harmonic_center - AppConstants.HALF_BAND_SECONDARY_PEAK_IDX.toInt()) {
                flag = true;
                firstHarmonicFreqIndex = secondPeakFreqIndex;
                firstHarmonicFreqMag = secondPeakAmplitude;

            }*/
        }

        /*// first harmonic is the highest frequency
        else if(peakFreqIndex >= first_harmonic_center - AppConstants.FIRST_HARMONIC_FREQ_HALF_BAND_IDX.toInt()
                && peakFreqIndex <= first_harmonic_center + AppConstants.FIRST_HARMONIC_FREQ_HALF_BAND_IDX.toInt()){
            firstHarmonicFreqIndex = peakFreqIndex;
            firstHarmonicFreqMag = peakAmplitude;

            // The second peak should be the fundamental, though the second harmonic and the fundamental could be close.
            int fundamental_center = peakFreqIndex / 2;
            if(localAliasFirstHarmonicFlag) {
                fundamental_center = (AppConstants.N_FFT.toInt() - peakFreqIndex) / 2;
            }
            if(secondPeakFreqIndex <= fundamental_center + AppConstants.HALF_BAND_SECONDARY_PEAK_IDX.toInt() &&
                    secondPeakFreqIndex >= fundamental_center - AppConstants.HALF_BAND_SECONDARY_PEAK_IDX.toInt()) {
                flag = true;
                fundamentalFreqIndex = secondPeakFreqIndex;
                fundamentalFreqMag = secondPeakAmplitude;

            }

        }*/

        // If none of the cases are satisfied, then this is not leg shaking
        if(!flag) {
            Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "None of cases met");
            return new Long(0);
        }

        // Check peak thresholding requirements
        if(!(fundamentalAndFirstHarmonicCheckGeneral(fundamentalFreqMag, 0, firstDirection.eigVal) &&
                fundamentalAndSecondHarmonicCheckGeneral(fundamentalFreqMag, 0, firstDirection.eigVal))) {
            Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Peak Thresholding not met.");
            return new Long(0);
        }

        // The area 1 to 1.5 hz away from peaks should not have any significant values
        lowerbound = Math.max(fundamentalFreqIndex - AppConstants.UPPER_BUFFER_ZONE_FREQ_IDX.toInt(), 0);
        upperbound = Math.max(fundamentalFreqIndex - AppConstants.LOWER_BUFFER_ZONE_FREQ_IDX.toInt(), 0);
        for(int i = lowerbound; i <= upperbound; i++) {
            double amp_divided_by_peak = dataFreqMagnitude[i] / peakAmplitude;
            if(amp_divided_by_peak > peakBaseMaxFrac) {
                Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "peak " + String.valueOf(peakFreqIndex) + " / Amp " + String.valueOf(i) + " = " + String.valueOf(amp_divided_by_peak));
                return new Long(0);
            }
        }
        lowerbound = Math.min(fundamentalFreqIndex + AppConstants.LOWER_BUFFER_ZONE_FREQ_IDX.toInt(), (AppConstants.N_FFT.toInt() / 2) - 1);
        upperbound = Math.min(fundamentalFreqIndex + AppConstants.UPPER_BUFFER_ZONE_FREQ_IDX.toInt(), (AppConstants.N_FFT.toInt() / 2) - 1);
        for(int i = lowerbound; i <= upperbound; i++) {
            double amp_divided_by_peak = dataFreqMagnitude[i] / peakAmplitude;
            if(amp_divided_by_peak > peakBaseMaxFrac) {
                Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Amp " + String.valueOf(i) + " / peak " + String.valueOf(peakFreqIndex) + " = " + String.valueOf(amp_divided_by_peak));
                return new Long(0);
            }
        }

        /*lowerbound = Math.max(firstHarmonicFreqIndex - AppConstants.UPPER_BUFFER_ZONE_FREQ_IDX.toInt(), 0);
        upperbound = Math.max(firstHarmonicFreqIndex - AppConstants.LOWER_BUFFER_ZONE_FREQ_IDX.toInt(), 0);
        for(int i = lowerbound; i <= upperbound; i++) {
            double amp_divided_by_peak = dataFreqMagnitude[i] / peakAmplitude;
            if(amp_divided_by_peak > peakBaseMaxFrac) {
                Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "peak " + String.valueOf(peakFreqIndex) + " / Amp " + String.valueOf(i) + " = " + String.valueOf(amp_divided_by_peak));
                return new Long(0);
            }
        }
        lowerbound = Math.min(firstHarmonicFreqIndex + AppConstants.LOWER_BUFFER_ZONE_FREQ_IDX.toInt(), (AppConstants.N_FFT.toInt() / 2) - 1);
        upperbound = Math.min(firstHarmonicFreqIndex + AppConstants.UPPER_BUFFER_ZONE_FREQ_IDX.toInt(), (AppConstants.N_FFT.toInt() / 2) - 1);
        for(int i = lowerbound; i <= upperbound; i++) {
            double amp_divided_by_peak = dataFreqMagnitude[i] / peakAmplitude;
            if(amp_divided_by_peak > peakBaseMaxFrac) {
                Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Amp " + String.valueOf(i) + " / peak " + String.valueOf(peakFreqIndex) + " = " + String.valueOf(amp_divided_by_peak));
                return new Long(0);
            }
        }*/

        // Compute average amplitude. Should be small compared to the peaks
        double avgAmplitude = 0.0;
        dataFreqMagnitude[fundamentalFreqIndex] = 0.0;
        //dataFreqMagnitude[firstHarmonicFreqIndex] = 0.0;
        for(int i = 0; i < AppConstants.N_FFT.toInt() / 2; i++) {
            avgAmplitude += dataFreqMagnitude[i];
        }
        avgAmplitude /= ((AppConstants.N_FFT.toInt() / 2) - 2);
        Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Peak Amplitude: " + String.valueOf(peakAmplitude));
        //Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Second Amplitude: " + String.valueOf(secondPeakAmplitude));
        Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "avg / Peak: " + String.valueOf(avgAmplitude / peakAmplitude));
        //Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "avg / Second Peak: " + String.valueOf(avgAmplitude / secondPeakAmplitude));
        if(!(avgAmplitude / peakAmplitude < firstPeakFraction)) {// && avgAmplitude / secondPeakAmplitude < secondPeakFraction)) {
            Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Power of Peaks not significant enough.");
            return new Long(0);
        }
        Log.d("Shake Analysis", "Fundamental: " + String.valueOf(fundamentalFreqIndex));
        //Log.d("Shake Analysis", "first: " + String.valueOf(firstHarmonicFreqIndex));
        Log.d("Shake Analysis", "second: " + String.valueOf(0));

        // The peak should be well above all other non-peaks
        int firstPeakIdx = fundamentalFreqIndex;
        /*int secondPeakIdx = firstHarmonicFreqIndex;
        if(localAliasFirstHarmonicFlag) {
            firstPeakIdx = firstHarmonicFreqIndex;
            secondPeakIdx = fundamentalFreqIndex;
        }*/
        for(int i = 0; i < firstPeakIdx - AppConstants.HALF_BAND_SECONDARY_PEAK_IDX.toInt(); i++) {
            double amp_divided_by_peak = dataFreqMagnitude[i] / peakAmplitude;
            if(amp_divided_by_peak > maxSpectrumFrac) {
                Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Amp " + String.valueOf(i) + " / peak " + String.valueOf(peakFreqIndex) + " = " + String.valueOf(amp_divided_by_peak));
                return new Long(0);
            }
        }
        for(int i = firstPeakIdx + AppConstants.HALF_BAND_SECONDARY_PEAK_IDX.toInt() + 1; i < AppConstants.N_FFT.toInt() / 2; i++) {
            double amp_divided_by_peak = dataFreqMagnitude[i] / peakAmplitude;
            if(amp_divided_by_peak > maxSpectrumFrac) {
                Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Amp " + String.valueOf(i) + " / peak " + String.valueOf(peakFreqIndex) + " = " + String.valueOf(amp_divided_by_peak));
                return new Long(0);
            }
        }
        /*for(int i = secondPeakIdx + (AppConstants.HALF_BAND_SECONDARY_PEAK_IDX.toInt() / 2) + 1; i < AppConstants.N_FFT.toInt() / 2; i++) {
            double amp_divided_by_peak = dataFreqMagnitude[i] / peakAmplitude;
            if(amp_divided_by_peak > maxSpectrumFrac) {
                Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Amp " + String.valueOf(i) + " / peak " + String.valueOf(peakFreqIndex) + " = " + String.valueOf(amp_divided_by_peak));
                return new Long(0);
            }
        }*/
        Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Leg Shaking Event: Logging to server");

        // Send the data to server
        dataFreqMagnitude[fundamentalFreqIndex] = fundamentalFreqMag;
        //dataFreqMagnitude[firstHarmonicFreqIndex] = firstHarmonicFreqMag;
        if(AppConstants.SERVER_FLAG.toInt() > 0) {
            JSONObject json_parameters = httpModule.packageParametersDetectionEvent(
                    userName,
                    password,
                    timestamp_primitive,
                    x_axis, y_axis,
                    z_axis,
                    eigVals,
                    eigVec1,
                    eigVec2,
                    eigVec3,
                    gforceMagSpectrum,
                    dataFreqMagnitude,
                    longitude,
                    latitude,
                    otherSensorData,
                    0);
            httpModule.run_http_post_to_server(AppConstants.LOGDETECTION_URL.toString(), json_parameters);
        }
        Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Done Init Loggin");
        Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Finish Computing");
        return timestamp;
    }

    /**
     * Yan Lu's implementation of leg shaking detection
     * @param start_index
     * @param acceleration
     * @return
     */
    private Long judge(int start_index, double[] acceleration) {

        // Compute FFT
        Complex[] sensor_data = new Complex[N];
        for (int i = 0; i < N; i++) {
            sensor_data[i] = new Complex(acceleration[start_index + i], 0);
        }
        Complex[] fft_result = FFT.fft(sensor_data);
        double sum = 0;
        Queue<Node> hp = new PriorityQueue<>(heapSize, new Comparator<Node>() {
            public int compare(Node a, Node b) {
                return a.amp - b.amp >0 ? 1: -1;
            }
        });
        Node[] peaks = new Node[heapSize];

        // Not sure what this is for
        for (int i = 1; i < N / 3; i++) {
            sum += fft_result[i].abs();
        }

        // Since the spectrum is symmetrical, find the top four peaks
        for (int i = 1; i < N / 2; i++) {
            if (Double.isNaN(fft_result[i].abs())) {
                Log.i("ShakeAnalysis", "FFT is NaN");
                return new Long(0);
            }
            if (fft_result[i].abs() > fft_result[i - 1].abs() && fft_result[i].abs() > fft_result[i + 1].abs() && (hp.size() < heapSize || hp.peek().amp < fft_result[i].abs())) {
                if (hp.size() == heapSize)
                    hp.poll();
                hp.offer(new Node(i, fft_result[i].abs()));
            }
        }
        int peak_index = 0;
        while (!hp.isEmpty()) {
            peaks[peak_index++] = hp.poll();
        }
        Arrays.sort(peaks, new Comparator<Node>() {
            public int compare(Node a, Node b) {
                return a.amp - b.amp >0 ? -1: 1;
            }
        });
        int peak_freq1 = peaks[0].index;
        int peak_freq2 = peaks[2].index;
        if (peak_freq1 == 0 || peak_freq2 == 0) {
            return new Long(0);
        }
        int sumCounter = N / 3 - 1;
//      Deal with peak frequency 1
        int i = peak_freq1 - 1;
        while (i >=0 && fft_result[i].abs() < fft_result[i + 1].abs()) {
            sum -= fft_result[i--].abs();
            sumCounter--;
        }
        i = peak_freq1 + 1;
        while (i < N / 3 && fft_result[i].abs() < fft_result[i - 1].abs()) {
            sum -= fft_result[i++].abs();
            sumCounter--;
        }
        i = peak_freq2 - 1;
        while (i >=0 && fft_result[i].abs() < fft_result[i + 1].abs()) {
            sum -= fft_result[i--].abs();
            sumCounter--;
        }
        i = peak_freq2 + 1;
        while (i < N / 3 && fft_result[i].abs() < fft_result[i - 1].abs()) {
            sum -= fft_result[i++].abs();
            sumCounter--;
        }
        double avg = (sum - peaks[0].amp - peaks[1].amp) / (sumCounter - 2);
        boolean result = isValidFrequency(peaks, avg);
        if (result == true) {
            StringBuilder sb = new StringBuilder();
            for (double oneAcceleration: acceleration) {
                sb.append(Double.toString(oneAcceleration));
                sb.append(" ");
            }
            last_message = sb.toString();
        }
        if(result == true) {
            return new Long(1);
        }
        return new Long(0);
    }

    /**
     * naive implementation
     * @param start_index
     * @param acceleration
     * @return
     */
    private Long judge_naive(int start_index, double[] acceleration) {
        double[] gforceMagSpectrum = new double[N / 2];
        double[] gforce_im = new double[N];
        double[] gforce_re = acceleration;

        // Compute Spectrum
        FFT2 fft_computer = new FFT2(N);
        fft_computer.fft(gforce_re, gforce_im);
        gforceMagSpectrum[0] = Math.sqrt((gforce_re[0] * gforce_re[0]) + (gforce_im[0] * gforce_im[0]));
        for(int i = 1; i < N / 2; i++) {
            gforceMagSpectrum[i] = 2 * Math.sqrt((gforce_re[i] * gforce_re[i]) + (gforce_im[i] * gforce_im[i]));
        }


        // Find Top peak
        int peakFreqIndex = 0;
        double peakFreqAmplitude = 0;
        for(int i = 0; i < N / 2; i++) {
            if(gforceMagSpectrum[i] > peakFreqAmplitude) {
                peakFreqIndex = i;
                peakFreqAmplitude = gforceMagSpectrum[i];
            }
        }
        int lowerbound = (int) Math.max(peakFreqIndex - 4, 0);
        int upperbound = (int) Math.min(peakFreqIndex + 4, (N / 2) - 1);

        // Find second top peak
        int secondPeakFreqIndex = 0;
        double secondPeakFreqAmplitude = 0;
        for(int i = 0; i < lowerbound; i++) {
            if(gforceMagSpectrum[i] > secondPeakFreqAmplitude) {
                secondPeakFreqIndex = i;
                secondPeakFreqAmplitude = gforceMagSpectrum[i];
            }
        }
        for(int i = upperbound + 1; i < N / 2; i++) {
            if(gforceMagSpectrum[i] > secondPeakFreqAmplitude) {
                secondPeakFreqIndex = i;
                secondPeakFreqAmplitude = gforceMagSpectrum[i];
            }
        }

        // If harmonic and fundamental are the top 2 peaks return true. false otherwise
        if((peakFreqIndex >= 10 && peakFreqIndex <= 17 && (secondPeakFreqIndex >= 20 && secondPeakFreqIndex <= 35)) ||
                (peakFreqIndex >= 20 && peakFreqIndex <= 35 && secondPeakFreqIndex >= 10 && secondPeakFreqIndex <=17)) {
        //if((peakFreqIndex >= 10 && peakFreqIndex <= 17 || (peakFreqAmplitude >= 20 && peakFreqAmplitude <= 35))) {
            // Compute energy
            double energy = 0;
            gforceMagSpectrum[peakFreqIndex] = 0;
            gforceMagSpectrum[secondPeakFreqIndex] = 0;
            for(int i = 0; i < N / 2; i++) {
                energy += gforceMagSpectrum[i];
            }
            energy /= (N / 2) - 2;
            if(energy <= 0.2 * peakFreqAmplitude) {
                return new Long(1);
            }
        }

        return new Long(0);
    }

    /**
     * Implementation from the paper: Continuous output periodicity detector from accelerometer data in activity monitoring
     * http://ieeexplore.ieee.org/abstract/document/6923570/
     */
    private Long otherImplementation(double[][] raw_3axes_data) {

        // Periodic movement detected if energy of R(0) above this threshold
        double energy_threshold = 0.1;

        // Compute mean vector
        double[] mean_vec = new double[3];
        for(int i = 0; i < N; i++) {
            mean_vec[0] += raw_3axes_data[i][0];
            mean_vec[1] += raw_3axes_data[i][1];
            mean_vec[2] += raw_3axes_data[i][2];
        }
        mean_vec[0] /= N;
        mean_vec[1] /= N;
        mean_vec[2] /= N;

        // Subtract mean from all data points.
        for(int i = 0; i < N; i++) {
            for(int j = 0; j < 3; j++) {
                raw_3axes_data[i][j] = raw_3axes_data[i][j] - mean_vec[j];
            }
        }
        Matrix raw_3axes_mat = new Matrix(raw_3axes_data);

        // Sum of data points dot-producted with itself (same thing as the Frobenius norm squared of the entire matrix)
        double mean_sum = Math.pow(raw_3axes_mat.normF(), 2);

        // Compute energy (R(0))
        double energy = 0;
        for(int i = 0; i < N; i++) {
            energy = raw_3axes_mat.getMatrix(i, i, 0, 2).times(raw_3axes_mat.getMatrix(i, i, 0, 2).transpose()).get(0, 0);
        }
        //Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Signal Energy: " + String.valueOf(energy));

        // If less than threshold, then there is no movement
        if(energy < energy_threshold) {
          //  Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Signal Energy: Insignificants");
            return new Long(0);
        }

        // Compute the rest of autocorrelation
        double[] autoFunc = autoCorrelation(raw_3axes_mat, mean_sum * energy);

        // Find peak that isn't R(0)
        double peakIndex = 1;
        double peakAmplitude = 0;
        for(int i = 1; i < N; i++) {
            if(autoFunc[i] > peakAmplitude) {
                peakIndex = i;
                peakAmplitude = autoFunc[i];
            }
        }
        //Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Peak Index" + String.valueOf(peakIndex));
        //Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Peak Amplitude" + String.valueOf(peakAmplitude));

        // If peak is between 6 to 12, then it's leg shaking
        if(peakIndex >= 6 && peakIndex <= 12) {
            Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Leg Shaking detected");
            return new Long(1);
        }


        return new Long(0);
    }



    private boolean isValidFrequency(Node[] peaks, double avg) {
        int peak_freq1 = peaks[0].index;
//      From experiment, the largest local maxima's frequency could happens at index 20-35(depends on person) with large second or third harmonic (2 or 3 times the largest frequency)
        double peak_val1 = peaks[0].amp;
        double peak_val2 = 0;
        //if (peak_freq1 <= 35 && peak_freq1 >= 20) {
        if (peak_freq1 <= 17 && peak_freq1 >= 10) {
            for (int i = 1; i < heapSize; i++) {
                if (Math.abs(peaks[i].index / 2.0 - peak_freq1) <= 1 || Math.abs(peaks[i].index / 3.0 - peak_freq1) <= 1) {
                    peak_val2 = peaks[i].amp;
                    break;
                }
            }
        }
        if (peak_freq1 <= 35 && peak_freq1 >= 20) {
            for (int i = 1; i < heapSize; i++) {
                if (peak_freq1 / 2.0 - peaks[i].index <= 1 && peak_freq1 / 2.0 >= peaks[i].index)
                    peak_val2 = peaks[i].amp;
                break;
            }
        }
        return ((peak_val2 > 3 * avg && peak_val2 > 2) && (peak_val1 > 5 * avg && peak_val1 > 3));
    }

    @Override
    protected void onPostExecute(Long retVal) {

        // retVal will be the timestamp if there is a detection, 0 otherwise
        if (retVal > 0) {

            // Update to the latest timestamp
            latest_timestamp = retVal.longValue();

            /* Old version of notification
            Intent passingIntent = new Intent(_this, MainActivity.class);
            //passingIntent.putExtra(AppConstants.TIME_JSON_TAG.toString(), retVal.longValue());
            final PendingIntent contentIntent = PendingIntent.getActivity(_this, 0,
                    passingIntent, 0);

            Intent alertIntent = new Intent(_this, switchButtonListener.class);
            final PendingIntent contentIntent2 = PendingIntent.getBroadcast(_this, 11225, new Intent(_this, switchButtonListener.class), PendingIntent.FLAG_UPDATE_CURRENT);
            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(_this)
                            .setSmallIcon(R.drawable.app_icon_notification)
                            .setContentTitle("Leg Shaking")
                            .setContentText("Please stop shaking your leg")
                            .setVibrate(new long[]{100, 1000})
                            .setContentIntent(contentIntent)
                            .addAction(R.drawable.error3, "Report Error Detection", contentIntent2);
            notificationManager = (NotificationManager) _this.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(NOTIFICATION_ID);
            notificationManager.notify(NOTIFICATION_ID, mBuilder.build());*/

            // New notification
            Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Setup FP Notification");
            Intent serviceIntent = new Intent(_this, FalsePositiveNotificationService.class);
            serviceIntent.setAction(NotificationConstants.ACTION.STARTFOREGROUND_ACTION);
            serviceIntent.putExtra(AppConstants.EMAIL_JSON_TAG.toString(), userName);
            serviceIntent.putExtra(AppConstants.PASSWORD_JSON_TAG.toString(), password);
            serviceIntent.putExtra(AppConstants.TIME_JSON_TAG.toString(), latest_timestamp);
            _this.startService(serviceIntent);

            // Send a message to main activity to update graph
            Intent intent = new Intent(AppConstants.ALERT_HISTORY_LEGSHAKING_TAG.toString());
            LocalBroadcastManager.getInstance(_this).sendBroadcast(intent);

            Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "Setup FP Notification End");
        }
    }

    public static class switchButtonListener extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if(AppConstants.SERVER_FLAG.toInt() > 0) {
                JSONObject json_parameters = httpModule.packageParametersFalsePositiveEvent(userName, password, latest_timestamp, "");
                httpModule.run_http_post_to_server(AppConstants.LOGFALSEPOSITIVE_URL.toString(), json_parameters);
                notificationManager.cancel(NOTIFICATION_ID);
            }
        }
    }


    /**
     * Sends a new window of accelerometer data back to the main activity; used for logging false negatives
     */
    private void sendWindowToMain(String username,
                                  String password,
                                  long timestamp,
                                  double[] x_axis,
                                  double[] y_axis,
                                  double[] z_axis,
                                  double[] eigVals,
                                  double[] eigVec1,
                                  double[] eigVec2,
                                  double[] eigVec3,
                                  double[] gforce_mag_spectrum,
                                  double[] one_dim_mag_spectrum,
                                  String longitude,
                                  String latitude,
                                  TreeMap<String, ArrayList<float[]>> otherSensorData,
                                  int falsePositive) {

        //Log.i(AppConstants.SHAKEDETECTSERVICE_TAG.toString(), "Sending new Window back to Main");
        Intent intent = new Intent(AppConstants.NEWACCELDATEUPDATE_INTENTFILTER_TAG.toString());
        intent.putExtra(AppConstants.EMAIL_JSON_TAG.toString(), username);
        intent.putExtra(AppConstants.PASSWORD_JSON_TAG.toString(), password);
        intent.putExtra(AppConstants.TIME_JSON_TAG.toString(), timestamp);
        intent.putExtra(AppConstants.XAXIS_JSON_TAG.toString(), x_axis);
        intent.putExtra(AppConstants.YAXIS_JSON_TAG.toString(), y_axis);
        intent.putExtra(AppConstants.ZAXIS_JSON_TAG.toString(), z_axis);
        intent.putExtra(AppConstants.EIGVALS_JSON_TAG.toString(), eigVals);
        intent.putExtra(AppConstants.EIGVEC1_JSON_TAG.toString(), eigVec1);
        intent.putExtra(AppConstants.EIGVEC2_JSON_TAG.toString(), eigVec2);
        intent.putExtra(AppConstants.EIGVEC3_JSON_TAG.toString(), eigVec3);
        intent.putExtra(AppConstants.GFORCEMAGSPECTRUM_JSON_TAG.toString(), gforce_mag_spectrum);
        intent.putExtra(AppConstants.ONEDIMMAGSPECTRUM_JSON_TAG.toString(), one_dim_mag_spectrum);
        intent.putExtra(AppConstants.FALSEPOSITIVE_JSON_TAG.toString(), falsePositive);
        intent.putExtra(AppConstants.LONGITUDE_JSON_TAG.toString(), longitude);
        intent.putExtra(AppConstants.LATITUDE_JSON_TAG.toString(), latitude);
        intent.putExtra(AppConstants.OTHERSENSORDATA_JSON_TAG.toString(), otherSensorData);
        LocalBroadcastManager.getInstance(_this).sendBroadcast(intent);
    }

    /**
     * Checking power level of fundamental and first harmonic. Return true if passed, false otherwise.
     * Also this function takes the value of the principal eigenvalue
     * @return
     */
    private boolean fundamentalAndFirstHarmonicCheck(double fundamental, double harmonic, double eigVal) {
        double dBRatio;
        double maximumThreshold;

        // If eigenvalue is less than a threshold, then the phone is not moving, so return false.
        if(eigVal < eigThreshLower) {
            return false;
        }

        // If eigenvalue is between upper and lower thresholds, return true because at this point,
        // the fundamental and harmonics could be anything
        if(eigVal > eigThreshLower && eigVal < eigThreshUpper) {
            return true;
        }

        // Otherwise, we must now compute the proper maximum threshold ratio harmonic vs fundamental based on the equation:
        // -2.2 * ln(eigVal) + 6.65;
        maximumThreshold = (-2.2 * Math.log(eigVal)) + 6.65;
        dBRatio = 10 * Math.log10(harmonic / fundamental);

        // If the ratio is less than the threshold, return true
        if(dBRatio < maximumThreshold) {
            return true;
        }
        //Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "maximumThreshold: " + String.valueOf(maximumThreshold));
        //Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "1st harmonic / fundamental: " + String.valueOf(dBRatio));
        return false;
    }


    /**
     * Checking power level of fundamental and second harmonic. Return true if passed, false otherwise.
     * Also this function takes the value of the principal eigenvalue
     * @return
     */
    private boolean fundamentalAndSecondHarmonicCheck(double fundamental, double harmonic, double eigVal) {

        double dBRatio;
        double maximumThreshold;

        // If eigenvalue is less than a threshold, then the phone is not moving, so return false.
        if(eigVal < eigThreshLower) {
            return false;
        }

        // Otherwise, we must now compute the proper maximum threshold ratio harmonic vs fundamental based on the equation:
        // -2.2 * ln(eigVal - 1) + 1;
        maximumThreshold = (-2.2 * Math.log(eigVal)) + 1;
        dBRatio = 10 * Math.log10(harmonic / fundamental);

        // If the ratio is less than the threshold, return true
        if(dBRatio < maximumThreshold) {
            return true;
        }

       // Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "maximumThreshold: " + String.valueOf(maximumThreshold));
       // Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "2nd harmonic / fundamental: " + String.valueOf(dBRatio));
        return false;
    }

    /**
     * Checking power level of fundamental and first harmonic. Return true if passed, false otherwise.
     * Also this function takes the value of the principal eigenvalue
     * @return
     */
    private boolean fundamentalAndFirstHarmonicCheckGeneral(double fundamental, double harmonic, double eigVal) {
        double dBRatio;
        double maximumThreshold;

        // If eigenvalue is less than a threshold, then the phone is not moving, so return false.
        if(eigVal < eigThreshLowerGeneral) {
            return false;
        }

        // If eigenvalue is between upper and lower thresholds, return true because at this point,
        // the fundamental and harmonics could be anything
        if(eigVal > eigThreshLowerGeneral && eigVal < eigThreshUpper) {
            return true;
        }

        // Otherwise, we must now compute the proper maximum threshold ratio harmonic vs fundamental based on the equation:
        // -2.2 * ln(eigVal) + 6.65;
        maximumThreshold = (-2.2 * Math.log(eigVal)) + 6.65;
        dBRatio = 10 * Math.log10(harmonic / fundamental);

        // If the ratio is less than the threshold, return true
        if(dBRatio < maximumThreshold) {
            return true;
        }
        //Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "maximumThreshold: " + String.valueOf(maximumThreshold));
        //Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "1st harmonic / fundamental: " + String.valueOf(dBRatio));
        return false;
    }


    /**
     * Checking power level of fundamental and second harmonic. Return true if passed, false otherwise.
     * Also this function takes the value of the principal eigenvalue
     * @return
     */
    private boolean fundamentalAndSecondHarmonicCheckGeneral(double fundamental, double harmonic, double eigVal) {

        double dBRatio;
        double maximumThreshold;

        // If eigenvalue is less than a threshold, then the phone is not moving, so return false.
        if(eigVal < eigThreshLowerGeneral) {
            return false;
        }

        // Otherwise, we must now compute the proper maximum threshold ratio harmonic vs fundamental based on the equation:
        // -2.2 * ln(eigVal - 1) + 1;
        maximumThreshold = (-2.2 * Math.log(eigVal)) + 1;
        dBRatio = 10 * Math.log10(harmonic / fundamental);

        // If the ratio is less than the threshold, return true
        if(dBRatio < maximumThreshold) {
            return true;
        }

        // Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "maximumThreshold: " + String.valueOf(maximumThreshold));
        // Log.d(AppConstants.SHAKEANALYSISSERVICE_TAG.toString(), "2nd harmonic / fundamental: " + String.valueOf(dBRatio));
        return false;
    }

    /**
     * Given observation vector return true if more likely to be leg shake, false otherwise
     * @return
     */
    public boolean HypothesisTest(Matrix observation) {

        // Compute probability of leg shake
        Matrix temp = observation.minus(mean_vector_positive);
        double prob_positive = log_cov_det_positive + (temp.transpose().times(covariance_matrix_positive_inverse).times(temp).get(0, 0) * -0.5);

        // Compute probability of non leg shake
        temp = observation.minus(mean_vector_negative);
        double prob_negative = log_cov_det_negative + (temp.transpose().times(covariance_matrix_negative_inverse).times(temp).get(0, 0) * -0.5);
        //double prob_negative = 1 / 64;

        // Determine if leg shake
        //Log.d("Prob", "prob_positive: " + prob_positive);
        //Log.d("Prob", "prob_negative: " + prob_negative);
        //if(prob_positive > prob_negative +  Math.log(lambda)) {
        //    return true;
        //}

        // Determine if leg shake
        Log.d("Prob", "prob_positive: " + prob_positive);
        Log.d("Prob", "prob_positive threshold: " + (log_cov_det_positive + Math.log(threshold_frac)));
        if(prob_positive > log_cov_det_positive + Math.log(threshold_frac)) {
            return true;
        }

        return false;
    }
}
