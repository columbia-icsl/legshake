package columbia.seas.icsl.legshake.constants;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/**
 * Created by jordanvega on 12/7/17.
 */

public class NotificationConstants {
    public interface ACTION {
        public static String MAIN_ACTION = "main";
        public static String PREV_ACTION = "prev";
        public static String FALSE_NEG_WITH_COMMENT = "alert_false_neg_comment";
        public static String NOTIFICATION_ICON = "notification_icon";
        public static String FALSE_NEG = "alert_false_neg";
        public static String STARTFOREGROUND_ACTION = "startforeground";
        public static String STOPFOREGROUND_ACTION = "stopforeground";
        public static String RETURN_COMMENT_ACTION = "returncomment";

        public static String FALSE_POS_WITH_COMMENT = "alert_false_pos_comment";

    }

    public interface NOTIFICATION_ID {
        public static int FOREGROUND_SERVICE = 101;
    }

    public static Bitmap getDefaultAlbumArt(Context context) {
        Bitmap bm = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        try {
            bm = BitmapFactory.decodeResource(context.getResources(),
                    columbia.seas.icsl.legshake.R.drawable.app_icon, options);
        } catch (Error ee) {
        } catch (Exception e) {
        }
        return bm;
    }

}