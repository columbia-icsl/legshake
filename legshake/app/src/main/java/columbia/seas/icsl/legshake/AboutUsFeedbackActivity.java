package columbia.seas.icsl.legshake;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by stephen on 2/2/2018.
 */

public class AboutUsFeedbackActivity extends Activity{

    Context context;
    Activity _this = this;

    private String username;
    private String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this.getApplicationContext();

        // get username
        SharedPreferences sharedPref = this.getApplicationContext().getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        username = sharedPref.getString(getString(R.string.preference_email),"");
        password = sharedPref.getString(getString(R.string.preference_password),"");

        setContentView(R.layout.activity_aboutus_feedback);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Button feedbackButton = (Button) findViewById(R.id.activity_aboutus_feedback_sendfeedbackButton);
        Button backButton = (Button) findViewById(R.id.activity_aboutus_feedback_backButton);

        // Login with credentials
        feedbackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!AboutUsFeedbackDialog.active) {
                    Intent commentDia = new Intent(_this, AboutUsFeedbackDialog.class);
                    commentDia.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    commentDia.setAction(Intent.ACTION_MAIN);
                    startActivity(commentDia);
                }
            }
        });

        // Go back to launch page
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(AboutUsFeedbackActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);
                startActivity(intent);
                _this.finish();
            }
        });
    }

}
