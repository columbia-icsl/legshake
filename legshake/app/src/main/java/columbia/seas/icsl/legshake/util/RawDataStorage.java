package columbia.seas.icsl.legshake.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.TreeMap;

/**
 * Created by stephen on 4/26/2017.
 * For storing timestamp + accelerometer data
 */

public class RawDataStorage implements Serializable {

    public String email;
    public String password;
    public long timestamp;
    public double[] x_axis;
    public double[] y_axis;
    public double[] z_axis;
    public double[] eigVals;
    public double[] eigVec1;
    public double[] eigVec2;
    public double[] eigVec3;
    public double[] one_dim_mag_spectrum;
    public double[] gforce_mag_spectrum;
    public int false_positive;
    public String longitude;
    public String latitude;
    public TreeMap<String, ArrayList<float[]>> otherSensorData;

    public RawDataStorage(String email, String password, long timestamp, double[] x_axis, double[] y_axis, double[] z_axis, double[] eigVals, double[] eigVec1, double[] eigVec2, double[] eigVec3, double[] gforce_mag_spectrum, double[] one_dim_mag_spectrum, String longitude, String latitude, TreeMap<String, ArrayList<float[]>> otherSensorData, int false_positive) {
        this.email = email;
        this.password = password;
        this.timestamp = timestamp;
        this.x_axis = x_axis;
        this.y_axis = y_axis;
        this.z_axis = z_axis;
        this.eigVals = eigVals;
        this.eigVec1 = eigVec1;
        this.eigVec2 = eigVec2;
        this.eigVec3 = eigVec3;
        this.one_dim_mag_spectrum = one_dim_mag_spectrum;
        this.gforce_mag_spectrum = gforce_mag_spectrum;
        this.longitude = longitude;
        this.latitude = latitude;
        this.otherSensorData = otherSensorData;
        this.false_positive = false_positive;
    }
}
