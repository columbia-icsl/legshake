"""
This is a simple backend endpoint used to collect false positive
"""
from flask import Flask
from flask import request
app = Flask(__name__)


@app.route('/', methods=['POST'])

def getdata():
    try:
        a = request.form
        data = a.keys()[0]
        splited = data.split(';')
        name = splited[0];
        data = splited[1];
        text_file = open(name+'.txt','a+')
        text_file.write(data)
        text_file.write('\n\n')
        text_file.close()
        print name
    #    print data
        return '200'
    except:
        return '400'


app.run(debug=True, host='0.0.0.0', port=5000)

