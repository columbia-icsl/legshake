% This script is used to visualization the leg shake's frequency domain
% plot, please fill in the value in the input variable from any txt file
% provide in this folder
input = [] % Please fill in the acceleration data from one line of any txt file in Matlab folder
N = 256;
f = 50;
x = [0 : N - 1] * f / N;
fft_data = abs(fft(input));
plot( fft_data(2:N / 2 - 1), '.-');
sum_freq = sum(fft_data(2:N/3));
sum_count = floor(N/3-1);
[val, index] = sort(fft_data(2:N/2));
max_freq1 = index(1);
max_freq2 = index(2);
i = max_freq1 - 1;
while (i > 0 && fft_data(i) < fft_data(i + 1))
    sum_freq = sum_freq - fft_data(i);
    sum_count = sum_count-1;
    i = i - 1;
end
i = max_freq1 + 1;
while (i <= N/3 && fft_data(i) < fft_data(i - 1))
    sum_freq = sum_freq - fft_data(i);
    sum_count = sum_count-1;
    i = i + 1;
end
i = max_freq2 - 1;
while (i > 0 && fft_data(i) < fft_data(i + 1))
    sum_freq = sum_freq - fft_data(i);
    sum_count = sum_count-1;
    i = i - 1;
end
i = max_freq2 + 1;
while (i <= N/3 && fft_data(i) < fft_data(i - 1))
    sum_freq = sum_freq - fft_data(i);
    sum_count = sum_count-1;
    i = i + 1;
end
avg = sum_freq / sum_count
[pks,locs] = findpeaks(fft_data(2:128));
[val, index] = sort(pks,'descend');
locs(index(1))
locs(index(2))
locs(index(3))
locs(index(4))
locs(index(5))
